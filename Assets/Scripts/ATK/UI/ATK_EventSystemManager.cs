﻿/// <summary>
/// EventSystemクラス
/// </summary>
/// インスタンスの使用できるタイミングは、インスタンス作成後のUpdate()以降




using UnityEngine;
//using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;




namespace ATK.UI
{
	public class ATK_EventSystemManager : ATK_SingletonMonoBehaviourFast<ATK_EventSystemManager>
	{
		
		#region 非公開メンバ変数	
		#endregion



		#region 公開メンバ変数

		public EventSystem @EventSystem = null;
		public StandaloneInputModule @StandaloneInputModule = null;
		public TouchInputModule @TouchInputModule = null;

		#endregion


		#region プロパティ


		#endregion


		#region 非公開メンバ関数	

		void Start()
		{
			Assert.IsNotNull(EventSystem, "EventSystemがアタッチされていません。");
			Assert.IsNotNull(StandaloneInputModule, "StandaloneInputModuleがアタッチされていません。");
			Assert.IsNotNull(TouchInputModule, "TouchInputModuleがアタッチされていません。");
		}

		#endregion


		#region 公開メンバ関数	

		/// <summary>
		/// 入力を有効化
		/// </summary>
		public void EnableInput()
		{
			EventSystem.enabled = true;
		}
		
		/// <summary>
		/// 入力を無効化
		/// </summary>
		public void DisableInput()
		{
			EventSystem.enabled = false;
		}

		#endregion		
	}

}


