﻿using UnityEngine;

#if false

namespace ATK
{
	/// <summary>
	///	アプリケーション初期化
	/// </summary>
	public partial class ATK_Application_AppInitializer
	{
	#region 非公開メンバ関数

		
		/// <summary>
		/// シーンマネージャの初期化
		/// </summary>
		private void _initSceneManager()
		{
			UIAtlas atlas = ATK.Utilities.Resources.Load<UIAtlas>("System/Scene/System_SceneAtlas");
			Assert.IsNotNull(atlas,"System/Scene/System_SceneAtlasが見つからない");

			UIFont font = ATK.Utilities.Resources.Load<UIFont>("System/Scene/System_SceneFont");
			Assert.IsNotNull(font,"System/Scene/System_SceneFontが見つからない");
				
			ATK_Scene_Manager mgr = ATK_Scene_Manager.Instance;
				
			mgr.Initialize(atlas,font);	
			mgr.SetFadeColor(1.0f,1.0f,1.0f);	
			
			_initToast();
			
			if( Debug.isDebugBuild ){
				_initDebugMenu();
			}
		}

		/// <summary>
		/// トーストの初期化
		/// </summary>
		private void _initToast()
		{				
			Toast.DefaultColor = Color.black;	
		}
		
		/// <summary>
		/// デバッグメニューの初期化
		/// </summary>
		private void _initDebugMenu()
		{
			ATK_Scene_DebugLayer debug = ATK_Scene_Manager.Instance.Debug;	
			/*
			var menus = ATK.Utilities.Resources.LoadAll<GameObject>("DebugMenu/Prefab");
			foreach(var pair in menus){
				debug.MenuFactory.AddMenu(pair.Key,pair.Value);
			}
			*/
		}
		



	#endregion

	#region 公開メンバ関数

		/// <summary>
		/// 初期化
		/// </summary>
		partial void OnInitialize()
		{		
			// シーンの初期化
			_initSceneManager();
							
			// マルチタッチ無効
			Input.multiTouchEnabled = false;				
		}

		/*
ATK_SoundManagerで詠込むようにした
		/// <summary>
		/// 常駐サウンドクリップ読み込み
		/// </summary>
		partial void OnLoadResidentAudioClips()
		{		
			ATK.Audio.SoundBankDB.Add(
				"BGM",
				ATK.Audio.SoundBank.Create("BGM")
				);
		
					
			ATK.Audio.SoundBankDB.Add(
				"SE",
				ATK.Audio.SoundBank.Create("SE")
				);					
			
		}
	*/	

		/// <summary>
		/// セーブデータ読み込み
		/// </summary>
		partial void LoadUserData()
		{
			
			bool isNew = false;
			bool isBroken = false;
			
			ATK.Utilities.SaveData.EError error;
					
			error = App.LoadSystemData();
			if( error == ATK.Utilities.SaveData.EError.FileNotFound ){
				// ないなら新規
				isNew = true;
			}else if (error != ATK.Utilities.SaveData.EError.Succeed){
				// なんかエラーなら破損			
				isBroken = true;
			}		

			error = App.LoadGameData();
			if( error == ATK.Utilities.SaveData.EError.FileNotFound ){
				// ないなら新規
				isNew = true;
			}else if (error != ATK.Utilities.SaveData.EError.Succeed){
				// なんかエラーなら破損			
				isBroken = true;
			}		
			Debug.Log("GameSaveLoad = " + error.ToString());
							
			if( isNew ){
				isBroken = false;
			}
					
			SaveUserData();	
			
		}
		
		/// <summary>
		/// セーブデータ保存
		/// </summary>
		partial void SaveUserData()
		{			
			App.SaveSystemData();
			App.SaveGameData();									
		}
		
	#endregion
		
		
	}
}
#endif