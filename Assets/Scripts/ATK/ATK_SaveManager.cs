﻿/// <summary>
/// データをシリアライズ／デシリアライズするクラス
/// </summary>
/// バイナリまたはXMLでシリアライズ／デシリアライズ
/// 完了後のコールバックを設定できる
/// このクラスをインスタンス化して常駐させておけば、いつでも使える
/// インスタンスの使用できるタイミングは、インスタンス作成直後から




using UnityEngine;
using System;
using System.Collections;


/*
TODO iCloudでバックアップしないように設定するには　iPhone.SetNoBackupFlag(path);
TODO コルーチン化すべき？

*/

namespace ATK
{

	public class ATK_SaveManager : ATK_SingletonMonoBehaviourFast<ATK_SaveManager>
	{
		#region 公開メンバ変数

		/// <summary>
		/// 暗号キー　未入力の場合はデフォルトが使われる
		/// </summary>
		[SerializeField] private string _cryptographicKey = UnityEngine.Application.productName;

		#endregion


		#region 非公開メンバ変数

		private static object _saveLock = new object();

        #endregion


        #region Callback

        public delegate void OnStartDelegete();
		public delegate void OnCompleteDelegate();

        #endregion




        #region Serializable Binary

        /// <summary>
        /// バイナリファイルでセーブ
        /// </summary>
        /// <param name="uniqueData">シリアライズするデータクラス</param>
        /// <param name="fullPath">ファイル名を含むパス</param>
        /// <param name="startCallback">開始のコールバック</param>
        /// <param name="completeCallback">完了後のコールバック。エラーの場合、呼ばれません</param>
        public ATK.Utilities.SaveData.EError Save<T>(
			T uniqueData,
			string fullPath,
            OnStartDelegete startCallback,
			OnCompleteDelegate completeCallback
			) where T : class
		{
			ATK.Utilities.SaveData.EError error = ATK.Utilities.SaveData.EError.Succeed;

            if (startCallback != null)
            {
                startCallback();
            }

            lock (_saveLock) {

				Debug.Log("Save = " + fullPath);
				
				try {
					ATK.Utilities.SaveData.SaveFullPath<T>(
						out error,
						fullPath,
						uniqueData,
                        _cryptographicKey
                        );			
					
				} catch (System.Exception e) {
					Debug.Log(e);
					error = ATK.Utilities.SaveData.EError.Unknown;
				}
			}
			
			if (error == ATK.Utilities.SaveData.EError.Succeed
			    && completeCallback != null
			    ) {
                completeCallback();
			}

			return error;
		}
        public ATK.Utilities.SaveData.EError Save<T>(
            T uniqueData,
            string fullPath,
            OnCompleteDelegate completeCallback
            ) where T : class
        {
            return Save(uniqueData, fullPath, null, completeCallback);
        }



        /// <summary>
        /// バイナリファイルをロード
        /// </summary>
        /// <param name="container">ロードしたデータを入れるコンテナ</param>
        /// <param name="fullPath">ファイル名を含むパス</param>
        /// <param name="startCallback">開始のコールバック</param>
        /// <param name="completeCallback">完了後のコールバック。エラーの場合、呼ばれません</param>
        public ATK.Utilities.SaveData.EError Load<T>(
			out T container,
			string fullPath,
            OnStartDelegete startCallback,
            OnCompleteDelegate completeCallback
			) where T : class
		{
			ATK.Utilities.SaveData.EError error = ATK.Utilities.SaveData.EError.Succeed;

            if (startCallback != null)
            {
                startCallback();
            }

            container = default(T);

			lock (_saveLock) {
				
				Debug.Log("Load = " + fullPath);
				
				try {
					
					container = ATK.Utilities.SaveData.LoadFullPath<T>(
						out error,
						fullPath,
                        _cryptographicKey
                        );	
					
					if( error == ATK.Utilities.SaveData.EError.Succeed ) {
					}
					
				} catch (System.Exception e) {
					Debug.Log(e);
					error = ATK.Utilities.SaveData.EError.Unknown;
				}
			}

			if (error == ATK.Utilities.SaveData.EError.Succeed
			    && completeCallback != null
			    ) {
				completeCallback();
			}

			return error;
		}
        public ATK.Utilities.SaveData.EError Load<T>(
            out T container,
            string fullPath,
            OnCompleteDelegate completeCallback
            ) where T : class
        {
            return Load(out container, fullPath, null, completeCallback);
        }

        #endregion







            #region Serializable XML

            /// <summary>
            /// XMLファイルでセーブ
            /// </summary>
            /// <param name="uniqueData">シリアライズするデータクラス</param>
            /// <param name="fullPath">ファイル名を含むパス</param>
            /// <param name="completeCallback">完了後のコールバック。エラーの場合、呼ばれません</param>
        public ATK.Utilities.SaveData.EError SaveXml<T>(
			T uniqueData,
			string fullPath,
            OnStartDelegete startCallback,
            OnCompleteDelegate completeCallback
			) where T : class
		{
			ATK.Utilities.SaveData.EError error = ATK.Utilities.SaveData.EError.Succeed;

            if (startCallback != null)
            {
                startCallback();
            }

            lock (_saveLock) {

				Debug.Log("SaveXml = " + fullPath);

				try {
					ATK.Utilities.SaveData.SaveXmlFullPath<T>(
						out error,
						fullPath,
						uniqueData
						);			
					
				} catch (System.Exception e) {
					Debug.Log(e);
					error = ATK.Utilities.SaveData.EError.Unknown;
				}
			}

			if (error == ATK.Utilities.SaveData.EError.Succeed
			    && completeCallback != null
			    ) {
				completeCallback();
			}
			
			return error;
		}
        public ATK.Utilities.SaveData.EError SaveXml<T>(
            T uniqueData,
            string fullPath,
            OnCompleteDelegate completeCallback
            ) where T : class
        {
            return SaveXml(uniqueData, fullPath, null, completeCallback);
        }


        /// <summary>
        /// バイナリファイルをロード
        /// </summary>
        /// <param name="container">ロードしたデータを入れるコンテナ</param>
        /// <param name="fullPath">ファイル名を含むパス</param>
        /// <param name="completeCallback">完了後のコールバック。エラーの場合、呼ばれません</param>
        public ATK.Utilities.SaveData.EError LoadXml<T>(
			out T container,
			string fullPath,
            OnStartDelegete startCallback,
            OnCompleteDelegate completeCallback
			) where T : class
		{
			ATK.Utilities.SaveData.EError error = ATK.Utilities.SaveData.EError.Succeed;

            if (startCallback != null)
            {
                startCallback();
            }

            container = default(T);
			
			lock (_saveLock) {
				
				Debug.Log("LoadXml = " + fullPath);
				
				try {
					
					container = ATK.Utilities.SaveData.LoadXmlFullPath<T>(
						out error,
						fullPath
						);	
					
					if( error == ATK.Utilities.SaveData.EError.Succeed ) {
					}
					
				} catch (System.Exception e) {
					Debug.Log(e);
					error = ATK.Utilities.SaveData.EError.Unknown;
				}
			}
			
			if (error == ATK.Utilities.SaveData.EError.Succeed
			    && completeCallback != null
			    ) {
				completeCallback();
			}
			
			return error;
		}
        public ATK.Utilities.SaveData.EError LoadXml<T>(
            out T container,
            string fullPath,
            OnCompleteDelegate completeCallback
            ) where T : class
        {
            return LoadXml(out container, fullPath, null, completeCallback);
        }

        #endregion

        }

}