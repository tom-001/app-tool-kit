﻿/*

シーンを遷移させるクラス（シングルトン）

シーン間をフェードでつなぐ
このクラスをインスタンス化して常駐させて使う
インスタンス化されると、キャンバス等を作成する
インスタンスの使用できるタイミングは、インスタンス作成後のUpdate()以降

*/
#define USING_TRANSITION_PRO	// ATK_TransitionProを使用する　※有料アセット必須

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Collections;


#pragma warning disable 414

namespace ATK.Scene
{

	/// <summary>
	/// シーンマネージャ
	/// </summary>
	public class ATK_SceneManager : ATK_SingletonMonoBehaviourFast<ATK_SceneManager>
	{
		
		#region 公開内部クラス
		
		
		/// <summary>
		/// シーン切り替えパラメータ
		/// </summary>
		public class Option
		{
			#region 公開メンバ変数
			
			/// <summary>
			/// シーン切り替え完了時に不要なアセットをアンロードする
			/// </summary>
			public bool UnloadUnusedAssets;
			
			/// <summary>
			/// シーン切替時にGCを実行する
			/// </summary>
			public bool GCCollect;
			
			#endregion
			
			
			#region 公開メンバ関数
			
			// コンストラクタ	
			public Option()
			{
				Reset();
			}
			
			/// <summary>
			///  パラメータリセット
			/// </summary>
			public void Reset()
			{
				this.UnloadUnusedAssets = true;
				this.GCCollect = true;
			}
			
			#endregion
			
		}
		
		
		/// <summary>
		/// シーン
		/// </summary>
		public struct Scene
		{
			
			// null シーン	
			public static Scene Null = new Scene("");
			
			#region 公開メンバ変数
			
			/// <summary>
			/// 現在のレベル名
			/// </summary>
			public string LevelName;
			
			
			#endregion
			
			#region 公開メンバ関数
			
			/// <summary>
			/// コンストラクタ
			/// </summary>
			/// <param name="levelName"></param>		
			public Scene(
				string levelName
				)
			{
				this.LevelName = levelName;
			}
			
			/// <summary>
			/// 無効なシーンか
			/// </summary>
			/// <returns></returns>
			public bool IsNull()
			{
				return 0 == this.LevelName.Length;
			}
			
			#endregion
			
		}
		
		#endregion
		
		#region 定数
		
		/// <summary>
		/// フェード状態
		/// </summary>
		private enum FadeState
		{
			Idle,
			
			InStart,
			In,
			
			OutStart,
			Out,
			
			Jump,
		}
		
		
		#endregion
		
		#region 非公開メンバ変数

		private ATK_TransitionInterface _transition;

		private List<Scene> _sceneStack = new List<Scene>();
		private Scene _current = Scene.Null;
		private Scene _prev = Scene.Null;
		private Option _option = new Option();
		
		private Canvas _canvas = null;
		private CanvasScaler _canvasScaler = null;
		private GraphicRaycaster _graphicRaycaster = null;

		private FadeState _fadeState = FadeState.Idle;
		private float _fadeEndTime = 0.0f;
		private float _fadeCurrentTime = 0.0f;

		private bool _initialized = false;
		
		
		private Font _font = null;
		//private Sprite _nowLoading = null;
		//private UIAtlas _atlas = null;
		//private ATK_Scene_ToastLayer _toast = null;
		
		[SerializeField] private GameObject _debugPrefab = null;
		//private ATK_Scene_DebugLayer _debug = null;
		
		
		#endregion
		
		
		#region 非公開クラス変数
		#endregion
		
		#region 公開クラス変数
		
		/// <summary>
		/// デフォルトフェード時間
		/// </summary>
		public static float DefaultFadeTime = 0.25f;
		
		/// <summary>
		/// ゴミ掃除レベル
		/// </summary>
		public static string CleanLevelName = "Clean";
		
		
		#endregion
		
		
		#region プロパティ
		
		
		/// <summary>
		/// 現在のシーン
		/// </summary>
		public Scene Current
		{
			get { return _current; }
		}
		
		/// <summary>
		/// 前のシーン
		/// </summary>
		public Scene Prev
		{
			get { return _prev; }
		}
		/*
		/// <summary>
		/// トースト取得
		/// </summary>
		public ATK_Scene_ToastLayer Toast
		{
			get { return _toast; }
		}

		/// <summary>
		/// デバッグレイヤー取得
		/// </summary>
		public ATK_Scene_DebugLayer Debug
		{
			get { return _debug; }
		}
		*/

		/// <summary>
		/// フェードイン(だんだん明るくなる)開始時に呼ばれるコールバック
		/// 引数なしのデリゲート
		/// </summary>
		public event OnStartFadeInDelegate OnStartFadeInEvent;

        /// <summary>
        /// フェードイン(だんだん明るくなる)終了時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnFinishedFadeInDelegate OnFinishedFadeInEvent;

        /// <summary>
        /// フェードアウト(だんだん暗くなる)開始時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnStartFadeOutDelegate OnStartFadeOutEvent;

        /// <summary>
        /// フェードアウト(だんだん暗くなる)終了時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnFinishedFadeOutDelegate OnFinishedFadeOutEvent;

        #endregion



        #region 公開メンバ関数
        //TODO initialize アトラス設定する？
        override protected void Awake() 
		{
			base.Awake();
			
			_current.LevelName = UnityEngine.Application.loadedLevelName;
			
			Initialize(null);
		}
		
		/// <summary>
		/// 初期化
		/// </summary>
		/// <param name="atlas"></param>
		/// <param name="font"></param>
		public void Initialize(
			//UIAtlas atlas,
			Font font
			)
		{
			//Assert.IsNotNull(atlas, "null==atlas");
			//Assert.IsNotNull(font, "null==font");
			
			//_atlas = atlas;
			_font = font;
			
			_createCanvas();
			_createTransition();
			_createNowLoadingImage();
			/*
            TODO
			// トーストを作成
			{
				GameObject obj = new GameObject();
				obj.name = "ToastLayer";
				obj.transform.parent = _canvas.transform;
				obj.transform.localPosition = new Vector3(0.0f, 0.0f, -10.0f);
				obj.transform.localScale = Vector3.one;

				_toast = obj.AddComponent<ATK_Scene_ToastLayer>();
				_toast.Initialize(atlas, font);
				_toast.gameObject.SetActive(false);

			}
			
			// デバッグの作成
			{
				GameObject obj = Instantiate(_debugPrefab) as GameObject;
				obj.name = "DebugLayer";
				obj.transform.parent = _canvas.transform;
				
				obj.transform.localPosition = new Vector3(0.0f, 0.0f, -10.0f);
				obj.transform.localScale = Vector3.one;

				//_debug = obj.GetComponent<ATK_Scene_DebugLayer>();			
			}
			*/
			_initialized = true;
		}
		
		
		
		// Update is called once per frame
		void LateUpdate()
		{
			if( !_initialized ){
				return;
			}
			
			_updateFade();
			#if false
			// カメラ制御
			if (_camera.enabled) {
				
				if (!_fadeImage.gameObject.activeSelf && /*!_nowLoading.gameObject.activeSelf &&*/ !_toast.gameObject.activeSelf && !_debug.IsVisible ) {
					_camera.enabled = false;
				}
				
			} else {
				
				if (_fadeImage.gameObject.activeSelf || /*_nowLoading.gameObject.activeSelf ||*/ _toast.gameObject.activeSelf || _debug.IsVisible )  {
					_camera.enabled = true;
				}
			}
			#endif
			
		}
		
		
		
		/// <summary>
		/// フェードカラー設定
		/// </summary>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="b"></param>
		public void SetFadeColor(
			float r,
			float g,
			float b
			)
		{
			_transition.SetFadeColor(r,g,b);
		}
		/// <summary>
		/// フェードカラー設定。α値は無視されます
		/// </summary>
		public void SetFadeColor(
			Color color
			)
		{
			SetFadeColor(color.r, color.g, color.b);
		}
		
		public void SetFadeImage( Texture texture )
		{
			_transition.SetFadeImage(texture);
		}
		public void SetFadeImage( Sprite sprite )
		{
			_transition.SetFadeImage(sprite.texture);
		}
		
		
		/// <summary>
		/// now loadingの表示
		/// </summary>
		/// <param name="isVisible"></param>
		public void SetVisibleNowLoading(bool isVisible)
		{
			if (isVisible) {
				//_nowLoading.gameObject.SetActive(true);
			} else {
				//_nowLoading.gameObject.SetActive(false);
			}
		}
		
		/// <summary>
		/// フェード処理中か
		/// </summary>
		/// <returns>true</returns>
		public bool NowFading()
		{
			return FadeState.Idle != _fadeState;
		}
		
		/// <summary>
		/// フェードイン
		/// </summary>
		/// <param name="time"></param>
		public void FadeIn(
            float time,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate
            )
		{
			_fadeEndTime = time;
			_fadeCurrentTime = 0.0f;
			if (0.01f >= _fadeEndTime) {
				_fadeEndTime = 0.01f;
			}
			_fadeState = FadeState.InStart;

            if (!object.ReferenceEquals(onStartFadeInDelegate, null)) this.OnStartFadeInEvent += onStartFadeInDelegate;
            if (!object.ReferenceEquals(onFinishedFadeInDelegate, null)) this.OnFinishedFadeInEvent += onFinishedFadeInDelegate;
        }
        public void FadeIn(
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate
            )
        {
            FadeIn(DefaultFadeTime, onStartFadeInDelegate, onFinishedFadeInDelegate);
        }
        public void FadeIn()
		{
			FadeIn(DefaultFadeTime, null, null);
		}

        /// <summary>
        /// シーンジャンプ
        /// </summary>
        /// <param name="name">シーン名</param>
        /// <param name="time">フェード時間</param>
        /// <param name="onStartFadeInDelegate"></param>
        /// <param name="onFinishedFadeInDelegate"></param>
        /// <param name="onStartFadeOutDelegate"></param>
        /// <param name="onFinishedFadeOutDelegate"></param>
        /// <returns></returns>
        public Option JumpScene(
            string name,
            float time,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            UnityEngine.Debug.Log("JumpScene = " + name);

            if (!object.ReferenceEquals(onStartFadeInDelegate, null)) this.OnStartFadeInEvent += onStartFadeInDelegate;
            if (!object.ReferenceEquals(onFinishedFadeInDelegate, null)) this.OnFinishedFadeInEvent += onFinishedFadeInDelegate;
            if (!object.ReferenceEquals(onStartFadeOutDelegate, null)) this.OnStartFadeOutEvent += onStartFadeOutDelegate;
            if (!object.ReferenceEquals(onFinishedFadeOutDelegate, null)) this.OnFinishedFadeOutEvent += onFinishedFadeOutDelegate;

            return _jumpScene(name, time);
        }

        public Option JumpScene(
            string name,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            return JumpScene(name, DefaultFadeTime, onStartFadeInDelegate, onFinishedFadeInDelegate, onStartFadeOutDelegate, onFinishedFadeOutDelegate);
        }

        public Option JumpScene(string name,float time)
		{
			return JumpScene(name, time, null, null, null, null);
		}

		public Option JumpScene(string name)
		{
            return JumpScene(name, DefaultFadeTime, null, null, null, null);
		}



        /// <summary>
        /// シーンコール
        /// </summary>
        /// <param name="name">シーン名</param>
        /// <param name="time">フェード時間</param>
        /// <param name="onStartFadeInDelegate"></param>
        /// <param name="onFinishedFadeInDelegate"></param>
        /// <param name="onStartFadeOutDelegate"></param>
        /// <param name="onFinishedFadeOutDelegate"></param>
        /// <returns></returns>
        public Option CallScene(
            string name,
            float time,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            // 現在のシーンをスタックに詰む
            if (!_current.IsNull())
            {
                _sceneStack.Add(_current);
            }

            UnityEngine.Debug.Log("CallScene = " + name);

            if (!object.ReferenceEquals(onStartFadeInDelegate, null)) this.OnStartFadeInEvent += onStartFadeInDelegate;
            if (!object.ReferenceEquals(onFinishedFadeInDelegate, null)) this.OnFinishedFadeInEvent += onFinishedFadeInDelegate;
            if (!object.ReferenceEquals(onStartFadeOutDelegate, null)) this.OnStartFadeOutEvent += onStartFadeOutDelegate;
            if (!object.ReferenceEquals(onFinishedFadeOutDelegate, null)) this.OnFinishedFadeOutEvent += onFinishedFadeOutDelegate;

            return _jumpScene(name, time);
        }

        public Option CallScene(
            string name,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            return CallScene(name, DefaultFadeTime, onStartFadeInDelegate, onFinishedFadeInDelegate, onStartFadeOutDelegate, onFinishedFadeOutDelegate);
        }

        public Option CallScene(string name,float time)
        {
            return CallScene(name, time, null, null, null, null);
        }

        public Option CallScene(string name)
        {
            return CallScene(name, DefaultFadeTime, null, null, null, null);
        }



        /// <summary>
        /// 前のシーンに戻る
        /// </summary>
        /// <param name="time">フェード時間</param>
        /// <param name="onStartFadeInDelegate"></param>
        /// <param name="onFinishedFadeInDelegate"></param>
        /// <param name="onStartFadeOutDelegate"></param>
        /// <param name="onFinishedFadeOutDelegate"></param>
        /// <returns></returns>
        public Option ReturnScene(
            float time,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            Assert.IsTrue(1 <= _sceneStack.Count, "シーン操作がおかしい");

            // 最後のシーンを取り出す
            Scene prev = _sceneStack[_sceneStack.Count - 1];
            _sceneStack.RemoveAt(_sceneStack.Count - 1);

            UnityEngine.Debug.Log("ReturnScene = " + prev.LevelName);

            if (!object.ReferenceEquals(onStartFadeInDelegate, null)) this.OnStartFadeInEvent += onStartFadeInDelegate;
            if (!object.ReferenceEquals(onFinishedFadeInDelegate, null)) this.OnFinishedFadeInEvent += onFinishedFadeInDelegate;
            if (!object.ReferenceEquals(onStartFadeOutDelegate, null)) this.OnStartFadeOutEvent += onStartFadeOutDelegate;
            if (!object.ReferenceEquals(onFinishedFadeOutDelegate, null)) this.OnFinishedFadeOutEvent += onFinishedFadeOutDelegate;

            return _jumpScene(prev.LevelName, time);
        }

        public Option ReturnScene(
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            return ReturnScene(DefaultFadeTime, onStartFadeInDelegate, onFinishedFadeInDelegate, onStartFadeOutDelegate, onFinishedFadeOutDelegate);
        }

        public Option ReturnScene(float time)
        {
            return ReturnScene(time, null, null, null, null);
        }

        public Option ReturnScene()
        {
            return ReturnScene(DefaultFadeTime, null, null, null, null);
        }



        /// <summary>
        /// シーンスタックをクリア
        /// </summary>
        public void ClearSceneStack()
		{
			_sceneStack.Clear();
		}


		
		/// <summary>
		/// シーンスタックにシーンを積む
		/// </summary>
		/// <param name="name"></param>	
		public void PushScene(string name)
		{
			Scene scene = Scene.Null;
			scene.LevelName = name;
			_sceneStack.Add(scene);
		}


		
		/// <summary>
		/// 指定のシーンがトップになるまでスタックを巻き戻す
		/// </summary>
		/// <param name="name"></param>
		public void RewindScene(string name)
		{
			if (0 == name.Length) {
				ClearSceneStack();
			} else {
				
				while (0 < _sceneStack.Count) {
					if (_sceneStack[_sceneStack.Count - 1].LevelName == name) {
						break;
					}
					_sceneStack.RemoveAt(_sceneStack.Count - 1);
				}
			}
		}
		
		#endregion
		
		
		
		
		#region 非公開メンバ関数
				
		/// <summary>
		/// フェード更新
		/// </summary>
		private void _updateFade()
		{
			switch (_fadeState) {
				
			case FadeState.InStart:
				_transition.SetActive(true);
				_transition.StartFadeIn(_fadeEndTime);
				
				_fadeState = FadeState.In;

				if( null != this.OnStartFadeInEvent ){
					this.OnStartFadeInEvent();
					this.OnStartFadeInEvent = null;
				}
				break;
				
			case FadeState.In:
				if ( !_transition.NowFading() ) {
					_transition.SetActive(false);
					_fadeState = FadeState.Idle;
					
					if( null != this.OnFinishedFadeInEvent ){
						this.OnFinishedFadeInEvent();
						this.OnFinishedFadeInEvent = null;
					}
				}
				break;
				
			case FadeState.OutStart:
				_transition.SetActive(true);
				_transition.StartFadeOut(_fadeEndTime);

				_fadeState = FadeState.Out;

				if( null != this.OnStartFadeOutEvent ){
					this.OnStartFadeOutEvent();
					this.OnStartFadeOutEvent = null;
				}
				break;
				
			case FadeState.Out:
				if ( !_transition.NowFading() ) {
					_fadeState = FadeState.Idle;

					if( null != this.OnFinishedFadeOutEvent ){
						this.OnFinishedFadeOutEvent();
						this.OnFinishedFadeOutEvent = null;
					}
						
					_fadeState = FadeState.Jump;
				}
				break;
				
			case FadeState.Jump:
				_fadeState = FadeState.Idle;
				
				// シーン切り替え
				UnityEngine.Application.LoadLevel(_current.LevelName);
				UnityEngine.Debug.Log("Change = " + _current.LevelName);				
				
				if (_option.UnloadUnusedAssets) {
					UnityEngine.Debug.Log("UnloadUnusedAssets...");
					Resources.UnloadUnusedAssets();
				}
				if (_option.GCCollect) {
					UnityEngine.Debug.Log("GCCollect...");
					long before = System.GC.GetTotalMemory(true) / 1024;
					System.GC.Collect();
					long after = System.GC.GetTotalMemory(true) / 1024;
					UnityEngine.Debug.Log(string.Format("before = {0} , after = {0}", before, after));
				}
				break;
			}
		}
		


		/// <summary>
		/// シーンジャンプ
		/// </summary>
		/// <param name="name">次のシーン</param>
		/// <param name="time">フェードアウト時間</param>
		private Option _jumpScene(
			string name,
			float time
			)
		{
			_prev = _current;
			_current.LevelName = name;
			_fadeEndTime = time;
			_fadeCurrentTime = 0.0f;
			if (0.01f >= _fadeEndTime) {
				_fadeEndTime = 0.01f;
			}
			_fadeState = FadeState.OutStart;
			_option.Reset();

			return _option;
		}

		
		
		/// <summary>
		/// Canvasを作成
		/// </summary>
		private void _createCanvas()
		{
			GameObject obj = new GameObject();
			obj.name = "Canvas";
			obj.transform.parent = this.transform;
			
			// Canvas
			_canvas = obj.AddComponent<Canvas>();
			Assert.IsNotNull(_canvas, "null==Canvas");
			_canvas.renderMode = RenderMode.ScreenSpaceOverlay;
			_canvas.sortingOrder = 100;//TODO 最前面にくるよう調整が必要　後から設定できるようにする
			
			// CanvasScaler
			_canvasScaler = obj.AddComponent<CanvasScaler>();
			Assert.IsNotNull(_canvasScaler, "null==CanvasScaler");
			_canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			_canvasScaler.referenceResolution = new Vector2((float)RuntimeEnvironment.Display.Width, (float)RuntimeEnvironment.Display.Height);
			_canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
			//デフォ_canvasScaler.referencePixelsPerUnit = 100;
			
			// GraphicRaycaster
			_graphicRaycaster = obj.AddComponent<GraphicRaycaster>();
			//デフォ_graphicRaycaster.ignoreReversedGraphics = true;
			//デフォ_graphicRaycaster.blockingObjects = GraphicRaycaster.BlockingObjects.None;
			//デフォ_graphicRaycaster.eventCamera.eventMask?  //TODO マスク設定出来るようにする
		}



		/// <summary>
		/// フェードを作成
		/// </summary>
		private void _createTransition()
		{
			var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 

			// Transitionインスタンスの作成
			#if USING_TRANSITION_PRO
			string prefabName = "ATK_TransitionPro";
			#else
			string prefabName = "ATK_Transition";
			#endif
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
			
			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			_transition = obj.GetComponent<ATK_TransitionInterface>();
			_transition.SetParentCanvas(_canvas);
			_transition.SetActive(false);
//			_transition.SetFadeColor(Color.white);
		}



		/// <summary>
		/// NowLoadingスプライトの作成
		/// </summary>
		private void _createNowLoadingImage()
		{
			
			GameObject obj = new GameObject();
			obj.name = "NowLoading Image";
			obj.transform.parent = _canvas.transform;
			obj.transform.localPosition = Vector3.zero;
			/*
            TODO
			UIAtlas.Sprite sprite = _atlas.GetSprite("NowLoading");
			Assert.IsNotNull(sprite,"null==sprite");

			obj.transform.localScale = new Vector3(
				sprite.outer.width,
				sprite.outer.height,
				1.0f
				);

			Sprite sp = obj.AddComponent<Sprite>();
			Assert.IsNotNull(sp,"null==sp");
			_nowLoading = sp;

			sp.atlas = _atlas;
			sp.spriteName = sprite.name;
			sp.depth = 1;

			*/
			obj.SetActive(false);
		}
		
		#endregion
		
	}
}

#pragma warning restore 414

