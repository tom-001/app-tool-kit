﻿/// <summary>
/// Twitter/Facebook/LINE にシェア
/// </summary>
/// iOS or Android only

/*
Social Connector v0.3.6
https://github.com/anchan828/social-connector
上記からダウンロード＆解凍＆同じフォルダ構成でプロジェクトに追加

iOSプロジェクトにLINEアイコン画像を容易する。デフォルトではダミー画像

そのうち統合されるかも？

//TODO キャプチャ画像をコルーチンでセーブ（保存完了確認）して　送信する


[iOS]
xcode
Build Phases -> SocialConnector.mm

*/


using UnityEngine;


namespace ATK
{

	public class ATK_SocialConnectorManager : ATK_SingletonMonoBehaviourFast<ATK_SocialConnectorManager>
	{

        string imagePath
        {
            get
            {
                return UnityEngine.Application.persistentDataPath + "/image.png";
            }
        }
        /*
        void OnGUI()
        {

            if (GUILayout.Button("<size=30><b>Take</b></size>", GUILayout.Height(60)))
            {
                UnityEngine.Application.CaptureScreenshot("image.png");
            }

            GUILayout.Space(60);

            ///=================
            /// Share
            ///=================

            if (GUILayout.Button("<size=30><b>Share</b></size>", GUILayout.Height(60)))
            {
                SocialConnector.Share("Social Connector", "https://www.google.co.jp", null);
            }
            if (GUILayout.Button("<size=30><b>Share Image</b></size>", GUILayout.Height(60)))
            {
                SocialConnector.Share("Social Connector", "https://github.com/anchan828/social-connector", imagePath);
            }
        }
        */
    }

}