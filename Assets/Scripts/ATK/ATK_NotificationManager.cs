﻿/// <summary>
/// 通知
/// </summary>
/// このクラスをインスタンス化して常駐させておけば、いつでも使える
/// インスタンスの使用できるタイミングは、インスタンス作成直後から


/// http://wiki.unity3d.com/index.php?title=CSharpNotificationCenter
/// CSharpNotificationCenter.csを参考に

using UnityEngine;
using System.Collections;
using System.Collections.Generic;



namespace ATK
{

	public class ATK_NotificationManager : ATK_SingletonMonoBehaviourFast<ATK_NotificationManager>
	{
		#region 公開メンバ変数

		#endregion


		#region 非公開メンバ変数

		/// <summary>
		/// 名前別オブサーバーリスト
		/// </summary>
		private static Hashtable _subjects = new Hashtable();

        #endregion

		/// <summary>
		/// オブサーバーの追加
		/// </summary>
		public void AddObserver(Component observer, string name) { AddObserver(observer, name, null); }
		public void AddObserver(Component observer, string name, Component sender)
		{
			if (string.IsNullOrEmpty (name)) {//assertでも良いかも？
				Debug.LogWarning("サブジェクト名がありません");
				return;
			}

			/// 指定名のサブジェクトがないなら、オブサーバー登録用リストを作成する
			if(_subjects[name] == null) {
				_subjects [name] = new List<Component> ();
			}

			var observerList = _subjects [name] as List<Component>;

			/// オブサーバーをリストに追加する
            if(!observerList.Contains(observer)) { observerList.Add(observer); }

		}

		/// <summary>
		/// オブサーバーの削除
		/// </summary>
		public void RemoveObserver(Component observer, string name)
		{
            var observerList = (List<Component>)_subjects[name];

            if (observerList != null) {
                if (observerList.Contains(observer)) { observerList.Remove(observer); }
				if (observerList.Count == 0) { _subjects.Remove(name); }
			}
		}

		public void PostNotification(Notification notification)
        {
            if (string.IsNullOrEmpty (notification.Name)) {
                Debug.Log ("Null name sent to PostNotification.");
                return;
            }

			var observerList = (List<Component>)_subjects [notification.Name];
            if (observerList == null) {
				Debug.Log ("observer list not found in PostNotification: " + notification.Name);
                return;
            }
                        

            var invalidObserverList = new List<Component> ();
                foreach (Component observer in invalidObserverList) {
                if (!observer) {
                    invalidObserverList.Add (observer);
                } else {
					observer.SendMessage (notification.Name, notification, SendMessageOptions.DontRequireReceiver);
                }
            }

            /// 無効なオブサーバーの削除
            foreach (Component observer in invalidObserverList) {
                observerList.Remove (observer);
            }
        }
				

		public class Notification
		{
			public Component Sender;
			public string Name;
			public Hashtable Data;
			public Notification(Component sender, string name) { Sender = sender; Name = name; Data = null; }
            public Notification(Component sender, string name, Hashtable data) { Sender = sender; Name = name; Data = data; }
		}

	}
}