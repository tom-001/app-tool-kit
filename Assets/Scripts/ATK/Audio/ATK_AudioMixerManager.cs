﻿/// <summary>
/// クラス
/// </summary>
/// BGMやSEなどの各(BGM/SystemSE/SE/Voice)PlayerManagerのインスタンスを扱って、それぞれの音を再生させる
/// このクラスをインスタンス化して常駐させておけば、簡単に音が鳴らせる
/// インスタンス化されると、サウンドの読込（同期）が行われる
/// インスタンスの使用できるタイミングは、インスタンス作成後のUpdate()以降







using UnityEngine;
using UnityEngine.Audio;
using System;
using System.Collections;


#pragma warning disable 414


namespace ATK.Audio
{
	public class ATK_AudioMixerManager : ATK_SingletonMonoBehaviourFast<ATK_AudioMixerManager>
	{
		
		#region 定数
		#endregion



		#region 非公開メンバ変数

		[SerializeField]
		private AudioMixer _audioMixer;

		private AudioMixerGroup _bgmAmg;
		private AudioMixerGroup _systemSeAmg;
		private AudioMixerGroup _seAmg;
		private AudioMixerGroup _voiceAmg;

		#endregion


		#region プロパティ

		public AudioMixer @AudioMixer { get {return _audioMixer;} }

		#endregion


		#region 非公開メンバ関数	
		#endregion



		#region 公開メンバ関数	

		void Start() 
		{
			AudioMixerGroup[] groups = _audioMixer.FindMatchingGroups("Master");
			Debug.Log(string.Format("{0}:{1}", groups.Length, groups[0].name));
		}



		/// <summary>
		/// BGMの再生
		/// </summary>
		public void PlayBGM(string name, float time=0.5f)
		{
		}

		/// <summary>
		/// BGMの停止
		/// </summary>
		public void StopBGM()
		{
		}

		public void StopBGM(float time)
		{
		}



		/// <summary>
		/// システムSEの再生
		/// </summary>
		public void PlaySystemSE(string name)
		{
		}

		/// <summary>
		/// システムSEの停止
		/// </summary>
		public void StopSystemSE()
		{
		}



		/// <summary>
		/// SEの再生
		/// </summary>
		public void PlaySE(string name)
		{
		}

		/// <summary>
		/// SEの停止
		/// </summary>
		public void StopSE()
		{
		}
		
		/// <summary>
		/// 音声の再生
		/// </summary>
		public void PlayVoice(string name)
		{
		}

		/// <summary>
		/// 音声の停止
		/// </summary>
		public void StopVoice()
		{
		}



		public float GetBGMVolume()
		{
			return 1f;
		}
		public void SetBGMVolume(float volume)
		{
		}
		
		public float GetSystemSEVolume()
		{
			return 1f;
		}
		public void SetSystemSEVolume(float volume)
		{
		}

		public float GetSEVolume()
		{
			return 1f;
		}
		public void SetSEVolume(float volume)
		{
		}

		public float GetVoiceVolume()
		{
			return 1f;
		}
		public void SetVoiceVolume(float volume)
		{
		}

		#endregion

	}

}


#pragma warning restore 414

