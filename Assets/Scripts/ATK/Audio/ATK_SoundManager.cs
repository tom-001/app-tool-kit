﻿/// <summary>
/// 簡易的に音を再生させるクラス
/// </summary>
/// BGMやSEなどの各(BGM/SystemSE/SE/Voice)PlayerManagerのインスタンスを扱って、それぞれの音を再生させる
/// このクラスをインスタンス化して常駐させておけば、簡単に音が鳴らせる
/// インスタンス化されると、サウンドの読込（同期）が行われる
/// インスタンスの使用できるタイミングは、インスタンス作成後のUpdate()以降

/*
AudioMixerを使う場合は、unity5以降
AudioMixer(Resources/Audio/Mixer/ATK_AudioMixer)はエディタ上で好みに編集する
AudioMixerのGroupは下記の順序で作成。
[Groups]
Master
 ├ BGM
 ├ SystemSE
 ├ SE
 └ Voice
*/


#define USING_BGM		// 使用するには、Rsources/Audio/BGM/ 下に、１つ以上の音源ファイルが必要です。
#define USING_SYSTEM_SE	// 使用するには、Rsources/Audio/SystemSE/ 下に、１つ以上の音源ファイルが必要です。
#define USING_SE		// 使用するには、Rsources/Audio/SE/ 下に、１つ以上の音源ファイルが必要です。
//#define USING_VOICE	// 使用するには、Rsources/Audio/Voice/ 下に、１つ以上の音源ファイルが必要です。
//#define USING_MIXER		// サウンドにエフェクトが必要ないなら無理に使用しない(アプリが軽くなるかも？)。

using UnityEngine;
using UnityEngine.Audio;
using System;
using System.Collections;


#pragma warning disable 414


namespace ATK.Audio
{
	public class ATK_SoundManager : ATK_SingletonMonoBehaviourFast<ATK_SoundManager>
	{
		
		#region 定数
		
		// サウンドデータベース識別子
		public static readonly string SOUND_BANK_ID_BGM = "BGM";
		public static readonly string SOUND_BANK_ID_SYSTEM_SE = "SYSTEM_SE";
		public static readonly string SOUND_BANK_ID_SE = "SE";
		public static readonly string SOUND_BANK_ID_VOICE = "VOICE";
		
		// Resources 以下のパス
		private static readonly string RESOURCES_PATH_BGM = "Audio/BGM";
		private static readonly string RESOURCES_PATH_SYSTEM_SE = "Audio/SystemSE";
		private static readonly string RESOURCES_PATH_SE = "Audio/SE";
		private static readonly string RESOURCES_PATH_VOICE = "Audio/Voice";
		private static readonly string RESOURCES_PATH_MIXER = "Audio/Mixer";
		
		#endregion



		#region 非公開メンバ変数

		// プレイヤー管理
		private ATK_Audio_BGMPlayerManager _bgmMgr;
		private ATK_Audio_SystemSEPlayerManager _systemSeMgr;
		private ATK_Audio_SEPlayerManager _seMgr;
		private ATK_Audio_VoicePlayerManager _voiceMgr;

		#if USING_MIXER
		// AudioMixerGroup
		// nullでも動く
		private AudioMixerGroup _masterAmg;
		private AudioMixerGroup _bgmAmg;
		private AudioMixerGroup _systemSeAmg;
		private AudioMixerGroup _seAmg;
		private AudioMixerGroup _voiceAmg;
		#endif

		#endregion


		#region プロパティ

		// プレイヤー管理
		public ATK_Audio_BGMPlayerManager BGMPlayerManager { get {return _bgmMgr;} }
		public ATK_Audio_SystemSEPlayerManager SystemSEPlayerManager { get {return _systemSeMgr;} }
		public ATK_Audio_SEPlayerManager SEPlayerManager { get {return _seMgr;} }
		public ATK_Audio_VoicePlayerManager VoicePlayerManager { get {return _voiceMgr;} }

		#endregion


		#region 非公開メンバ関数	
		#endregion



		#region 公開メンバ関数	

		void Start() 
		{
			_bgmMgr = ATK_Audio_BGMPlayerManager.Instance;
			_systemSeMgr = ATK_Audio_SystemSEPlayerManager.Instance;
			_seMgr = ATK_Audio_SEPlayerManager.Instance;
			_voiceMgr = ATK_Audio_VoicePlayerManager.Instance;

			/// 常駐サウンドクリップ読み込み
			#if USING_BGM
			// BGM読み込み
			SoundBankDB.Add(
				SOUND_BANK_ID_BGM,
				SoundBank.Create (RESOURCES_PATH_BGM)
				);
			#endif

			#if USING_SYSTEM_SE
			// システムSE読み込み
			SoundBankDB.Add(
				SOUND_BANK_ID_SYSTEM_SE,
				SoundBank.Create (RESOURCES_PATH_SYSTEM_SE)
				);
			#endif

			#if USING_SE
			// SE読み込み
			SoundBankDB.Add(
				SOUND_BANK_ID_SE,
				SoundBank.Create (RESOURCES_PATH_SE)
				);
			#endif

			#if USING_VOICE
			// 音声読み込み
			SoundBankDB.Add(
				SOUND_BANK_ID_VOICE,
				SoundBank.Create (RESOURCES_PATH_VOICE)
				);
			#endif


			#if USING_MIXER
			string mixerName = "ATK_AudioMixer";
			var mixer = ATK.Utilities.Resources.Load<AudioMixer>(RESOURCES_PATH_MIXER + "/" + mixerName); 
			ATK.Assert.IsTrue( mixer != null, mixerName + "がない");
			AudioMixerGroup[] groups = mixer.FindMatchingGroups(string.Empty);//"Master"を指定しても同じ
			_masterAmg	 = groups[0];
			_bgmAmg		 = groups[1];
			_systemSeAmg = groups[2];
			_seAmg		 = groups[3];
			_voiceAmg	 = groups[4];
			#endif
		}



		/// <summary>
		/// BGMの再生
		/// </summary>
		public void PlayBGM(string name, float time=0.5f)
		{
			#if USING_BGM
			_bgmMgr.StopAll(time);
			var player = _bgmMgr.GetFree();
			var req = player.Create();
			req.Clip = SoundBankDB.Find(SOUND_BANK_ID_BGM).GetAudioClip(name);
			req.Loop = true;
			#if USING_MIXER
   			req.Output = _bgmAmg;
			#endif
			player.Add(req);	
			#endif
		}

		/// <summary>
		/// BGMの停止
		/// </summary>
		public void StopBGM()
		{
			_bgmMgr.StopAll();
		}

		public void StopBGM(float time)
		{
			_bgmMgr.StopAll(time);
		}



		/// <summary>
		/// システムSEの再生
		/// </summary>
		public void PlaySystemSE(string name)
		{
			#if USING_SYSTEM_SE
			var player = _systemSeMgr.GetFree();
			var req = player.Create();
			req.Clip = SoundBankDB.Find(SOUND_BANK_ID_SYSTEM_SE).GetAudioClip(name);
			#if USING_MIXER
			req.Output = _systemSeAmg;
			#endif
			player.Add(req);	
			#endif
		}

		/// <summary>
		/// システムSEの停止
		/// </summary>
		public void StopSystemSE()
		{
			#if USING_SYSTEM_SE
			_systemSeMgr.StopAll();
			#endif
		}



		/// <summary>
		/// SEの再生
		/// </summary>
		public void PlaySE(string name)
		{
			#if USING_SE
			var player = _seMgr.GetFree();
			var req = player.Create();
			req.Clip = SoundBankDB.Find(SOUND_BANK_ID_SE).GetAudioClip(name);
			#if USING_MIXER
			req.Output = _seAmg;
			#endif
			player.Add(req);	
			#endif
		}

		/// <summary>
		/// SEの停止
		/// </summary>
		public void StopSE()
		{
			_seMgr.StopAll();
		}
		
		/// <summary>
		/// 音声の再生
		/// </summary>
		public void PlayVoice(string name)
		{
			#if USING_VOICE
			var player = _voiceMgr.GetFree();
			var req = player.Create();
			req.Clip = SoundBankDB.Find(SOUND_BANK_ID_VOICE).GetAudioClip(name);
			#if USING_MIXER
			req.Output = _voiceAmg;
			#endif
			player.Add(req);	
			#endif
		}

		/// <summary>
		/// 音声の停止
		/// </summary>
		public void StopVoice()
		{
			_voiceMgr.StopAll();
		}



		public float GetBGMVolume()
		{
			return _bgmMgr.GetMasterVolume();
		}
		public void SetBGMVolume(float volume)
		{
			_bgmMgr.SetMasterVolume(volume);
		}
		
		public float GetSystemSEVolume()
		{
			return _systemSeMgr.GetMasterVolume();
		}
		public void SetSystemSEVolume(float volume)
		{
			_systemSeMgr.SetMasterVolume(volume);
		}

		public float GetSEVolume()
		{
			return _seMgr.GetMasterVolume();
		}
		public void SetSEVolume(float volume)
		{
			_seMgr.SetMasterVolume(volume);
		}

		public float GetVoiceVolume()
		{
			return _voiceMgr.GetMasterVolume();
		}
		public void SetVoiceVolume(float volume)
		{
			_voiceMgr.SetMasterVolume(volume);
		}

		#endregion

	}

}


#pragma warning restore 414

