﻿/*


再生識別子クラス

各リソースにある音源ファイル名と同じにする

TODO
このクラスを自動生成する

*/

using UnityEngine;
using System;
using System.Collections;


namespace ATK.Audio
{

	public class SoundID
	{
		#region アプリ毎に追加
		
		//////////////////////////////
		/// ATK
		/// ATK/Resources/Audio/～
		//////////////////////////////
		// ATK/Resources/Audio/SystemSe/
		public static readonly string SYS_SE_OK = "Ok";
		public static readonly string SYS_SE_CANCEL = "Cancel";
		public static readonly string SYS_SE_OPEN = "Open";

		// ATK/Resources/Audio/BGM/

		// ATK/Resources/Audio/SE/
		


		//////////////////////////////
		/// 追加
		/// Resources/Audio/～
		//////////////////////////////

		// Resources/Audio/SystemSE/

		// Resources/Audio/SE/

		// Resources/Audio/BGM/
		public static readonly string BGM_TITLE = "Title";
		public static readonly string BGM_GAME = "Game";

		#endregion
	}

}

