﻿using UnityEngine;

namespace ATK.Localization
{
	public partial class LanguagePostfixEnvironment
	{
		partial void _setup()
		{
			// 基本言語変更
			_baseLanguage = SystemLanguage.Japanese;
			/*
			// 基本言語変更
			_baseLanguage = SystemLanguage.English;
			
			// Postfix テーブルの追加
			_postfixs.Add(SystemLanguage.Chinese, "Chinese");
			
			// Postfix テーブルの書き換え
			_postfixs.Clear();
			_postfixs.Add(SystemLanguage.Chinese, "Chinese");
			_postfixs.Add(SystemLanguage.English, "English");
			*/			
		}		
	}		
} 

