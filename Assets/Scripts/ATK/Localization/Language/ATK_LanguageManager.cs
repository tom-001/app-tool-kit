﻿/// <summary>
/// 言語クラスを管理するクラス（シングルトン）
/// </summary>
/// 各言語ファイルを一括ロード
/// このクラスをインスタンス化して常駐させておけば、言語変数を扱うことができる
/// インスタンス化されると、言語ファイルの読込（同期）が行われる
/// インスタンスの使用できるタイミングは、インスタンス作成直後から


using UnityEngine;
using System.Collections;


namespace ATK.Localization
{
	public class ATK_LanguageManager : ATK_SingletonMonoBehaviourFast<ATK_LanguageManager>
	{
		#region 定数

		// 言語ファイル名
		private static readonly string LN_NAME_CONFIG     = "Ln_Config";
		private static readonly string LN_NAME_GAME       = "Ln_Game";
		private static readonly string LN_NAME_HELP       = "Ln_Help";
		private static readonly string LN_NAME_MESSAGEBOX = "Ln_MessageBox";
		private static readonly string LN_NAME_STAFF      = "Ln_Staff";

		// Resources 以下のパス
		private static readonly string RESOURCES_PATH_LANGUAGE = "Localization/Language/";

		private SystemLanguage _currentLanguage = SystemLanguage.Unknown;

		#endregion



		#region 公開メンバ変数

		public Ln_MessageBox MsgBox;
		public Ln_Game Game;
		public Ln_Config Config;
		public Ln_Staff Staff;
		public Ln_Help Help;

		#endregion



		#region プロパティ
		
		public SystemLanguage CurrentLanguege { get {return _currentLanguage;} }

		#endregion



		#region 公開メンバ関数

		public void LoadLanguage( SystemLanguage language )
		{
			if ( _currentLanguage == language ) return;
			_currentLanguage = language;

			Config = Language.LoadXml<Ln_Config>(RESOURCES_PATH_LANGUAGE     + LN_NAME_CONFIG,     language);
			Game   = Language.LoadXml<Ln_Game>(RESOURCES_PATH_LANGUAGE       + LN_NAME_GAME,       language);
			Help   = Language.LoadXml<Ln_Help>(RESOURCES_PATH_LANGUAGE       + LN_NAME_HELP,       language);
			MsgBox = Language.LoadXml<Ln_MessageBox>(RESOURCES_PATH_LANGUAGE + LN_NAME_MESSAGEBOX, language);
			Staff  = Language.LoadXml<Ln_Staff>(RESOURCES_PATH_LANGUAGE      + LN_NAME_STAFF,      language);
		}

		#endregion



		#region 非公開メンバ関数

		override protected void Awake() 
		{
			base.Awake();
			LoadLanguage( Language.LanguagePostfix.BaseLanguage );
		}
		

		#endregion

	}	
}
