﻿namespace ATK.Localization
{
	public class Ln_Game
	{
		/// <summary>
		/// 
		/// </summary>
		public string 開始情報_ゲームレベル;

		/// <summary>
		/// 
		/// </summary>
		public string 開始情報_目標回数;

		/// <summary>
		/// 
		/// </summary>
		public string 結果_クリア時間;

		/// <summary>
		/// 
		/// </summary>
		public string 結果_やり直し回数;

		/// <summary>
		/// 
		/// </summary>
		public string 結果_残り回数;

		/// <summary>
		/// 
		/// </summary>
		public string 結果_残り回数なし;

		/// <summary>
		/// 
		/// </summary>
		public string 結果_クリア時間ラベル;

		/// <summary>
		/// 
		/// </summary>
		public string 結果_やり直し回数ラベル;

		/// <summary>
		/// 
		/// </summary>
		public string 結果_残り回数ラベル;

		/// <summary>
		/// 
		/// </summary>
		public string 結果_評価ラベル;

	}
}
