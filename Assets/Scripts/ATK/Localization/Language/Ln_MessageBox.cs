﻿namespace ATK.Localization
{
	public class Ln_MessageBox
	{
		/// <summary>
		/// 
		/// </summary>
		//　ボタン
		public string ボタン_OK;
		public string ボタン_キャンセル;
		public string ボタン_中止;
		public string ボタン_再試行;
		public string ボタン_無視;
		public string ボタン_はい;
		public string ボタン_いいえ;
		public string ボタン_削除;
		public string ボタン_上書き;
		
		
		// インジケーターの簡易メッセージ
		public string 伝言_ギャラリー選択;
		public string 伝言_カメラ撮影;
		public string 伝言_アクション再生中;
		public string 伝言_レンダリング中;
		public string 伝言_ギャラリー保存中;
		public string 伝言_データ保存中;
		
		
		// 汎用表題
		public string 表題_確認;
		public string 表題_お知らせ;
		public string 表題_エラー;
		public string 表題_警告;
		public string 表題_通知;
		
		
		// 汎用伝言 -->
		public string 伝言_エラー;
		public string 伝言_読込エラー;
		public string 伝言_保存エラー;
		
		
		// メインメニュー
		public string 伝言_アルバム未選択;
		public string 伝言_アルバム削除;
		public string 伝言_写真未選択;
		public string 伝言_写真削除;
		public string 伝言_写真エラー;
		public string 伝言_写真無し;
		
		
		// 編集
		public string 伝言_全て削除;
		public string 伝言_レイヤー未選択;
		public string 伝言_要入力;
		
		
		// プレビュー
		public string 伝言_データ未保存;
		
	}
}
