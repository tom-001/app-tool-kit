﻿namespace ATK.Localization
{
	public class Ln_Help
	{
		/// <summary>
		/// 
		/// </summary>
		public string 基本操作;

		/// <summary>
		/// 
		/// </summary>
		public string 基本操作00;

		/// <summary>
		/// 
		/// </summary>
		public string 基本操作01;

		/// <summary>
		/// 
		/// </summary>
		public string 基本操作02;

		/// <summary>
		/// やり直し
		/// </summary>
		public string 基本操作03;

		/// <summary>
		/// メニュー
		/// </summary>
		public string 基本操作04;

		/// <summary>
		/// 再開
		/// </summary>
		public string 基本操作05;

		/// <summary>
		/// リスタート
		/// </summary>
		public string 基本操作06;

		/// <summary>
		/// やめる
		/// </summary>
		public string 基本操作07;

		/// <summary>
		/// 目標
		/// </summary>
		public string 基本操作08;

		/// <summary>
		/// 
		/// </summary>
		public string ワープ説明;

		/// <summary>
		/// 
		/// </summary>
		public string ワープ説明00;

		/// <summary>
		/// 
		/// </summary>
		public string ワープ説明01;

		/// <summary>
		/// 
		/// </summary>
		public string 無効パネル;

		/// <summary>
		/// 
		/// </summary>
		public string 無効パネル説明;

	}
}
