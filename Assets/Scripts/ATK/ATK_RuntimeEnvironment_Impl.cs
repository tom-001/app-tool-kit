﻿using UnityEngine;

namespace ATK
{
    /*
	public class AppEnvironment
	{
		public static readonly string ProductName = "ProductName";
	}

	public class LocalizationEnvironment
	{
		public static readonly SystemLanguage BaseLanguage = SystemLanguage.Japanese;
	}
	*/

    public partial class DisplayEnvironment
	{
		partial void _setup()
		{
            /*
            ┌─────────────────┬──────┬───────────┬──────┬───────────┐
            │    デバイス      │インチ│   画素数   │解像度│アスペクト比│
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPhone 4/4S      │ 3.5  │  960x 640 │326ppi│  3:2      │
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPhone 5/5S/5C   │ 4    │ 1136x 640 │326ppi│ 16:9(ほぼ)│
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPhone 6         │ 4.7  │ 1334x 750 │326ppi│ 16:9(ほぼ)│
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPhone 6 Plus    │ 5.5  │ 1920x1080 │401ppi│ 16:9      │
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPad 1/2         │ 9.7  │ 1024x 768 │132ppi│  4:3      │
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPad 3 Retina    │ 9.7  │ 2048x1536 │264ppi│  4:3      │
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPad Air         │ 9.7  │ 2048x1536 │264ppi│  4:3      │
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPad Air 2       │ 9.7  │ 2048x1536 │264ppi│  4:3      │
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPad mini        │ 7.9  │ 1024x 768 │163ppi│  4:3      │
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPad mini Retina │ 7.9  │ 2048x1536 │326ppi│  4:3      │
            ├─────────────────┼──────┼───────────┼──────┼───────────┤
            │iPad mini 3      │ 7.9  │ 2048x1536 │326ppi│  4:3      │
            └─────────────────┴──────┴───────────┴──────┴───────────┘
            */
            _width = 960;
			_height = 640;
			_frameRate = 60;
		}	

	}
	


	public partial class AudioEnvironment
	{
		partial void _setup()
		{			
			_numBGMChannels = 1;
			_numSEChannels = 8;
			_numSystemSEChannels = 4;
			_numVoiceChannels = 1;
		}
	}

} 