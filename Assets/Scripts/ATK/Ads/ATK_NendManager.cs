﻿/// <summary>
/// nend (バナー/インタースティシャル/アイコン広告)
/// </summary>
/// iOS or Android only
/// 完了後のコールバックを設定できる
/// このクラスをインスタンス化して常駐させておけば、いつでも使える
/// 広告の使用できるタイミングは、IsReady()で確認
/*
==================================================
　導入
==================================================
1)nend　へ登録
　http://nend.net/
　仮登録後して、審査。審査が通れば数日で本登録通知あり。
　仮登録時にアプリ内容（アプリ名/ストアURL/説明）を記入するが、適当でよいみたい。
　本登録後にログインでき、サービスが利用できる。

2)マニュアルに沿ってセットアップ
　unityパッケージダウンロード＆インポートなど、本登録後にログインが必要

・マニュアル
　https://github.com/fan-ADN/nendSDK-Unity/wiki/nendSDK-Unity-Plugin-%E8%A8%AD%E5%AE%9A%E3%82%AC%E3%82%A4%E3%83%89

 




==================================================
　テストモード
==================================================
テスト用のIDが決められているので、それを使う
/// Androidアプリ向け表示テスト用ID
/// サイズ               ｜ apiKey                                   ｜ spotID
/// 320 × 50            ｜ c5cb8bc474345961c6e7a9778c947957ed8e1e4f ｜ 3174
/// 320 × 100           ｜ 8932b68d22d1d32f5d7251f9897a6aa64117995e ｜ 71000
/// 300 × 100           ｜ 1e36d1183d1ab66539998df4170a591c13028416 ｜ 71001
/// 300 × 250           ｜ 499f011dbec5d37cfa388b749aed2bfff440a794 ｜ 70357
/// 728 × 90            ｜ 02e6e186bf0183105fba7ce310dafe68ac83fb1c ｜ 71002
/// インタースティシャル ｜ 8c278673ac6f676dae60a1f56d16dad122e23516 ｜ 213206
/// アイコン             ｜ 0c734134519f25412ae9a9bff94783b81048ffbe ｜ 101282

/// iOS アプリ向け表示テスト用 ID
/// サイズ               ｜ apiKey                                   ｜ spotID
/// 320 x 50             ｜ a6eca9dd074372c898dd1df549301f277c53f2b9 ｜ 3172
/// 320 x100             ｜ eb5ca11fa8e46315c2df1b8e283149049e8d235e ｜ 70996
/// 300 x100             ｜ 25eb32adddc4f7311c3ec7b28eac3b72bbca5656 ｜ 70998
/// 300 x250             ｜ 88d88a288fdea5c01d17ea8e494168e834860fd6 ｜ 70356
/// 728 x 90             ｜ 2e0b9e0b3f40d952e6000f1a8c4d455fffc4ca3a ｜ 70999
/// インタースティシャル ｜ 308c2499c75c4a192f03c02b2fcebd16dcb45cc9 ｜ 213208





==================================================
　ポイント
==================================================




==================================================
　参考
==================================================
・マニュアル
　https://github.com/fan-ADN/nendSDK-Unity/wiki/nendSDK-Unity-Plugin-%E8%A8%AD%E5%AE%9A%E3%82%AC%E3%82%A4%E3%83%89

・サイトの説明（アプリの登録とか）
　http://nelog.jp/how-to-use-nend

 ・このスクリプトについて
　Asset/ThirdParty/Nend/AD/NendAdBanner.cs
　Asset/ThirdParty/Nend/AD/NendAdIcon.cs
　Asset/ThirdParty/Nend/AD/NendAdInterstital.cs
　を参考に作成


 



 
*/

/*

//using System;
//using System.Collections;
using UnityEngine;

#if UNITY_IOS || UNITY_ANDROID
using NendUnityPlugin.AD;
using NendUnityPlugin.Layout;
using NendUnityPlugin.Common;
#endif


#pragma warning disable 414


namespace ATK.Ads
{
    public class ATK_NendManager : ATK_SingletonMonoBehaviourFast<ATK_NendManager>
    {
        /// <summary>
        /// テストモード
        /// </summary>
        public bool EnableTestMode = true;

        private NendAdBanner _bannerView = null;
        private NendAdInterstitial _interstitialView = null;
        private NendAdIcon _iconView = null;

#if UNITY_ANDROID
        /// Androidアプリ向け表示テスト用ID
        private static readonly string TestApiKey_320x50       = "c5cb8bc474345961c6e7a9778c947957ed8e1e4f";
        private static readonly string TestApiKey_320x100      = "8932b68d22d1d32f5d7251f9897a6aa64117995e";
        private static readonly string TestApiKey_300x100      = "1e36d1183d1ab66539998df4170a591c13028416";
        private static readonly string TestApiKey_300x250      = "499f011dbec5d37cfa388b749aed2bfff440a794";
        private static readonly string TestApiKey_728x90       = "02e6e186bf0183105fba7ce310dafe68ac83fb1c";
        private static readonly string TestApiKey_Interstitial = "8c278673ac6f676dae60a1f56d16dad122e23516";
        private static readonly string TestApiKey_Icon         = "0c734134519f25412ae9a9bff94783b81048ffbe";
        private static readonly string TestSpotID_320x50       = "3174";
        private static readonly string TestSpotID_320x100      = "71000";
        private static readonly string TestSpotID_300x100      = "71001";
        private static readonly string TestSpotID_300x250      = "70357";
        private static readonly string TestSpotID_728x90       = "71002";
        private static readonly string TestSpotID_Interstitial = "213206";
        private static readonly string TestSpotID_Icon         = "101282";
#elif UNITY_IOS
        /// iOS アプリ向け表示テスト用 ID
        private static readonly string TestApiKey_320x50       = "a6eca9dd074372c898dd1df549301f277c53f2b9";
        private static readonly string TestApiKey_320x100      = "eb5ca11fa8e46315c2df1b8e283149049e8d235e";
        private static readonly string TestApiKey_300x100      = "25eb32adddc4f7311c3ec7b28eac3b72bbca5656";
        private static readonly string TestApiKey_300x250      = "88d88a288fdea5c01d17ea8e494168e834860fd6";
        private static readonly string TestApiKey_728x90       = "2e0b9e0b3f40d952e6000f1a8c4d455fffc4ca3a";
        private static readonly string TestApiKey_Interstitial = "308c2499c75c4a192f03c02b2fcebd16dcb45cc9";
        private static readonly string TestApiKey_Icon         = "cant not use";
        private static readonly string TestSpotID_320x50       = "3172";
        private static readonly string TestSpotID_320x100      = "70996";
        private static readonly string TestSpotID_300x100      = "70998";
        private static readonly string TestSpotID_300x250      = "70356";
        private static readonly string TestSpotID_728x90       = "70999";
        private static readonly string TestSpotID_Interstitial = "213208";
        private static readonly string TestSpotID_Icon         = "cant not use";
#endif
        void Awake()
        {
            base.Awake();

            //            _bannerView = NendUtils.GetBannerComponent(this.gameObject.name);
            _bannerView = GetComponentInChildren<NendAdBanner>();
            Assert.IsNotNull(_bannerView, "NendAdBanner コンポーネントがアタッチされていない");

            _interstitialView = GetComponentInChildren<NendAdInterstitial>();
            Assert.IsNotNull(_interstitialView, "NendAdInterstitial コンポーネントがアタッチされていない");

            _iconView = GetComponentInChildren<NendAdIcon>();
            Assert.IsNotNull(_iconView, "NendAdIcon コンポーネントがアタッチされていない");

            /// テストモードの場合は、強制的にテスト用IDを設定する
            if (EnableTestMode)
            {
                //switch(_bannerView.size)

            }
        }

    }
}




#pragma warning restore 414
*/