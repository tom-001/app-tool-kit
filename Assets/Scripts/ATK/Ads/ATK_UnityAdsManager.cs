﻿/// <summary>
/// unity Ads (動画広告)
/// </summary>
/// 
/// unity5.2 以降
/// iOS or Android only
/// 完了後のコールバックを設定できる
/// このクラスをインスタンス化して常駐させておけば、いつでも使える
/// 広告の使用できるタイミングは、IsReady()で確認
/*
*/

using System;
using System.Collections;
using UnityEngine;
#if UNITY_IOS || UNITY_ANDROID
using UnityEngine.Advertisements;
#endif

namespace ATK.Ads
{

    public class ATK_UnityAdsManager : ATK_SingletonMonoBehaviourFast<ATK_UnityAdsManager>
    {
        public static string ZoneID_Video { get; private set; }
        public static string ZoneID_RewardVideo { get; private set; }

        /// <summary>
        /// ZoneID（Zone ID は サイトから確認する。ダッシュボードでは Placement ID）
        /// </summary>
        /// Android
        [SerializeField] private string _zoneID_Video_Android = "video";
        [SerializeField] private string _zoneID_RewardVideo_Android = "rewardedVideo";
        /// iOS
        [SerializeField] private string _zoneID_Video_iOS = "video";
        [SerializeField] private string _zoneID_RewardVideo_iOS = "rewardedVideo";

        private static Action _onFinished;
        private static Action _onSkipped;
        private static Action _onFailed;
        /// <summary>
        /// 広告の見る見ない関係なく、広告終了で常に呼ばれる
        /// </summary>
        private static Action _onContinue;

#if UNITY_IOS || UNITY_ANDROID

        void Start()
        {
#if UNITY_ANDROID
            ZoneID_Video = _zoneID_Video_Android;
            ZoneID_RewardVideo = _zoneID_RewardVideo_Android;
#elif UNITY_IOS
            ZoneID_Video = _zoneID_Video_iOS;
            ZoneID_RewardVideo = _zoneID_RewardVideo_iOS;
#endif

            if (Advertisement.testMode && !Debug.isDebugBuild)
            {
                //Debug.LogWarning("Development Build must be enabled in Build Settings to enable test mode for Unity Ads.");
                Debug.LogError("Unity Adsのテストモードを使用する場合、デバッグビルドにしてください");
            }

            bool isTestModeEnabled = Debug.isDebugBuild && Advertisement.testMode;
            Debug.Log(string.Format("Precheck done. Initializing Unity Ads for game ID {0} with test mode {1}...",
                                    Advertisement.gameId, isTestModeEnabled ? "enabled" : "disabled"));

           StartCoroutine(LogWhenUnityAdsIsInitialized());
        }

        private IEnumerator LogWhenUnityAdsIsInitialized()
        {
            float initStartTime = Time.time;

            do yield return new WaitForSeconds(0.1f);
            while (!Advertisement.isInitialized);

            Debug.Log(string.Format("Unity Ads was initialized in {0:F1} seconds.", Time.time - initStartTime));
            yield break;
        }



        public bool IsShowing { get { return Advertisement.isShowing; } }
        public bool IsSupported { get { return Advertisement.isSupported; } }
        public bool IsInitialized { get { return Advertisement.isInitialized; } }

		/// <summary>
		/// 動画広告の準備ができたかどうか
		/// </summary>
        public bool IsReady()
        {
            return IsReady(null);
        }
        public bool IsReady(string zoneID)
        {
            if (string.IsNullOrEmpty(zoneID)) zoneID = null;

            return Advertisement.IsReady(zoneID);
        }
		public bool IsReadyForSkippableVideo()
		{
			return IsReady(ZoneID_Video);
		}
		public bool IsReadyForRewardVideo()
		{
			return IsReady(ZoneID_RewardVideo);
		}

			
		/// <summary>
		/// 動画広告表示
		/// </summary>
        public void ShowVideo()
        {
			ShowVideo(null, null, null, null, null);
        }
        public void ShowVideo(string zoneID)
        {
			ShowVideo(zoneID, null, null, null, null);
        }
        public void ShowVideo(string zoneID, Action onFinished)
        {
			ShowVideo(zoneID, onFinished, null, null, null);
        }
        public void ShowVideo(string zoneID, Action onFinished, Action onSkipped)
        {
			ShowVideo(zoneID, onFinished, onSkipped, null, null);
        }
        public void ShowVideo(string zoneID, Action onFinished, Action onSkipped, Action onFailed)
        {
			ShowVideo(zoneID, onFinished, onSkipped, onFailed, null);
        }
        public void ShowVideo(string zoneID, Action onFinished, Action onSkipped, Action onFailed, Action onContinue)
        {
            if (string.IsNullOrEmpty(zoneID)) zoneID = null;

            _onFinished = onFinished;
            _onSkipped = onSkipped;
            _onFailed = onFailed;
            _onContinue = onContinue;

            if (Advertisement.IsReady(zoneID))
            {
                Debug.Log("Showing ad now...");

                ShowOptions options = new ShowOptions();
                options.resultCallback = HandleShowResult;

                Advertisement.Show(zoneID, options);
            }
            else
            {
                Debug.LogWarning(string.Format("Unable to show ad. The ad placement zone {0} is not ready.",
                                               object.ReferenceEquals(zoneID, null) ? "default" : zoneID));
            }
        }

		/// <summary>
		/// 動画広告表示　スキップ可能
		/// </summary>
		public void ShowSkippableVideo()
		{
			ShowVideo(ZoneID_Video, null, null, null, null);
		}
		public void ShowSkippableVideo(Action onFinished)
		{
			ShowVideo(ZoneID_Video, onFinished, null, null, null);
		}
		public void ShowSkippableVideo(Action onFinished, Action onSkipped)
		{
			ShowVideo(ZoneID_Video, onFinished, onSkipped, null, null);
		}
		public void ShowSkippableVideo(Action onFinished, Action onSkipped, Action onFailed)
		{
			ShowVideo(ZoneID_Video, onFinished, onSkipped, onFailed, null);
		}
		public void ShowSkippableVideo(Action onFinished, Action onSkipped, Action onFailed, Action onContinue)
		{
			ShowVideo(ZoneID_Video, onFinished, onSkipped, onFailed, onContinue);
		}

		/// <summary>
		/// 動画広告表示　スキップ不可能
		/// </summary>
		public void ShowRewardVideo()
		{
			ShowVideo(ZoneID_RewardVideo, null, null, null, null);
		}
		public void ShowRewardVideo(Action onFinished)
		{
			ShowVideo(ZoneID_RewardVideo, onFinished, null, null, null);
		}
		public void ShowRewardVideo(Action onFinished, Action onSkipped)
		{
			ShowVideo(ZoneID_RewardVideo, onFinished, onSkipped, null, null);
		}
		public void ShowRewardVideo(Action onFinished, Action onSkipped, Action onFailed)
		{
			ShowVideo(ZoneID_RewardVideo, onFinished, onSkipped, onFailed, null);
		}
		public void ShowRewardVideo(Action onFinished, Action onSkipped, Action onFailed, Action onContinue)
		{
			ShowVideo(ZoneID_RewardVideo, onFinished, onSkipped, onFailed, onContinue);
		}

        private void HandleShowResult(ShowResult result)
        {
            switch (result)
            {
                case ShowResult.Finished:
                    Debug.Log("The ad was successfully shown.");
                    if (!object.ReferenceEquals(_onFinished, null)) _onFinished();
                    _onFinished = null;
                    break;
                case ShowResult.Skipped:
                    Debug.LogWarning("The ad was skipped before reaching the end.");
                    if (!object.ReferenceEquals(_onSkipped, null)) _onSkipped();
                    _onSkipped = null;
                    break;
                case ShowResult.Failed:
                    Debug.LogError("The ad failed to be shown.");
                    if (!object.ReferenceEquals(_onFailed, null)) _onFailed();
                    _onFailed = null;
                    break;
            }

            if (!object.ReferenceEquals(_onContinue, null)) _onContinue();
        }
        

#else
	//--- Properties and Methods for Unsuported Platforms
	
	void Awake ()
	{
		Debug.LogWarning("Unity Ads is not supported under the current build platform.");
	}

	public bool isShowing { get { return false; }}
	public bool isSupported { get { return false; }}
	public bool isInitialized { get { return false; }}

	public bool IsReady () { return false; }
	public bool IsReady (string zoneID) { return false; }
	public bool IsReadyForSkippableVideo() { return false; }
	public bool IsReadyForRewardVideo() { return false; }


	public void ShowVideo () 
	{
		Debug.LogError("Failed to show ad. Unity Ads is not supported under the current build platform.");
	}
	public void ShowVideo (string zoneID) { ShowVideo(); }
	public void ShowVideo (string zoneID, Action onFinished) { ShowVideo(); }
	public void ShowVideo (string zoneID, Action onFinished, Action onSkipped) { ShowVideo(); }
	public void ShowVideo (string zoneID, Action onFinished, Action onSkipped, Action onFailed) { ShowVideo(); }
	public void ShowVideo (string zoneID, Action onFinished, Action onSkipped, Action onFailed, Action onContinue) { ShowVideo(); }

	public void ShowSkippableVideo () { ShowVideo(); }
	public void ShowSkippableVideo (Action onFinished) { ShowVideo(); }
	public void ShowSkippableVideo (Action onFinished, Action onSkipped) { ShowVideo(); }
	public void ShowSkippableVideo (Action onFinished, Action onSkipped, Action onFailed) { ShowVideo(); }
	public void ShowSkippableVideo (Action onFinished, Action onSkipped, Action onFailed, Action onContinue) { ShowVideo(); }

	public void ShowRewardVideo () { ShowVideo(); }
	public void ShowRewardVideo (Action onFinished) { ShowVideo(); }
	public void ShowRewardVideo (Action onFinished, Action onSkipped) { ShowVideo(); }
	public void ShowRewardVideo (Action onFinished, Action onSkipped, Action onFailed) { ShowVideo(); }
	public void ShowRewardVideo (Action onFinished, Action onSkipped, Action onFailed, Action onContinue) { ShowVideo(); }

#endif

}
}