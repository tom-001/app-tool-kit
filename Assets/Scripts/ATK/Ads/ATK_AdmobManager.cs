﻿/// <summary>
/// Google Admob (バナー広告/インタースティシャル広告)
/// </summary>
/// iOS or Android only(他のプラットフォームは無視）
/// 完了後のコールバックを設定できる
/// このクラスをインスタンス化して常駐させておけば、いつでも使える
/// 広告の使用できるタイミングは、IsReady()で確認
/// バナーを二つ配置できるようにした
/*


==================================================
　簡易サンプル
==================================================
デモ：Assets/ATK/Demo/Ads/Demo_Admob
プレファブ：Assets/ATK/Resources/Prefab/ATK_AdmobManager.prefab
　※各ユニットIDやテストデバイスIDなどの変更があったら、プレファブを修正する
特徴：ATK_AdmobManagerは、バナー広告２つ、インターステーシャル広告１つ表示可能

 
*/

using System;
using System.Collections;
using UnityEngine;

#if UNITY_IOS || UNITY_ANDROID
using GoogleMobileAds;
using GoogleMobileAds.Api;
#endif

#pragma warning disable 414


namespace ATK.Ads
{

	public class GoogleMobileAdsDemoHandler : IDefaultInAppPurchaseProcessor
	{
		private readonly string[] validSkus = { "android.test.purchased" };

		//Will only be sent on a success.
		public void ProcessCompletedInAppPurchase(IInAppPurchaseResult result)
		{
			result.FinishPurchase();
			ATK_AdmobManager.OutputMessage = "Purchase Succeeded! Credit user here.";
		}

		//Check SKU against valid SKUs.
		public bool IsValidPurchase(string sku)
		{
			foreach(string validSku in validSkus)
			{
				if (sku == validSku)
				{
					return true;
				}
			}
			return false;
		}

		//Return the app's public key.
		public string AndroidPublicKey
		{
			//In a real app, return public key instead of null.
			get { return null; }
		}
	}



    public class ATK_AdmobManager : ATK_SingletonMonoBehaviourFast<ATK_AdmobManager>
    {
        /// <summary>
        /// バナーの形
        /// </summary>
        public enum BannerViewType
        {
            Banner,             // 320×50 	標準のバナー  携帯電話とタブレット  BANNER
            MediumRectangle,    // 300×250 IAB レクタングル（中） 	携帯電話とタブレット  MEDIUM_RECTANGLE
            Leaderboard,        // 728×90 	IAB ビッグバナー  タブレット   LEADERBOARD     ※2015/09 Androidでは使えなかった　iOS未検証
            IABBanner,          // 468×60 	IAB フルサイズ バナー   タブレット   FULL_BANNER
            SmartBanner,        // 画面の幅×32、50 または 90 	スマートバナー     携帯電話とタブレット  SMART_BANNER
        }

        /// <summary>
        /// バナーのレイアウト
        /// </summary>
        public enum BannerViewLayout
        {
            Top,
            Bottom,
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight,
        }

        #region 公開メンバ変数

        /// <summary>
        /// バナー広告用のユニットID
        /// </summary>
        public string BannerAdUnitId_Android = "ca-app-pub-????????????????/??????????"; //変更はプレファブを修正する
        public string BannerAdUnitId_iOS     = "ca-app-pub-????????????????/??????????"; //変更はプレファブを修正する

        /// <summary>
        /// バナーオプション
        /// </summary>
        public BannerViewType BannerType = BannerViewType.SmartBanner;
        public BannerViewLayout BannerLayout = BannerViewLayout.Top;
        public BannerViewType SubBannerType = BannerViewType.MediumRectangle;
        public BannerViewLayout SubBannerLayout = BannerViewLayout.Bottom;



        /// <summary>
        /// インタースティシャル広告用のユニットID
        /// </summary>
        public string InterstitialAdUnitId_Android = "ca-app-pub-????????????????/??????????"; //変更はプレファブを修正する
        public string InterstitialAdUnitId_iOS     = "ca-app-pub-????????????????/??????????"; //変更はプレファブを修正する



		#if false //2016/04/10 Android実機検証で表示されなかった　サンプルコードの方も同様
		/// <summary>
		/// ビデオ広告用のユニットID
		/// </summary>
		public string VideoAdUnitId_Android = "ca-app-pub-????????????????/??????????"; //変更はプレファブを修正する
		public string VideoAdUnitId_iOS     = "ca-app-pub-????????????????/??????????"; //変更はプレファブを修正する
#endif


        /// <summary>
        /// テストモード
        /// </summary>
        public bool EnableTestMode = true;

        /// <summary>
        /// テスト用のデバイスID
        /// EnableTestMode = falseの時はデバイスIDは無視される
        /// </summary>
        public string TestDeviceID_Android = "????????????????????????????????"; //変更はプレファブを修正する
        public string TestDeviceID_iOS     = "????????????????????????????????"; //変更はプレファブを修正する

        public enum LoadedResult
        {
            None,
            Failed,
            Succeed,
        }

        #endregion


        #region 非公開メンバ変数

        private static string _outputMessage = "";

        private BannerView _bannerView = null;
        private LoadedResult _bannerResult = LoadedResult.None;

        private BannerView _subBannerView = null;
        private LoadedResult _subBannerResult = LoadedResult.None;

        private InterstitialAd _interstitialView = null;
        private LoadedResult _interstitialResult = LoadedResult.None;

		#if false //2016/04/10 Android実機検証で表示されなかった　サンプルコードの方も同様
		private RewardBasedVideoAd _videoView = null;
		private LoadedResult _videoResult = LoadedResult.None;
		#endif

        #endregion


        #region プロパティ

        public static string OutputMessage { set { _outputMessage = value; } }

        #endregion



        #region 非公開メンバ関数

        private AdSize BannerTypeToAdSize(BannerViewType type)
        {
            AdSize ret = AdSize.SmartBanner;
            switch (type)
            {
                case BannerViewType.Banner: ret = AdSize.Banner; break;
                case BannerViewType.MediumRectangle: ret = AdSize.MediumRectangle; break;
                case BannerViewType.Leaderboard: ret = AdSize.Leaderboard; break;
                case BannerViewType.IABBanner: ret = AdSize.IABBanner; break;
                case BannerViewType.SmartBanner: ret = AdSize.SmartBanner; break;
            }
            Debug.Log("BannerTypeToAdSize : " + type.ToString() + " > " + ret.ToString());
            return ret;
        }

        private AdPosition BannerLayoutToAdPosition(BannerViewLayout layout)
        {
            AdPosition ret = AdPosition.Top;
            switch (layout)
            {
                case BannerViewLayout.Top: ret = AdPosition.Top; break;
                case BannerViewLayout.Bottom: ret = AdPosition.Bottom; break;
                case BannerViewLayout.TopLeft: ret = AdPosition.TopLeft; break;
                case BannerViewLayout.TopRight: ret = AdPosition.TopRight; break;
                case BannerViewLayout.BottomLeft: ret = AdPosition.BottomLeft; break;
                case BannerViewLayout.BottomRight: ret = AdPosition.BottomRight; break;
            }
            Debug.Log("BannerLayoutToAdPosition : " + layout.ToString() + " > " + ret.ToString());
            return ret;
        }


        private AdRequest createAdRequest()
        {
            string TestDeviceID = "unused";
            if (EnableTestMode)
            {
#if UNITY_ANDROID
                TestDeviceID = TestDeviceID_Android;
#elif UNITY_IPHONE
                TestDeviceID = TestDeviceID_iOS;
#endif
            }

            return new AdRequest.Builder()
                    .AddTestDevice(AdRequest.TestDeviceSimulator)
                    .AddTestDevice(TestDeviceID)
                    .AddKeyword("game")
                    .SetGender(Gender.Male)
                    .SetBirthday(new DateTime(1985, 1, 1))
                    .TagForChildDirectedTreatment(false)
                    .AddExtra("color_bg", "9B30FF")
                    .Build();

        }

        #endregion


        #region バナー

        /// <summary>
        /// バナーのリクエスト　※Showせずに表示される
        /// 既にリクエスト済みの場合は、削除してから再リクエストを行う
        /// </summary>
        public void RequestBanner(
            EventHandler<EventArgs> onAdLoaded,
            EventHandler<AdFailedToLoadEventArgs> onAdFailedToLoad,
            EventHandler<EventArgs> onAdOpened,
            EventHandler<EventArgs> onAdClosing,
            EventHandler<EventArgs> onAdClosed,
            EventHandler<EventArgs> onAdLeftApplication
            )
        {
            Debug.Log("Request Banner");
            Debug.Log("Banner Type = " + BannerType.ToString());
            Debug.Log("Banner Layout = " + BannerLayout.ToString());

            DestroyBanner();

            string adUnitId = "unused";
#if UNITY_ANDROID
            adUnitId = BannerAdUnitId_Android;
#elif UNITY_IPHONE
            adUnitId = BannerAdUnitId_iOS;
#endif
            // デフォルトコールバック
            _bannerView = new BannerView(adUnitId, BannerTypeToAdSize(BannerType), BannerLayoutToAdPosition(BannerLayout));
            _bannerView.OnAdLoaded += handleAdLoaded;
            _bannerView.OnAdFailedToLoad += handleAdFailedToLoad;
			_bannerView.OnAdLoaded += handleAdOpened;
            _bannerView.OnAdClosed += handleAdClosed;
			_bannerView.OnAdLeavingApplication += handleAdLeftApplication;

            // 追加コールバック
            if (!object.ReferenceEquals(onAdLoaded, null)) _bannerView.OnAdLoaded += onAdLoaded;
            if (!object.ReferenceEquals(onAdLoaded, null)) _bannerView.OnAdFailedToLoad += onAdFailedToLoad;
			if (!object.ReferenceEquals(onAdOpened, null)) _bannerView.OnAdLoaded += onAdOpened;
            if (!object.ReferenceEquals(onAdClosed, null)) _bannerView.OnAdClosed += onAdClosed;
			if (!object.ReferenceEquals(onAdLeftApplication, null)) _bannerView.OnAdLeavingApplication += onAdLeftApplication;

            _bannerView.LoadAd(createAdRequest());
        }

        public void RequestBanner()
        {
            RequestBanner(null, null, null, null, null, null);
        }

        public void ShowBanner()
        {
            Debug.Log("Show Banner");
            if (IsReadyBanner())
            {
                _bannerView.Show();
            }
            else
            {
                Debug.Log("banner is not ready yet.");
            }
        }

        public void HideBanner()
        {
            Debug.Log("Hide Banner");
            if (IsReadyBanner())
            {
                _bannerView.Hide();
            }
            else
            {
                Debug.Log("banner is not ready yet.");
            }
        }

        /// <summary>
        /// バナーの破棄　※リクエスト中も含む
        /// </summary>
        public void DestroyBanner()
        {
            Debug.Log("Destroy Banner");
            if (!object.ReferenceEquals(_bannerView, null)) 
            {
                _bannerView.Destroy();
                _bannerView = null;
                _bannerResult = LoadedResult.None;
            }
            else
            {
                Debug.Log("banner is not ready yet.");
            }
        }
        public bool IsReadyBanner()
        {
            return !object.ReferenceEquals(_bannerView, null) && object.Equals(LoadedResult.Succeed, _bannerResult);

        }

        /// <summary>
        /// 広告が表示された場合に呼び出されます
        /// </summary>
        private void handleAdLoaded(object sender, EventArgs args)
        {
            _bannerResult = LoadedResult.Succeed;
            print("Admob : handleAdLoaded");
        }

        /// <summary>
        /// 広告リクエストが失敗した場合に呼び出されます
        /// </summary>
        private void handleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            _bannerResult = LoadedResult.Failed;
            print("Admob : handleAdFailedToLoad : msg = " + args.Message);
        }

        /// <summary>
        /// 広告からオーバーレイを開いて画面全体が覆われた場合に呼び出されます
        /// </summary>
        private void handleAdOpened(object sender, EventArgs args)
        {
            print("Admob : HandleAdOpened");
        }

        private void handleAdClosed(object sender, EventArgs args)
        {
            print("Admob : HandleAdClosed");
        }

        /// <summary>
        /// 広告からアプリを終了した場合（ブラウザに移動する場合など）に呼び出されます
        /// </summary>
        private void handleAdLeftApplication(object sender, EventArgs args)
        {
            print("Admob : HandleAdLeftApplication");
        }

        #endregion



        #region サブバナー
        
        /// <summary>
        /// バナーのリクエスト　※Showせずに表示される
        /// 既にリクエスト済みの場合は、削除してから再リクエストを行う
        /// </summary>
        public void RequestSubBanner(
            EventHandler<EventArgs> onAdLoaded,
            EventHandler<AdFailedToLoadEventArgs> onAdFailedToLoad,
            EventHandler<EventArgs> onAdOpened,
            EventHandler<EventArgs> onAdClosing,
            EventHandler<EventArgs> onAdClosed,
            EventHandler<EventArgs> onAdLeftApplication
            )
        {
            Debug.Log("Request Sub Banner");
            Debug.Log("Banner Type = " + SubBannerType.ToString());
            Debug.Log("Banner Layout = " + SubBannerLayout.ToString());

            DestroySubBanner();

            string adUnitId = "unused";
#if UNITY_ANDROID
            adUnitId = BannerAdUnitId_Android;
#elif UNITY_IPHONE
            adUnitId = BannerAdUnitId_iOS;
#endif
            // デフォルトコールバック
            _subBannerView = new BannerView(adUnitId, BannerTypeToAdSize(SubBannerType), BannerLayoutToAdPosition(SubBannerLayout));
            _subBannerView.OnAdLoaded += handleAdLoadedSub;
            _subBannerView.OnAdFailedToLoad += handleAdFailedToLoadSub;
			_subBannerView.OnAdLoaded += handleAdOpenedSub;
            _subBannerView.OnAdClosed += handleAdClosedSub;
			_subBannerView.OnAdLeavingApplication += handleAdLeftApplicationSub;

            // 追加コールバック
            if (!object.ReferenceEquals(onAdLoaded, null)) _subBannerView.OnAdLoaded += onAdLoaded;
            if (!object.ReferenceEquals(onAdLoaded, null)) _subBannerView.OnAdFailedToLoad += onAdFailedToLoad;
			if (!object.ReferenceEquals(onAdOpened, null)) _subBannerView.OnAdLoaded += onAdOpened;
            if (!object.ReferenceEquals(onAdClosed, null)) _subBannerView.OnAdClosed += onAdClosed;
			if (!object.ReferenceEquals(onAdLeftApplication, null)) _subBannerView.OnAdLeavingApplication += onAdLeftApplication;

            _subBannerView.LoadAd(createAdRequest());
        }

        public void RequestSubBanner()
        {
            RequestSubBanner(null, null, null, null, null, null);
        }

        public void ShowSubBanner()
        {
            Debug.Log("Show Sub Banner");
            if (IsReadySubBanner())
            {
                _subBannerView.Show();
            }
            else
            {
                Debug.Log("banner is not ready yet.");
            }
        }

        public void HideSubBanner()
        {
            Debug.Log("Hide Sub Banner");
            if (IsReadySubBanner())
            {
                _subBannerView.Hide();
            }
            else
            {
                Debug.Log("banner is not ready yet.");
            }
        }

        /// <summary>
        /// バナーの破棄　※リクエスト中も含む
        /// </summary>
        public void DestroySubBanner()
        {
            Debug.Log("Destroy Sub Banner");
            if (!object.ReferenceEquals(_subBannerView, null))
            {
                _subBannerView.Destroy();
                _subBannerView = null;
                _subBannerResult = LoadedResult.None;
            }
            else
            {
                Debug.Log("banner is not ready yet.");
            }
        }
        public bool IsReadySubBanner()
        {
            return !object.ReferenceEquals(_subBannerView, null) && object.Equals(LoadedResult.Succeed, _subBannerResult);

        }

        /// <summary>
        /// 広告が表示された場合に呼び出されます
        /// </summary>
        private void handleAdLoadedSub(object sender, EventArgs args)
        {
            _subBannerResult = LoadedResult.Succeed;
            print("Admob : HandleAdLoadedSub");
        }

        /// <summary>
        /// 広告リクエストが失敗した場合に呼び出されます
        /// </summary>
        private void handleAdFailedToLoadSub(object sender, AdFailedToLoadEventArgs args)
        {
            _subBannerResult = LoadedResult.Failed;
            print("Admob : handleAdFailedToLoadSub : msg = " + args.Message);
        }

        /// <summary>
        /// 広告からオーバーレイを開いて画面全体が覆われた場合に呼び出されます
        /// </summary>
        private void handleAdOpenedSub(object sender, EventArgs args)
        {
            print("Admob : handleAdOpenedSub");
        }

        private void handleAdClosingSub(object sender, EventArgs args)
        {
            print("Admob : handleAdClosingSub");
        }

        private void handleAdClosedSub(object sender, EventArgs args)
        {
            print("Admob : handleAdClosedSub");
        }

        /// <summary>
        /// 広告からアプリを終了した場合（ブラウザに移動する場合など）に呼び出されます
        /// </summary>
        private void handleAdLeftApplicationSub(object sender, EventArgs args)
        {
            print("Admob : handleAdLeftApplicationSub");
        }

        #endregion バナー




        #region インタースティシャル

        /// <summary>
        /// インタースティシャルのリクエスト　※Showするまで表示されない
        /// </summary>
        public void RequestInterstitial(
            EventHandler<EventArgs> onInterstitialLoaded,
            EventHandler<AdFailedToLoadEventArgs> onInterstitialFailedToLoad,
            EventHandler<EventArgs> onInterstitialOpened,
            EventHandler<EventArgs> onInterstitialClosing,
            EventHandler<EventArgs> onInterstitialClosed,
            EventHandler<EventArgs> onInterstitialLeftApplication
            )
        {
            Debug.Log("Request Interstitial");
            if (IsReadyInterstitial())
            {
                Debug.Log("interstitial is already set");
                DestroyInterstitial();
            }

            string adUnitId = "unused";
#if UNITY_ANDROID
            adUnitId = InterstitialAdUnitId_Android;
#elif UNITY_IPHONE
            adUnitId = InterstitialAdUnitId_iOS;
#endif
            // デフォルトコールバック
            _interstitialView = new InterstitialAd(adUnitId);
            _interstitialView.OnAdLoaded += handleInterstitialLoaded;
            _interstitialView.OnAdFailedToLoad +=handleInterstitialFailedToLoad;
			_interstitialView.OnAdOpening += handleInterstitialOpened;
            _interstitialView.OnAdClosed += handleInterstitialClosed;
			_interstitialView.OnAdLeavingApplication += handleInterstitialLeftApplication;

            // 追加コールバック
            if (!object.ReferenceEquals(onInterstitialLoaded, null)) _interstitialView.OnAdLoaded += onInterstitialLoaded;
            if (!object.ReferenceEquals(onInterstitialFailedToLoad, null)) _interstitialView.OnAdFailedToLoad += onInterstitialFailedToLoad;
			if (!object.ReferenceEquals(onInterstitialOpened, null)) _interstitialView.OnAdOpening += onInterstitialOpened;
            if (!object.ReferenceEquals(onInterstitialClosed, null)) _interstitialView.OnAdClosed += onInterstitialClosed;
			if (!object.ReferenceEquals(onInterstitialLeftApplication, null)) _interstitialView.OnAdLeavingApplication += onInterstitialLeftApplication;

//いらなくなった？            GoogleMobileAdsDemoHandler handler = new GoogleMobileAdsDemoHandler();
//いらなくなった？            _interstitialView.SetInAppPurchaseHandler(handler);
            _interstitialView.LoadAd(createAdRequest());
        }
        public void RequestInterstitial()
        {
            RequestInterstitial(null, null, null, null, null, null);
        }



        public void ShowInterstitial()
        {
            Debug.Log("Show Interstitial");
            if (IsReadyInterstitial())
            {
                _interstitialView.Show();
            }
            else
            {
                Debug.Log("interstitial is not ready yet.");
            }
        }
        public void DestroyInterstitial()
        {
            Debug.Log("Destroy Interstitial");
            if (IsReadyInterstitial())
            {
                _interstitialView.Destroy();
                _interstitialView = null;
                _interstitialResult = LoadedResult.None;
            }
            else
            {
                Debug.Log("Interstitial is not ready yet.");
            }
        }
        public bool IsReadyInterstitial()
        {
            return !object.ReferenceEquals(_interstitialView, null) && _interstitialView.IsLoaded();
        }


        /// デフォルトコールバック
        private void handleInterstitialLoaded(object sender, EventArgs args)
        {
            _interstitialResult = LoadedResult.Succeed;
            print("Admob : HandleInterstitialLoaded event received.");
        }

        private void handleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
        {
            _interstitialResult = LoadedResult.Failed;
            print("Admob : HandleInterstitialFailedToLoad event received with message: " + args.Message);
        }

        private void handleInterstitialOpened(object sender, EventArgs args)
        {
            print("Admob : HandleInterstitialOpened event received");
        }

        private void handleInterstitialClosing(object sender, EventArgs args)
        {
            print("Admob : HandleInterstitialClosing event received");
        }

        private void handleInterstitialClosed(object sender, EventArgs args)
        {
            print("Admob : HandleInterstitialClosed event received");
        }

        private void handleInterstitialLeftApplication(object sender, EventArgs args)
        {
            print("Admob : HandleInterstitialLeftApplication event received");
        }

        #endregion インタースティシャル


		#if false //2016/04/10 Android実機検証で表示されなかった　サンプルコードの方も同様
		#region ビデオ

		public void RequestVideo()
		{
			string adUnitId = "unused";
			#if UNITY_ANDROID
			adUnitId = VideoAdUnitId_Android;
			#elif UNITY_IPHONE
			adUnitId = VideoAdUnitId_iOS;
			#endif


			_videoView = RewardBasedVideoAd.Instance;
			// デフォルトコールバック
			_videoView.OnAdLoaded += HandleRewardBasedVideoLoaded;
			_videoView.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
			_videoView.OnAdOpening += HandleRewardBasedVideoOpened;
			_videoView.OnAdStarted += HandleRewardBasedVideoStarted;
			_videoView.OnAdRewarded += HandleRewardBasedVideoRewarded;
			_videoView.OnAdClosed += HandleRewardBasedVideoClosed;
			_videoView.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

			/*
			// 追加コールバック
			if (!object.ReferenceEquals(onInterstitialLoaded, null)) _interstitialView.OnAdLoaded += onInterstitialLoaded;
			if (!object.ReferenceEquals(onInterstitialFailedToLoad, null)) _interstitialView.OnAdFailedToLoad += onInterstitialFailedToLoad;
			if (!object.ReferenceEquals(onInterstitialOpened, null)) _interstitialView.OnAdOpening += onInterstitialOpened;
			if (!object.ReferenceEquals(onInterstitialClosed, null)) _interstitialView.OnAdClosed += onInterstitialClosed;
			if (!object.ReferenceEquals(onInterstitialLeftApplication, null)) _interstitialView.OnAdLeavingApplication += onInterstitialLeftApplication;
			*/

			_videoView.LoadAd(createAdRequest(), adUnitId);
		}
		public void ShowVideo()
		{
			if (IsReadyVideo())
				_videoView.Show ();
		}

		public bool IsReadyVideo()
		{
			return !object.ReferenceEquals(_videoView, null) && object.Equals(LoadedResult.Succeed, _videoResult);

		}

		public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
		{
			_videoResult = LoadedResult.Succeed;
			print("HandleRewardBasedVideoLoaded event received.");
		}

		public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
		{
			_videoResult = LoadedResult.Failed;
			print("HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
		}

		public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
		{
			print("HandleRewardBasedVideoOpened event received");
		}

		public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
		{
			print("HandleRewardBasedVideoStarted event received");
		}

		public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
		{
			print("HandleRewardBasedVideoClosed event received");
		}

		public void HandleRewardBasedVideoRewarded(object sender, Reward args)
		{
			string type = args.Type;
			double amount = args.Amount;
			print("HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " +
				type);
		}

		public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
		{
			print("HandleRewardBasedVideoLeftApplication event received");
		}

		#endregion ビデオ
		#endif
    }
}

#pragma warning restore 414
