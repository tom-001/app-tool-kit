﻿/// <summary>
/// iAD (バナー広告/インタースティシャル広告)
/// </summary>
/// iOS only
/// インタースティシャル（フルスクリーン）広告はiPadのみ
/// このクラスをインスタンス化して常駐させておけば、いつでも使える
/// 広告の使用できるタイミングは、IsReady()で確認
/*
==================================================
　導入
==================================================
・UnityのApple iAd フレームワークを使う


==================================================
　ポイント
==================================================

・バナーのポイント
　RequestBanner()：バナーをリクエストする。読込完了で自動表示される。※非表示にできるが、Admobと仕様を合わせた
　ShowBanner()：（リクエスト完了後）バナーを表示する
　HideBanner()：（リクエスト完了後）バナーを非表示する
　DestroyBanner()：（リクエスト完了後）バナーを破棄する。再表示にはリクエストが必要
　注意：
　　リクエスト完了後、破棄しなければ、表示と非表示を繰り返すことができる

・インタースティシャルのポイント
　RequestInterstitial()：インタースティシャルをリクエストする。読込完了で自動表示されない。※表示できるが、Admobと仕様を合わせた
　ShowInterstitial()：（リクエスト完了後）インタースティシャルを表示する
　DestroyInterstitial()：（リクエスト完了後）インタースティシャルを破棄する。再表示にはリクエストが必要
　注意：
　　インタースティシャルは全画面表示され、閉じると破棄される
　　再表示にはリクエストが必要だが、クールタイムを設け、乱発しないようにするべき
　　フル画面オーバーレイなので、表示位置やカスタマイズはできない。



==================================================
　参考
==================================================
・ADBannerView
http://docs.unity3d.com/jp/current/ScriptReference/iOS.ADBannerView.html

・ADInterstitialAd
http://docs.unity3d.com/jp/current/ScriptReference/iOS.ADInterstitialAd.html


*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_IOS || UNITY_EDITOR
using ADBannerView = UnityEngine.iOS.ADBannerView;
using ADInterstitialAd = UnityEngine.iOS.ADInterstitialAd;
#endif

#pragma warning disable 414


namespace ATK.Ads
{


    public class ATK_iAdManager : ATK_SingletonMonoBehaviourFast<ATK_iAdManager>
    {
        /// <summary>
        /// バナーの形
        /// </summary>
        public enum BannerViewType
        {
            SmartBanner,     // 従来のバナー (フルスクリーンの幅を使用) スマートバナー
            MediumRect,      // 四角形のバナー (300×250)
        }

        /// <summary>
        /// バナーのレイアウト(四角いバナーはiOS6以降)
        /// </summary>
        public enum BannerViewLayout
        {
            Top,           // 従来のバナー: スクリーン最上部に合わせる
            Bottom,        // 従来のバナー: スクリーン最下部に合わせて表示
            TopLeft,       // 四角形バナー: スクリーン上部に合わせて、左隅に配置
            TopRight,      // 四角形のバナー: スクリーン上部に合わせて、右隅に配置
            TopCenter,     // 四角形のバナー: スクリーン上部に合わせて、中央に配置
            BottomLeft,    // 四角形のバナー: スクリーン最下部に合わせて、左隅に配置
            BottomRight,   // 四角形のバナー: スクリーン最下部に合わせて、右隅に配置
            BottomCenter,  // 四角形のバナー: スクリーン最下部に合わせて、中央に配置
            CenterLeft,    // 四角形バナー: スクリーン左側に合わせて、中央に配置
            CenterRight,   // 四角形のバナー: スクリーン右側に合わせて、中央に配置
            Center,        // 四角形のバナー: 画面中央に配置
            Manual,        // 完全に手動で配置
        }

        /// <summary>
        /// バナーオプション
        /// </summary>
        public BannerViewType BannerType = BannerViewType.SmartBanner;
        public BannerViewLayout BannerLayout = BannerViewLayout.TopCenter;


        /// <summary>
        /// テストモード
        /// </summary>
        public bool EnableTestMode = true;//TODO いまのところ無関係　公式サイトでテストモードにするのかな？

#if UNITY_IOS || UNITY_EDITOR

        #region 公開メンバ変数

        public enum LoadedResult
        {
            None,
            Failed,
            Succeed,
        }

        #endregion



        #region 非公開メンバ変数

        private ADBannerView _bannerView = null;
        private ADInterstitialAd _interstitialView = null;
        private LoadedResult _bannerResult = LoadedResult.None;
        private LoadedResult _interstitialResult = LoadedResult.None;

        #endregion



        #region 非公開メンバ関数

        private ADBannerView.Type toTrueBannerType(BannerViewType type)
        {
            ADBannerView.Type ret = ADBannerView.Type.Banner;
            switch (type)
            {
                case BannerViewType.SmartBanner: ret = ADBannerView.Type.Banner; break;
                case BannerViewType.MediumRect: ret = ADBannerView.Type.MediumRect; break;
            }
            return ret;
        }

        private ADBannerView.Layout toTrueBannerLayout(BannerViewLayout layout)
        {
            ADBannerView.Layout ret = ADBannerView.Layout.Top;
            switch (layout)
            {
                case BannerViewLayout.Top: ret = ADBannerView.Layout.Top; break;
                case BannerViewLayout.Bottom: ret = ADBannerView.Layout.Bottom; break;
                case BannerViewLayout.TopLeft: ret = ADBannerView.Layout.TopLeft; break;
                case BannerViewLayout.TopRight: ret = ADBannerView.Layout.TopRight; break;
                case BannerViewLayout.TopCenter: ret = ADBannerView.Layout.TopCenter; break;
                case BannerViewLayout.BottomLeft: ret = ADBannerView.Layout.BottomLeft; break;
                case BannerViewLayout.BottomRight: ret = ADBannerView.Layout.BottomRight; break;
                case BannerViewLayout.BottomCenter: ret = ADBannerView.Layout.BottomCenter; break;
                case BannerViewLayout.CenterLeft: ret = ADBannerView.Layout.CenterLeft; break;
                case BannerViewLayout.CenterRight: ret = ADBannerView.Layout.CenterRight; break;
                case BannerViewLayout.Center: ret = ADBannerView.Layout.Center; break;
                case BannerViewLayout.Manual: ret = ADBannerView.Layout.Manual; break;
            }
            return ret;
        }

        #endregion

        #region Banner

        /// <summary>
        /// バナーのリクエスト　※読込完了したら表示される
        /// </summary>
        public void RequestBanner(
            ADBannerView.BannerWasClickedDelegate onBannerClicked,
            ADBannerView.BannerWasLoadedDelegate onBannerLoaded,
            ADBannerView.BannerFailedToLoadDelegate onBannerFailedToLoad
            )
        {
            Debug.Log("Request Banner");
            Debug.Log("Banner Type = " + BannerType.ToString());
            Debug.Log("Banner Layout = " + BannerLayout.ToString());
            if (!ADBannerView.IsAvailable(toTrueBannerType(BannerType)))
            {
                Debug.Log("cannot use banner");
                return;
            }
            DestroyBanner();

            _bannerView = new ADBannerView(toTrueBannerType(BannerType), toTrueBannerLayout(BannerLayout));

            // デフォルトコールバック
            ADBannerView.onBannerWasClicked += this.onBannerClicked;
            ADBannerView.onBannerWasLoaded += this.onBannerLoaded;
            ADBannerView.onBannerFailedToLoad += this.onBannerFailedToLoad;

            // 追加コールバック
            if (!object.ReferenceEquals(onBannerClicked, null)) ADBannerView.onBannerWasClicked += onBannerClicked;
            if (!object.ReferenceEquals(onBannerLoaded, null)) ADBannerView.onBannerWasLoaded += onBannerLoaded;
            if (!object.ReferenceEquals(onBannerFailedToLoad, null)) ADBannerView.onBannerFailedToLoad += onBannerFailedToLoad;

        }

        public void RequestBanner()
        {
            RequestBanner(null, null, null);
        }


        public void ShowBanner()
        {
            Debug.Log("Show Banner");
            if (IsReadyBanner())
            {
                _bannerView.visible = true;
            }
            else
            {
                Debug.Log("banner is not ready yet.");
            }
        }

        public void HideBanner()
        {
            Debug.Log("Hide Banner");
            if (IsReadyBanner())
            {
                _bannerView.visible = false;
            }
            else
            {
                Debug.Log("banner is not ready yet.");
            }
        }

        /// <summary>
        /// バナーの破棄　※リクエスト中も含む
        /// </summary>
        public void DestroyBanner()
        {
            Debug.Log("Destroy Banner");
            if (!object.ReferenceEquals(_bannerView, null))
            {
                _bannerView.visible = false;
                _bannerView = null;
                _bannerResult = LoadedResult.None;
            }
            else
            {
                Debug.Log("banner is not ready yet.");
            }
        }
        public bool IsReadyBanner()
        {
            return !object.ReferenceEquals(_bannerView, null) && object.Equals(LoadedResult.Succeed, _bannerResult);
        }

        #endregion



        #region Interstitial

        /// <summary>
        /// バナーのリクエスト　※読込完了したら表示される
        /// </summary>
        public void RequestInterstitial(
            ADInterstitialAd.InterstitialWasLoadedDelegate onInterstitialLoaded
            )
        {
            Debug.Log("Request Interstitial");
            if (!ADInterstitialAd.isAvailable)
            {
                Debug.Log("cannot use interstitial");
                return;
            }
            if (IsReadyInterstitial())
            {
                Debug.Log("interstitial is already set");
                DestroyInterstitial();
            }

            _interstitialView = new ADInterstitialAd();

            // デフォルトコールバック
            ADInterstitialAd.onInterstitialWasLoaded += this.onInterstitialLoaded;

            // 追加コールバック
            if (!object.ReferenceEquals(onInterstitialLoaded, null)) ADInterstitialAd.onInterstitialWasLoaded += onInterstitialLoaded;

        }

        public void RequestInterstitial()
        {
            RequestInterstitial(null);
        }


        public void ShowInterstitial()
        {
            Debug.Log("Show Interstitial");
            if (IsReadyInterstitial())
            {
                _interstitialView.Show();
            }
            else
            {
                Debug.Log("Interstitial is not ready yet.");
            }
        }

        public void DestroyInterstitial()
        {
            Debug.Log("Destroy Interstitial");
            if (IsReadyInterstitial())
            {
                _interstitialView = null;
                _interstitialResult = LoadedResult.None;
            }
            else
            {
                Debug.Log("Interstitial is not ready yet.");
            }
        }

        public bool IsReadyInterstitial()
        {
            return !object.ReferenceEquals(_interstitialView, null) && _interstitialView.loaded;
        }

        public void ReloadAd()
        {
            Debug.Log("Reload Interstitial");
            if (IsReadyInterstitial())
            {
                _interstitialView.ReloadAd();
            }
            else
            {
                Debug.Log("Interstitial is not ready yet.");
            }
        }

        #endregion



        #region Banner callback 

        /// <summary>
        /// バナーをクリックした時に呼び出されます。
        /// </summary>
        private void onBannerClicked()
        {
            Debug.Log("banner clicked");
        }

        /// <summary>
        /// 新しいバナー広告をロードした時に起動します。
        /// </summary>
        private void onBannerLoaded()
        {
            Debug.Log("banner loaded");
			_bannerResult = LoadedResult.Succeed;
			ShowBanner();
        }

        private void onBannerFailedToLoad()
        {
            Debug.Log("banner failed");
            _bannerResult = LoadedResult.Failed;
        }

        #endregion



        #region Interstitial callback 

        /// <summary>
        /// 広告を表示する準備ができた時に呼び出されます
        /// </summary>
        private void onInterstitialLoaded()
        {
            Debug.Log("interstitial loaded");
            //ここで表示可能　ShowInterstitial();
            _interstitialResult = LoadedResult.Succeed;
        }

        #endregion
#else

        public delegate void BannerWasClickedDelegate();
        public delegate void BannerWasLoadedDelegate();
        public delegate void BannerFailedToLoadDelegate();

        public void RequestBanner(
            BannerWasClickedDelegate onBannerClicked,
            BannerWasLoadedDelegate onBannerLoaded,
            BannerFailedToLoadDelegate onBannerFailedToLoad
            ) {
            Debug.Log("RequestBanner is iOS only");
            Debug.Log("Banner Type = " + BannerType.ToString());
            Debug.Log("Banner Layout = " + BannerLayout.ToString());
        }
        public void RequestBanner() {
            RequestBanner(null, null, null);
        }
        public void ShowBanner() {
            Debug.Log("ShowBanner is iOS only");
        }
        public void HideBanner() {
            Debug.Log("HideBanner is iOS only");
        }
        public void DestroyBanner() {
            Debug.Log("DestroyBanner is iOS only");
        }
        public bool IsReadyBanner() {
            Debug.Log("IsReadyBanner is iOS only");
            return false;
        }

        public delegate void InterstitialWasLoadedDelegate();

        public void RequestInterstitial(InterstitialWasLoadedDelegate onInterstitialLoaded) {
            Debug.Log("RequestInterstitial is iOS only");
        }
        public void ShowInterstitial() {
            Debug.Log("RequestInterstitial is iOS only");
        }
        public void DestroyInterstitial() {
            Debug.Log("DestroyInterstitial is iOS only");
        }
        public bool IsReadyInterstitial() {
            Debug.Log("IsReadyInterstitial is iOS only");
            return false;
        }
#endif
    }
}

#pragma warning restore 414
