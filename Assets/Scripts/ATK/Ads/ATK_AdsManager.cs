﻿/// <summary>
/// UnityAds/iAd(iOS only)/Admobを使った、広告の表示
/// </summary>
/*


Android は Admob 固定
iOS は iAd 固定

バナー
表示タイプやレイアウトを変えるには、一度DestroyBanner()をしてから


※Admobのコールバック内でログを表示する場合はprintを使用する

*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#pragma warning disable 414


namespace ATK.Ads
{

    public class ATK_AdsManager : ATK_SingletonMonoBehaviourFast<ATK_AdsManager>
    {
        /// <summary>
        /// 各広告管理プレファブ
        /// </summary>
        [SerializeField] private ATK_UnityAdsManager _unityAdsManagerPrefab = null;
        [SerializeField] private ATK_iAdManager _iadManagerPrefab = null;
        [SerializeField] private ATK_AdmobManager _admobManagerPrefab = null;

        #region 公開メンバ変数

        /// <summary>
        /// バナーの形
        /// </summary>
        public enum BannerViewType
        {
            /// Android : 画面の幅×32、50 または 90  ATK_AdmobManager.BannerViewType.SmartBannerを使用
            /// iOS     : 従来のスマートバナー        ATK_iAdManager.BannerViewType.SmartBannerを使用
            SmartBanner,

            /// Android : 300×250 ATK_AdmobManager.BannerViewType.MediumRectangleを使用
            /// iOS     : 300×250 ATK_iAdManager.BannerViewType.MediumRectを使用
            MediumRect,
        }

        /// <summary>
        /// バナーのレイアウト
        /// </summary>
        public enum BannerViewLayout
        {
            /// Android : ATK_AdmobManager.BannerViewLayout.～を使用
            /// iOS     : ATK_iAdManager.BannerViewLayout.～を使用
            Top,
            Bottom,
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight,
        }

        /// <summary>
        /// バナーオプション
        /// </summary>
        public BannerViewType BannerType = BannerViewType.SmartBanner;
        public BannerViewLayout BannerLayout = BannerViewLayout.Top;

        #endregion



        #region 非公開メンバ変数

        /// <summary>
        /// コールバック
        /// </summary>
        private static Action _onBannerLoaded;
        private static Action _onBannerFailedToLoad;
        private static Action _onInterstitialLoaded;
        private static Action _onInterstitialFailedToLoad;

        private enum AdsServices
        {
            None,
            UnityAds,
            iAd,
            Admob,
        }

        #endregion


        void Awake()
        {
            Assert.IsNotNull(_unityAdsManagerPrefab, "ATK_UnityAdsManager Prefab がアタッチされていない");
            Assert.IsNotNull(_iadManagerPrefab, "ATK_iAdManager Prefab がアタッチされていない");
            Assert.IsNotNull(_admobManagerPrefab, "ATK_AdmobManager Prefab がアタッチされていない");

			var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 

			string prefabName = string.Empty;
			GameObject prefab = null;
			GameObject obj = null;

			// インスタンスの作成
			// ATK_UnityAdsManager
			prefabName = _unityAdsManagerPrefab.name;
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
			prefab = prefabs[prefabName];			
			obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			
			// ATK_iAdManager
			prefabName = _iadManagerPrefab.name;
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
			prefab = prefabs[prefabName];			
			obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			
			// ATK_AdmobManager
			prefabName = _admobManagerPrefab.name;
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
			prefab = prefabs[prefabName];			
			obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
		}


        #region バナー

        private AdsServices bestBannerAdsServices()
        {
            AdsServices type = AdsServices.None;

            if (UnityEngine.Application.platform == RuntimePlatform.Android)
                type = AdsServices.Admob;
            else if (UnityEngine.Application.platform == RuntimePlatform.IPhonePlayer)
                type = AdsServices.iAd;
            else if (UnityEngine.Application.platform == RuntimePlatform.WindowsEditor ||
                     UnityEngine.Application.platform == RuntimePlatform.OSXEditor)
                type = AdsServices.Admob;
            else
                Debug.LogWarning("this platform is not supported");

            return type;
        }

        private AdsServices bestVideoAdsServices()
        {
            AdsServices type = AdsServices.None;

            if (UnityEngine.Application.platform == RuntimePlatform.Android ||
                UnityEngine.Application.platform == RuntimePlatform.IPhonePlayer ||
                UnityEngine.Application.platform == RuntimePlatform.WindowsEditor || 
                UnityEngine.Application.platform == RuntimePlatform.OSXEditor)
                type = AdsServices.UnityAds;
            else
                Debug.LogWarning("this platform is not supported");

            return type;
        }

        private bool isReadyBanner()
        {
            bool ret = false;
            switch (bestBannerAdsServices())
            {
                case AdsServices.Admob: ret = ATK_AdmobManager.Instance.IsReadyBanner(); break;
                case AdsServices.iAd: ret = ATK_iAdManager.Instance.IsReadyBanner(); break;
            }
            return ret;
        }


        /// <summary>
        /// バナー表示　※リクエストが未完了の場合は、再リクエスト
        /// </summary>
        public void ShowBanner(
            BannerViewType type,
            BannerViewLayout layout,
            Action onBannerLoaded,
            Action onBannerFailedToLoad
            )
        {
            Debug.Log("ShowBanner");

            BannerType = type;
            BannerLayout = layout;

            switch (bestBannerAdsServices())
            {
                case AdsServices.Admob:
                    if (isReadyBanner()) {
                        ATK_AdmobManager.Instance.ShowBanner();
                    }
                    else {
                        ATK_AdmobManager.Instance.BannerType = toAdmobBannerViewType(BannerType);
                        ATK_AdmobManager.Instance.BannerLayout = toAdmobBannerViewLayout(BannerLayout);
                        ATK_AdmobManager.Instance.RequestBanner(admobOnBannerLoaded, admobOnBannerFailedToLoad, null, null, null, null);
                    }
                    break;

                case AdsServices.iAd:
                    if (isReadyBanner()) {
                        ATK_iAdManager.Instance.ShowBanner();
                    }
                    else {
                        ATK_iAdManager.Instance.BannerType = toiAdBannerViewType(BannerType);
                        ATK_iAdManager.Instance.BannerLayout = toiAdBannerViewLayout(BannerLayout);
                        ATK_iAdManager.Instance.RequestBanner(null, iAdOnBannerLoaded, iAdOnBannerFailedToLoad);
                    }
                    break;
            }

            if (AdsServices.None != bestBannerAdsServices())
            {
                if (!object.ReferenceEquals(onBannerLoaded, null)) _onBannerLoaded = onBannerLoaded;
                if (!object.ReferenceEquals(onBannerFailedToLoad, null)) _onBannerFailedToLoad = onBannerFailedToLoad;
            }
        }

        public void ShowBanner(BannerViewType type, BannerViewLayout layout)
        {
            ShowBanner(type, layout, null, null);
        }

        public void ShowBanner(Action onBannerLoaded, Action onBannerFailedToLoad)
        {
            ShowBanner(BannerType, BannerLayout, onBannerLoaded, onBannerFailedToLoad);
        }

        public void ShowBanner()
        {
            ShowBanner(BannerType, BannerLayout, null, null);
        }

        public void HideBanner()
        {
            Debug.Log("HideBanner");
            switch (bestBannerAdsServices())
            {
                case AdsServices.Admob:
                    if (isReadyBanner()){
                        ATK_AdmobManager.Instance.HideBanner();
                    }
                    break;

                case AdsServices.iAd:
                    if (isReadyBanner()){
                        ATK_iAdManager.Instance.HideBanner();
                    }
                    break;
            }
        }

        /// <summary>
        /// バナーの破棄　※リクエスト中も含む
        /// </summary>
        public void DestroyBanner()
        {
            Debug.Log("DestroyBanner");
            switch (bestBannerAdsServices())
            {
                case AdsServices.Admob:
                    ATK_AdmobManager.Instance.DestroyBanner();
                    break;

                case AdsServices.iAd:
                    ATK_iAdManager.Instance.DestroyBanner();
                    break;
            }
            _onBannerLoaded = null;
            _onBannerFailedToLoad = null;
        }



        /// <summary>
        /// Admobのバナータイプに変換
        /// </summary>
        private ATK_AdmobManager.BannerViewType toAdmobBannerViewType(BannerViewType type)
        {
            ATK_AdmobManager.BannerViewType ret = ATK_AdmobManager.BannerViewType.SmartBanner;
            switch (type)
            {
                /// Android : 画面の幅×32、50 または 90  ATK_AdmobManager.BannerViewType.SmartBannerを使用
                case BannerViewType.SmartBanner:    ret = ATK_AdmobManager.BannerViewType.SmartBanner;      break;

                /// Android : 300×250 ATK_AdmobManager.BannerViewType.MediumRectangleを使用
                case BannerViewType.MediumRect:     ret = ATK_AdmobManager.BannerViewType.MediumRectangle;  break;
            }
            return ret;
        }

        /// <summary>
        /// iAdのバナータイプに変換
        /// </summary>
        private ATK_iAdManager.BannerViewType toiAdBannerViewType(BannerViewType type)
        {
            ATK_iAdManager.BannerViewType ret = ATK_iAdManager.BannerViewType.SmartBanner;
            switch (type)
            {
                /// iOS     : 従来のスマートバナー        ATK_iAdManager.BannerViewType.SmartBannerを使用
                case BannerViewType.SmartBanner:    ret = ATK_iAdManager.BannerViewType.SmartBanner;    break;

                /// iOS     : 300×250 ATK_iAdManager.BannerViewType.MediumRectを使用
                case BannerViewType.MediumRect:     ret = ATK_iAdManager.BannerViewType.MediumRect;     break;
            }
            return ret;
        }

        /// <summary>
        /// Admobのバナーレイアウトに変換
        /// </summary>
        private ATK_AdmobManager.BannerViewLayout toAdmobBannerViewLayout(BannerViewLayout layout)
        {
            /// Android : ATK_AdmobManager.BannerViewLayout.～を使用
            ATK_AdmobManager.BannerViewLayout ret = ATK_AdmobManager.BannerViewLayout.Top;
            switch (layout)
            {
                case BannerViewLayout.Top:         ret = ATK_AdmobManager.BannerViewLayout.Top;         break;
                case BannerViewLayout.Bottom:      ret = ATK_AdmobManager.BannerViewLayout.Bottom;      break;
                case BannerViewLayout.TopLeft:     ret = ATK_AdmobManager.BannerViewLayout.TopLeft;     break;
                case BannerViewLayout.TopRight:    ret = ATK_AdmobManager.BannerViewLayout.TopRight;    break;
                case BannerViewLayout.BottomLeft:  ret = ATK_AdmobManager.BannerViewLayout.BottomLeft;  break;
                case BannerViewLayout.BottomRight: ret = ATK_AdmobManager.BannerViewLayout.BottomRight; break;
            }
            return ret;
        }

        /// <summary>
        /// iAdのバナーレイアウトに変換
        /// </summary>
        private ATK_iAdManager.BannerViewLayout toiAdBannerViewLayout(BannerViewLayout layout)
        {
            /// iOS     : ATK_iAdManager.BannerViewLayout.～を使用
            ATK_iAdManager.BannerViewLayout ret = ATK_iAdManager.BannerViewLayout.Top;
            switch (layout)
            {
                case BannerViewLayout.Top:         ret = ATK_iAdManager.BannerViewLayout.Top;         break;
                case BannerViewLayout.Bottom:      ret = ATK_iAdManager.BannerViewLayout.Bottom;      break;
                case BannerViewLayout.TopLeft:     ret = ATK_iAdManager.BannerViewLayout.TopLeft;     break;
                case BannerViewLayout.TopRight:    ret = ATK_iAdManager.BannerViewLayout.TopRight;    break;
                case BannerViewLayout.BottomLeft:  ret = ATK_iAdManager.BannerViewLayout.BottomLeft;  break;
                case BannerViewLayout.BottomRight: ret = ATK_iAdManager.BannerViewLayout.BottomRight; break;
            }
            return ret;
        }


        /// <summary>
        /// Admob Banner 用デフォルトコールバック
        /// </summary>
        private void admobOnBannerLoaded(object sender, EventArgs args)
        {
            print("admobOnBannerLoaded");
            if (!object.ReferenceEquals(_onBannerLoaded, null)) _onBannerLoaded();
            _onBannerLoaded = null;
        }

        private void admobOnBannerFailedToLoad(object sender, GoogleMobileAds.Api.AdFailedToLoadEventArgs args)
        {
            print("admobOnBannerFailedToLoadb : msg = " + args.Message);
            if (!object.ReferenceEquals(_onBannerFailedToLoad, null)) _onBannerFailedToLoad();
            _onBannerFailedToLoad = null;
        }

        /// <summary>
        /// iAd Banner 用デフォルトコールバック
        /// </summary>
        private void iAdOnBannerLoaded()
        {
            Debug.Log("iAdOnBannerLoaded");
            if (!object.ReferenceEquals(_onBannerLoaded, null)) _onBannerLoaded();
            _onBannerLoaded = null;
        }

        private void iAdOnBannerFailedToLoad()
        {
            Debug.Log("iAdOnBannerFailedToLoad");
            if (!object.ReferenceEquals(_onBannerFailedToLoad, null)) _onBannerFailedToLoad();
            _onBannerFailedToLoad = null;
        }

        #endregion バナー




        #region インタースティシャル

        private AdsServices bestInterstitialAdsServices()
        {
            AdsServices type = AdsServices.None;

            if (UnityEngine.Application.platform == RuntimePlatform.Android)
                type = AdsServices.Admob;
            else if (UnityEngine.Application.platform == RuntimePlatform.IPhonePlayer)
                type = AdsServices.iAd;
            else if (UnityEngine.Application.platform == RuntimePlatform.WindowsEditor ||
                     UnityEngine.Application.platform == RuntimePlatform.OSXEditor)
                type = AdsServices.Admob;
            else
                Debug.LogWarning("this platform is not supported");

            return type;
        }


        private bool isReadyInterstitial()
        {
            bool ret = false;
            switch (bestInterstitialAdsServices())
            {
                case AdsServices.Admob: ret = ATK_AdmobManager.Instance.IsReadyInterstitial(); break;
                case AdsServices.iAd: ret = ATK_iAdManager.Instance.IsReadyInterstitial(); break;
            }
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ShowInterstitial(Action onInterstitialLoaded)
        {
            Debug.Log("ShowInterstitial");

            switch (bestInterstitialAdsServices())
            {
                case AdsServices.Admob:
                    if (isReadyInterstitial())
                    {
                        ATK_AdmobManager.Instance.ShowInterstitial();
                    }
                    else
                    {
                        ATK_AdmobManager.Instance.RequestInterstitial(admobOnInterstitialLoaded, null, null, null, null, null);
                    }
                    break;

                case AdsServices.iAd:
                    if (isReadyInterstitial())
                    {
                        ATK_iAdManager.Instance.ShowInterstitial();
                    }
                    else
                    {
                        ATK_iAdManager.Instance.RequestInterstitial(iAdOnInterstitialLoaded);
                    }
                    break;
            }

            if (AdsServices.None != bestInterstitialAdsServices())
            {
                if (!object.ReferenceEquals(onInterstitialLoaded, null)) _onInterstitialLoaded = onInterstitialLoaded;
            }
        }

        public void ShowInterstitial()
        {
            ShowInterstitial(null);
        }

 

        /// <summary>
        /// バナーの破棄　※リクエスト中も含む
        /// </summary>
        public void DestroyInterstitial()
        {
            Debug.Log("DestroyInterstitial");
            switch (bestInterstitialAdsServices())
            {
                case AdsServices.Admob:
                    ATK_AdmobManager.Instance.DestroyInterstitial();
                    break;

                case AdsServices.iAd:
                    ATK_iAdManager.Instance.DestroyInterstitial();
                    break;
            }
            _onInterstitialLoaded = null;
            _onInterstitialFailedToLoad = null;
        }

        



        /// <summary>
        /// Admob Interstitial 用デフォルトコールバック
        /// </summary>

        private void admobOnInterstitialLoaded(object sender, EventArgs args)
        {
            print("admobOnInterstitialLoaded");
            if (!object.ReferenceEquals(_onInterstitialLoaded, null)) _onInterstitialLoaded();
            _onInterstitialLoaded = null;
        }


        /// <summary>
        /// iAd Interstitial 用デフォルトコールバック
        /// </summary>
        private void iAdOnInterstitialLoaded()
        {
            Debug.Log("iAdOnInterstitialLoaded");
            if (!object.ReferenceEquals(_onInterstitialLoaded, null)) _onInterstitialLoaded();
            _onInterstitialLoaded = null;
        }

        #endregion インタースティシャル



        public void ShowVideo()
        {
            switch (bestVideoAdsServices())
            {
                case AdsServices.Admob:
                    break;

                case AdsServices.iAd:
                    break;
            }
        }





    }


}

#pragma warning restore 414
