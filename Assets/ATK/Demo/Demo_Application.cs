﻿using UnityEngine;
using System;

using ATK;

#pragma warning disable 414



public class Demo_Application : MonoBehaviour
{
	
	
	private bool _isEnabled = false;
	
	
	void Awake() 
	{

		
		_isEnabled = true;
		
	}
	
	
	
	
	
	#if UNITY_EDITOR
	float _scale = 1.5f;
	#else
	float _scale = 4.0f;
	#endif
	
	void OnGUI() {
		if ( !_isEnabled ) return;
		
		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();
		
		
		//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();
		
		GUILayout.Space(16);
		
		
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("button")) {
		}
		GUILayout.EndHorizontal();
		

		GUILayout.EndVertical();
		//		GUILayout.EndArea();
	}
	
	
	
}


#pragma warning restore 414



/*



*/





