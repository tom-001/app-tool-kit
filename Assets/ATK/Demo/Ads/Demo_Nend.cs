﻿
/*

nendサンプル　未完成

*/

using UnityEngine;
using System;
using System.Collections;


using ATK.Ads;

//#pragma warning disable 414

public class Demo_Nend : MonoBehaviour
{
	private bool _isEnabled = false;


	void Awake() 
	{
		var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
		/*
        TODO
		// インスタンスの作成
		{
			string prefabName = "ATK_NendManager";
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");

			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			GameObject.DontDestroyOnLoad(obj); // 永続化
		}
        */
		_isEnabled = true;
    }

	
	#if UNITY_EDITOR
	float _scale = 1.5f;
#else
	float _scale = 4.0f;
#endif

    private string _result = string.Empty;
    private GUIStyle _guiStyle = new GUIStyle();
    private GUIStyleState _styleState = new GUIStyleState();


    void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));

		GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();

		//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();



		GUILayout.Space(16);
        

        GUILayout.Space(16);
        _guiStyle.normal = _styleState;
//        GUILayout.Label("Result : " + _result, _guiStyle);


        GUILayout.Space(16);
 
        GUILayout.EndVertical();




        //		GUILayout.EndArea();
    }



    
}


#pragma warning restore 414







