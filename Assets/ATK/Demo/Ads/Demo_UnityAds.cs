﻿
/*

Adsサンプル

*/

using UnityEngine;
using System;
using System.Collections;


using ATK.Ads;

//#pragma warning disable 414

public class Demo_UnityAds: MonoBehaviour
{
	private bool _isEnabled = false;


	void Awake() 
	{
		var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
		
		// インスタンスの作成
		{
			string prefabName = "ATK_UnityAdsManager";
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");

			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			GameObject.DontDestroyOnLoad(obj); // 永続化
		}

		_isEnabled = true;
	}
	
	
	
	#if UNITY_EDITOR
	float _scale = 1.5f;
#else
	float _scale = 4.0f;
#endif

    private string _result = string.Empty;
    private GUIStyle _guiStyle = new GUIStyle();
    private GUIStyleState _styleState = new GUIStyleState();

    void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
        GUILayout.Label("= Unity Ads Demo =");
        GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();

		//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();



		GUILayout.Space(16);


//		GUILayout.BeginHorizontal();
		if (ATK_UnityAdsManager.Instance.IsReadyForSkippableVideo ()) {
			if (GUILayout.Button ("show skippable video")) {
				ATK_UnityAdsManager.Instance.ShowSkippableVideo (onFinished, onSkipped, onFailed, onContinue);
			}
		}
		if (ATK_UnityAdsManager.Instance.IsReadyForRewardVideo()) {
            if (GUILayout.Button("show reward video")) {
                ATK_UnityAdsManager.Instance.ShowRewardVideo(onFinished, onSkipped, onFailed, onContinue);
            }
        }
//       GUILayout.EndHorizontal();


		GUILayout.Space(16);
        _guiStyle.normal = _styleState;
        GUILayout.Label("Result : " + _result, _guiStyle);


        GUILayout.EndVertical();
		//		GUILayout.EndArea();
	}

    private void onFinished()
    {
        _result = "onFinished!";
        Debug.Log(_result);
        _styleState.textColor = Color.white;
    }
    private void onSkipped()
    {
        _result = "onSkipped!";
        Debug.Log(_result);
        _styleState.textColor = Color.yellow;
    }
    private void onFailed()
    {
        _result = "onFailed!";
        Debug.Log(_result);
        _styleState.textColor = Color.red;
    }
    private void onContinue()
    {
        Debug.Log("onContinue!");
    }

}


#pragma warning restore 414


