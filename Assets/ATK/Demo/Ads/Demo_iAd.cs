﻿
/*

Adsサンプル

*/

using UnityEngine;
using System;
using System.Collections;


using ATK.Ads;

//#pragma warning disable 414

public class Demo_iAd : MonoBehaviour
{
	private bool _isEnabled = false;


	void Awake() 
	{
		var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
		
		// インスタンスの作成
		{
			string prefabName = "ATK_iAdManager";
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");

			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			GameObject.DontDestroyOnLoad(obj); // 永続化
		}

		_isEnabled = true;
    }

	
	#if UNITY_EDITOR
	float _scale = 1.5f;
#else
	float _scale = 4.0f;
#endif

    private string _result = string.Empty;
    private string _logstream = string.Empty;
    private GUIStyle _guiStyle = new GUIStyle();
    private GUIStyleState _styleState = new GUIStyleState();

    private ATK_iAdManager.BannerViewType _bannerType = ATK_iAdManager.BannerViewType.SmartBanner;
    private ATK_iAdManager.BannerViewLayout _bannerLayout = ATK_iAdManager.BannerViewLayout.Top;
    private bool _isRequest = false;

    void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));

        GUILayout.Label("= iAd Demo =");

        GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();

		//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();



		GUILayout.Space(16);

        if (GUILayout.Button("Request Banner"))
        {
            ATK_iAdManager.Instance.RequestBanner(onBannerClicked, onBannerLoaded, onBannerFailedToLoad);
        }
        if (GUILayout.Button("Show Banner"))
        {
            ATK_iAdManager.Instance.ShowBanner();
        }
        if (GUILayout.Button("Hide Banner"))
        {
            ATK_iAdManager.Instance.HideBanner();
        }
        if (GUILayout.Button("Destroy Banner"))
        {
            ATK_iAdManager.Instance.DestroyBanner();
        }

        if (GUILayout.Button("Request Interstitial"))
        {
            ATK_iAdManager.Instance.RequestInterstitial(onInterstitialLoaded);
        }
        if (GUILayout.Button("Show Interstitial"))
        {
            ATK_iAdManager.Instance.ShowInterstitial();
        }
        if (GUILayout.Button("Destroy Interstitial"))
        {
            ATK_iAdManager.Instance.DestroyInterstitial();
        }
#if !UNITY_EDITOR && UNITY_IPHONE
        if (!ATK_iAdManager.Instance.IsReadyInterstitial())
        {
            if (GUILayout.Button("Request Interstitial & Show"))
            {
                StartCoroutine(RequestAndShowInterstitial());
            }
        }
#endif

        GUILayout.Space(16);
        _guiStyle.normal = _styleState;
//        GUILayout.Label("Result : " + _result, _guiStyle);


        GUILayout.Space(16);
        if (GUILayout.Button("Request Banner TopCenter"))
        {
            _bannerLayout = ATK_iAdManager.BannerViewLayout.TopCenter;
            _isRequest = true;
        }
        if (GUILayout.Button("Request Banner BottomCenter"))
        {
            _bannerLayout = ATK_iAdManager.BannerViewLayout.BottomCenter;
            _isRequest = true;
        }
        if (GUILayout.Button("Request Banner SmartBanner"))
        {
            _bannerType = ATK_iAdManager.BannerViewType.SmartBanner;
            _isRequest = true;
        }
        if (GUILayout.Button("Request Banner MediumRect"))
        {
            _bannerType = ATK_iAdManager.BannerViewType.MediumRect;
            _isRequest = true;
        }
        if (_isRequest)
        {
            ATK_iAdManager.Instance.BannerType = _bannerType;
            ATK_iAdManager.Instance.BannerLayout = _bannerLayout;
            _isRequest = false;
            ATK_iAdManager.Instance.RequestBanner();
        }
        GUILayout.EndVertical();




        //		GUILayout.EndArea();
    }


        
    private IEnumerator RequestAndShowInterstitial()
    {
        ATK_iAdManager.Instance.RequestInterstitial(onInterstitialLoaded);
        float initStartTime = Time.time;

        do yield return new WaitForSeconds(0.1f);
        while (!ATK_iAdManager.Instance.IsReadyInterstitial());

        Debug.Log(string.Format("Interstitial was initialized in {0:F1} seconds.", Time.time - initStartTime));
        ATK_iAdManager.Instance.ShowInterstitial();
        yield break;
    }



    private void onBannerClicked()
    {
        _result = "onBannerClicked";
        Debug.Log("iAd : "+_result);
    }

    private void onBannerLoaded()
    {
        _result = "onBannerLoaded";
        Debug.Log("iAd : "+_result);
    }

    private void onBannerFailedToLoad()
    {
        _result = "onBannerFailedToLoad";
        Debug.Log("iAd : "+_result);
    }


    private void onInterstitialLoaded()
    {
        _result = "onInterstitialLoaded";
        Debug.Log("iAd : "+_result);
    }

}


#pragma warning restore 414







