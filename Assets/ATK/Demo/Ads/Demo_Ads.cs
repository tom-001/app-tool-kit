﻿
/*

Adsサンプル

*/

using UnityEngine;
using System;
using System.Collections;


using ATK.Ads;

//#pragma warning disable 414

public class Demo_Ads : MonoBehaviour
{
	private bool _isEnabled = false;


	void Awake() 
	{
		var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
		
		// インスタンスの作成
		{
			string prefabName = "ATK_AdsManager";
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");

			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			GameObject.DontDestroyOnLoad(obj); // 永続化
		}

		_isEnabled = true;
    }

	
	#if UNITY_EDITOR
	float _scale = 1.5f;
#else
	float _scale = 4.0f;
#endif

    private string _result = string.Empty;
    private GUIStyle _guiStyle = new GUIStyle();
    private GUIStyleState _styleState = new GUIStyleState();

    private ATK_AdsManager.BannerViewType _bannerType = ATK_AdsManager.BannerViewType.SmartBanner;
    private ATK_AdsManager.BannerViewLayout _bannerLayout = ATK_AdsManager.BannerViewLayout.Top;
    private bool _isRequest = false;

    void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
        GUILayout.Label("= Ads Demo =");
        GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();

		//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();



		GUILayout.Space(16);
        if (GUILayout.Button("Show Banner"))
        {
            ATK_AdsManager.Instance.ShowBanner(onBannerLoaded, onBannerFailedToLoad);
        }
        if (GUILayout.Button("Hide Banner"))
        {
            ATK_AdsManager.Instance.HideBanner();
        }
        if (GUILayout.Button("Destroy Banner"))
        {
            ATK_AdsManager.Instance.DestroyBanner();
        }
        /*
                if (GUILayout.Button("Request Interstitial"))
                {
                ATK_AdsManager.Instance.RequestInterstitial(onInterstitialLoaded, onInterstitialFailedToLoad, onInterstitialOpened, onInterstitialClosing, onInterstitialClosed, onInterstitialLeftApplication);
                }
                if (GUILayout.Button("Show Interstitial"))
                {
                ATK_AdsManager.Instance.ShowInterstitial();
                }
                if (GUILayout.Button("Destroy Interstitial"))
                {
                    ATK_AdsManager.Instance.DestroyInterstitial();
                }
        #if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
                if (!ATK_AdsManager.Instance.IsReadyInterstitial())
                {
                    if (GUILayout.Button("Request Interstitial & Show"))
                    {
                        StartCoroutine(RequestAndShowInterstitial());
                    }
                }
        #endif
        */
        GUILayout.Space(16);
                _guiStyle.normal = _styleState;
        //        GUILayout.Label("Result : " + _result, _guiStyle);


                GUILayout.Space(16);
                if (GUILayout.Button("Show Banner Top"))
                {
                    _bannerLayout = ATK_AdsManager.BannerViewLayout.Top;
                    _isRequest = true;
                }
                if (GUILayout.Button("Show Banner Bottom"))
                {
                    _bannerLayout = ATK_AdsManager.BannerViewLayout.Bottom;
                    _isRequest = true;
                }
                if (GUILayout.Button("Show Banner SmartBanner"))
                {
                    _bannerType = ATK_AdsManager.BannerViewType.SmartBanner;
                    _isRequest = true;
                }
                if (GUILayout.Button("Show Banner MediumBanner"))
                {
                    _bannerType = ATK_AdsManager.BannerViewType.MediumRect;
                    _isRequest = true;
                }
                if (_isRequest)
                {
                    ATK_AdsManager.Instance.DestroyBanner();
                    ATK_AdsManager.Instance.BannerType = _bannerType;
                    ATK_AdsManager.Instance.BannerLayout = _bannerLayout;
                    ATK_AdsManager.Instance.ShowBanner(onBannerLoaded, onBannerFailedToLoad);
                    _isRequest = false;
                }

            
        GUILayout.EndVertical();




        //		GUILayout.EndArea();
    }


    private void onBannerLoaded()
    {
        _result = "onBannerLoaded";
        print("AdsManager : " + _result);
    }

    private void onBannerFailedToLoad()
    {
        _result = "onBannerFailedToLoad";
        print("AdsManager : " + _result);
    }
}


#pragma warning restore 414







