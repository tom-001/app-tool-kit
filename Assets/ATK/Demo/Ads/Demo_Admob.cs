﻿
/*

Adsサンプル

*/

using UnityEngine;
using System;
using System.Collections;


using ATK.Ads;

//#pragma warning disable 414

public class Demo_Admob : MonoBehaviour
{
	private bool _isEnabled = false;


	void Awake() 
	{
		var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
		
		// インスタンスの作成
		{
			string prefabName = "ATK_AdmobManager";
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");

			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			GameObject.DontDestroyOnLoad(obj); // 永続化
		}

		_isEnabled = true;
    }

	
	#if UNITY_EDITOR
	float _scale = 1.5f;
#else
	float _scale = 4.0f;
#endif

    private string _result = string.Empty;
    private GUIStyle _guiStyle = new GUIStyle();
    private GUIStyleState _styleState = new GUIStyleState();

    private ATK_AdmobManager.BannerViewType _bannerType = ATK_AdmobManager.BannerViewType.SmartBanner;
    private ATK_AdmobManager.BannerViewLayout _bannerLayout = ATK_AdmobManager.BannerViewLayout.Top;
    private bool _isRequest = false;
    private ATK_AdmobManager.BannerViewType _subBannerType = ATK_AdmobManager.BannerViewType.SmartBanner;
    private ATK_AdmobManager.BannerViewLayout _subBannerLayout = ATK_AdmobManager.BannerViewLayout.Top;
    private bool _isRequestSub = false;

    void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));

        GUILayout.Label("= Admob Demo =");

        GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();

		//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();



		GUILayout.Space(4);
   

        _guiStyle.normal = _styleState;
//        GUILayout.Label("Result : " + _result, _guiStyle);


        GUILayout.Space(4);


        GUILayout.Label("- Main Banner -");
        _bannerType = ATK_AdmobManager.Instance.BannerType;
        _bannerLayout = ATK_AdmobManager.Instance.BannerLayout;
        /// Request Layout
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Request")) {_isRequest = true;}
        if (GUILayout.Button("Top")){       _bannerLayout = ATK_AdmobManager.BannerViewLayout.Top;      _isRequest = true;}
        if (GUILayout.Button("Bottom")){    _bannerLayout = ATK_AdmobManager.BannerViewLayout.Bottom;   _isRequest = true;}
        GUILayout.EndHorizontal();

        /// Request Type
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Banner")){        _bannerType = ATK_AdmobManager.BannerViewType.Banner;       _isRequest = true;}
        if (GUILayout.Button("Leaderboard")){   _bannerType = ATK_AdmobManager.BannerViewType.Leaderboard;  _isRequest = true;}
        if (GUILayout.Button("IABBanner")){     _bannerType = ATK_AdmobManager.BannerViewType.IABBanner;    _isRequest = true;}
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("MediumRectangle")){   _bannerType = ATK_AdmobManager.BannerViewType.MediumRectangle;  _isRequest = true;}
        if (GUILayout.Button("SmartBanner")){       _bannerType = ATK_AdmobManager.BannerViewType.SmartBanner;      _isRequest = true;}
        GUILayout.EndHorizontal();
        if (ATK_AdmobManager.Instance.IsReadyBanner())
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Show")) { ATK_AdmobManager.Instance.ShowBanner(); }
            if (GUILayout.Button("Hide")) { ATK_AdmobManager.Instance.HideBanner(); }
            if (GUILayout.Button("Destroy")) { ATK_AdmobManager.Instance.DestroyBanner(); }
            GUILayout.EndHorizontal();
        }

        if (_isRequest)
        {
            ATK_AdmobManager.Instance.BannerType = _bannerType;
            ATK_AdmobManager.Instance.BannerLayout = _bannerLayout;
            _isRequest = false;
            ATK_AdmobManager.Instance.RequestBanner();
        }
    

        GUILayout.Space(4);


        GUILayout.Label("- Sub Banner -");
        _subBannerType = ATK_AdmobManager.Instance.SubBannerType;
        _subBannerLayout = ATK_AdmobManager.Instance.SubBannerLayout;
        /// Request Layout
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Request")){_isRequestSub = true;}
        if (GUILayout.Button("Top")) {      _subBannerLayout = ATK_AdmobManager.BannerViewLayout.Top; _isRequestSub = true; }
        if (GUILayout.Button("Bottom")){    _subBannerLayout = ATK_AdmobManager.BannerViewLayout.Bottom;    _isRequestSub = true;}
        GUILayout.EndHorizontal();
        /// Request Type
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Banner")){        _subBannerType = ATK_AdmobManager.BannerViewType.Banner;      _isRequestSub = true;}
        if (GUILayout.Button("Leaderboard")){   _subBannerType = ATK_AdmobManager.BannerViewType.Leaderboard;   _isRequestSub = true;}
        if (GUILayout.Button("IABBanner")){     _subBannerType = ATK_AdmobManager.BannerViewType.IABBanner;     _isRequestSub = true;}
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("MediumRectangle")){   _subBannerType = ATK_AdmobManager.BannerViewType.MediumRectangle;   _isRequestSub = true;}
        if (GUILayout.Button("SmartBanner")){       _subBannerType = ATK_AdmobManager.BannerViewType.SmartBanner;       _isRequestSub = true;}
        GUILayout.EndHorizontal();
        if (ATK_AdmobManager.Instance.IsReadySubBanner())
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Show")) { ATK_AdmobManager.Instance.ShowSubBanner(); }
            if (GUILayout.Button("Hide")) { ATK_AdmobManager.Instance.HideSubBanner(); }
            if (GUILayout.Button("Destroy")) { ATK_AdmobManager.Instance.DestroySubBanner(); }
            GUILayout.EndHorizontal();
        }
        if (_isRequestSub)
        {
            ATK_AdmobManager.Instance.SubBannerType = _subBannerType;
            ATK_AdmobManager.Instance.SubBannerLayout = _subBannerLayout;
            _isRequestSub = false;
            ATK_AdmobManager.Instance.RequestSubBanner();
        }


        GUILayout.Space(4);


        GUILayout.Label("- Interstitial -");
        /// Request Layout
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Request")) { ATK_AdmobManager.Instance.RequestInterstitial(onInterstitialLoaded, onInterstitialFailedToLoad, onInterstitialOpened, onInterstitialClosing, onInterstitialClosed, onInterstitialLeftApplication); }
#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IPHONE)
        if (!ATK_AdmobManager.Instance.IsReadyInterstitial())
        {
            if (GUILayout.Button("Request Interstitial & Show"))
            {
                StartCoroutine(RequestAndShowInterstitial());
            }
        }
#endif
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        if (ATK_AdmobManager.Instance.IsReadyInterstitial())
        {
            if (GUILayout.Button("Show")) {     ATK_AdmobManager.Instance.ShowInterstitial(); }
            if (GUILayout.Button("Destroy")) {  ATK_AdmobManager.Instance.DestroyInterstitial(); }
        }
        GUILayout.EndHorizontal();



		GUILayout.Space(4);

		#if false //2016/04/10 Android実機検証で表示されなかった　サンプルコードの方も同様

		GUILayout.Label("- Video -");
		/// Request Layout
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Request")) { ATK_AdmobManager.Instance.RequestVideo(); }
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (ATK_AdmobManager.Instance.IsReadyVideo())
		{
			if (GUILayout.Button("Show")) {     ATK_AdmobManager.Instance.ShowVideo(); }
		}
		GUILayout.EndHorizontal();
#endif






        GUILayout.EndVertical();




        //		GUILayout.EndArea();
    }


        
    private IEnumerator RequestAndShowInterstitial()
    {
        ATK_AdmobManager.Instance.RequestInterstitial(onInterstitialLoaded, onInterstitialFailedToLoad, onInterstitialOpened, onInterstitialClosing, onInterstitialClosed, onInterstitialLeftApplication);
        float initStartTime = Time.time;

        do yield return new WaitForSeconds(0.1f);
        while (!ATK_AdmobManager.Instance.IsReadyInterstitial());

        Debug.Log(string.Format("Interstitial was initialized in {0:F1} seconds.", Time.time - initStartTime));
        ATK_AdmobManager.Instance.ShowInterstitial();
        yield break;
    }

    private void onAdLoaded(object sender, EventArgs args)
    {
        _result = "onAdLoaded";
        print("Admob : "+_result);
    }

    private void onAdFailedToLoad(object sender, GoogleMobileAds.Api.AdFailedToLoadEventArgs args)
    {
        _result = "onFailedToReceiveAd : " + args.Message;
        print("Admob : "+_result);
    }

    private void onAdOpened(object sender, EventArgs args)
    {
        _result = "onAdOpened";
        print("Admob : "+_result);
    }

    private void onAdClosing(object sender, EventArgs args)
    {
        _result = "onAdClosing";
        print("Admob : "+_result);
    }

    private void onAdClosed(object sender, EventArgs args)
    {
        _result = "onAdClosed";
        print("Admob : "+_result);
    }

    private void onAdLeftApplication(object sender, EventArgs args)
    {
        _result = "onAdLeftApplication";
        print("Admob : "+_result);
    }



    private void onInterstitialLoaded(object sender, EventArgs args)
    {
        _result = "onInterstitialLoaded";
        print("Admob : "+_result);
    }

    private void onInterstitialFailedToLoad(object sender, GoogleMobileAds.Api.AdFailedToLoadEventArgs args)
    {
        _result = "onInterstitialFailedToLoad : " + args.Message;
        print("Admob : "+_result);
    }

    private void onInterstitialOpened(object sender, EventArgs args)
    {
        _result = "onInterstitialOpened";
        print("Admob : "+_result);
    }

    private void onInterstitialClosing(object sender, EventArgs args)
    {
        _result = "onInterstitialClosing";
        print("Admob : "+_result);
    }

    private void onInterstitialClosed(object sender, EventArgs args)
    {
        _result = "onInterstitialClosed";
        print("Admob : "+_result);
    }

    private void onInterstitialLeftApplication(object sender, EventArgs args)
    {
        _result = "onInterstitialLeftApplication";
        print("Admob : "+_result);
    }
}


#pragma warning restore 414







