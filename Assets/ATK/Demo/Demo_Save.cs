﻿

/*

セーブ・ロードのサンプル
 
*/
 


#define HASH_TEST
#define DICT_TEST

#define XML_TEST

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using ATK;
using ATK.Utilities;
using ATK.Extension;

//#pragma warning disable 414


public class Demo_Save : MonoBehaviour
{


	private bool _isEnabled = false;
	
	
	void Awake() 
	{
		var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
		
		// SaveManagerインスタンスの作成
		{
			string prefabName = "ATK_SaveManager";
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
			
			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			GameObject.DontDestroyOnLoad(obj); // 永続化
		}
		
		_isEnabled = true;
		


   #if HASH_TEST
		_hogeData.Arguments = SerializableHashtable.CreateCollection(
			KEY_POSITION, Vector3.one,
			KEY_ROTATION, Quaternion.identity
			);
		_hogeData.MyProfile.Arguments = SerializableHashtable.CreateCollection(
			KEY_POSITION, Vector3.one,
			KEY_ROTATION, Quaternion.identity
			);
#endif
#if DICT_TEST
		_hogeData.Flags = SerializableDictionary<string, object>.CreateCollection(
			KEY_POSITION, Vector3.one,
			KEY_ROTATION, Quaternion.identity
			);
		_hogeData.MyProfile.Flags = SerializableDictionary<string, object>.CreateCollection(
			KEY_POSITION, Vector3.one,
			KEY_ROTATION, Quaternion.identity
			);
#endif

	}

	#if UNITY_EDITOR
	float _scale = 1.0f;
	#else
	float _scale = 2.5f;
	#endif

	private string _result = string.Empty;
	private GUIStyle _guiStyle = new GUIStyle();
	private GUIStyleState _styleState = new GUIStyleState();

	void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
        GUILayout.Label("= Serialize Demo =");
        GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();

//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();

		// パス確認
		GUILayout.Space(8);
		GUILayout.Label("External : "+ ATK.Plugin.Storage.GetExternalStorage());
		GUILayout.Label("Internal : "+ ATK.Plugin.Storage.GetInternalStorage());
		GUILayout.Label("dataPath : " + UnityEngine.Application.dataPath);
		GUILayout.Label("persistentDataPath : " + UnityEngine.Application.persistentDataPath);
		GUILayout.Label("streamingAssetsPath : " + UnityEngine.Application.streamingAssetsPath);
		GUILayout.Label("temporaryCachePath : " + UnityEngine.Application.temporaryCachePath);



		GUILayout.Space(8);
		if (GUILayout.Button("Save to persistentDataPath, by binary")) {
			_result = "Save Succeed : persistentDataPath";
			_styleState.textColor = Color.white;
			if ( ATK.Utilities.SaveData.EError.Succeed
			    != ATK_SaveManager.Instance.Save( _hogeData, UnityEngine.Application.persistentDataPath + "/persistentDataPath.sav", onComplete )
			    ) {
				_result = "Save Failed : persistentDataPath ";
				_styleState.textColor = Color.yellow;
			}
		}
		if (GUILayout.Button("Save to temporaryCachePath, by binary")) {
			_result = "Save Succeed : temporaryCachePath";
			_styleState.textColor = Color.white;
			if ( ATK.Utilities.SaveData.EError.Succeed
			    != ATK_SaveManager.Instance.Save( _hogeData, UnityEngine.Application.temporaryCachePath + "/temporaryCachePath.sav", onComplete )
			    ) {
				_result = "Save Failed : temporaryCachePath ";
				_styleState.textColor = Color.yellow;
			}
		}
		if (GUILayout.Button("Save to persistentDataPath, by XML")) {
			_result = "Save XML Succeed : persistentDataPath";
			_styleState.textColor = Color.white;
			if ( ATK.Utilities.SaveData.EError.Succeed
			    != ATK_SaveManager.Instance.SaveXml( _hogeData, UnityEngine.Application.persistentDataPath + "/persistentDataPath.xml", onComplete )
				) {
				_result = "Save XML Failed : persistentDataPath ";
				_styleState.textColor = Color.yellow;
			}
		}
		GUILayout.Space(8);
		if (GUILayout.Button("Load from persistentDataPath, by binary")) {
			_result = "Load Succeed : persistentDataPath";
			_styleState.textColor = Color.white;
			if ( ATK.Utilities.SaveData.EError.Succeed
			    != ATK_SaveManager.Instance.Load(out _hogeData, UnityEngine.Application.persistentDataPath + "/persistentDataPath.sav", onComplete)
			    ) {
				_result = "Load Failed : persistentDataPath ";
				_styleState.textColor = Color.yellow;
			}
		}
		if (GUILayout.Button("Load from temporaryCachePath, by binary")) {
			_result = "Load Succeed : temporaryCachePath";
			_styleState.textColor = Color.white;
			if ( ATK.Utilities.SaveData.EError.Succeed
			    != ATK_SaveManager.Instance.Load(out _hogeData, UnityEngine.Application.temporaryCachePath + "/temporaryCachePath.sav", onComplete )
			    ) {
				_result = "Load Failed : temporaryCachePath ";
				_styleState.textColor = Color.yellow;
			}
		}
		if (GUILayout.Button("Load from persistentDataPath, by XML")) {
			_result = "Load XML Succeed : persistentDataPath";
			_styleState.textColor = Color.white;
			if ( ATK.Utilities.SaveData.EError.Succeed
				    != ATK_SaveManager.Instance.LoadXml( out _hogeData, UnityEngine.Application.persistentDataPath + "/persistentDataPath.xml", onComplete )
			    ) {
				_result = "Load XML Failed : persistentDataPath ";
				_styleState.textColor = Color.yellow;
			}
		}
		_guiStyle.normal = _styleState;
		GUILayout.Label("Result : " + _result, _guiStyle);


#if UNITY_EDITOR
		// 簡易チェック
		GUILayout.Space(8);
		if (GUILayout.Button("check data")) {
			Debug.Log("Date : " + _hogeData.MyProfile.Name);
			Vector3 pos = new Vector3();
			Debug.Log(pos.ToString());

			#if HASH_TEST
			_hogeData.Arguments.TryGetValue<string, Vector3>(KEY_POSITION, out pos);
			pos = _hogeData.Arguments.GetValueOrDefault<string, Vector3>(KEY_POSITION);
			#endif

			#if DICT_TEST
			object obj;
			_hogeData.Flags.TryGetValue(KEY_POSITION, out obj);
			pos = (Vector3)obj;
			#endif

			Debug.Log(pos.ToString());
		}

#endif

		GUILayout.EndVertical();
//		GUILayout.EndArea();
	}







	//http://smdn.jp/programming/netfx/serialization/2_xmlserializer/

	string KEY_POSITION		 = "position";
	string KEY_ROTATION		 = "rotation";

	/// データクラス　
	/// このクラスをシリアライズ
	/// 適当に拡張して、XMLセーブで確認できればシリアライズ成功
	/// 
	[System.Xml.Serialization.XmlRoot("データです")]
	public class HogeData
	{
		[System.Xml.Serialization.XmlElement("日付")] // エレメント名を指定　未指定の場合は変数名になる
		public DateTime Date = DateTime.Now;
		public Profile MyProfile = new Profile("山田太郎", 20);

		#if HASH_TEST
		public SerializableHashtable Arguments = new SerializableHashtable(0);
		#endif
		#if DICT_TEST
		public SerializableDictionary<string, object> Flags = new SerializableDictionary<string, object>();
#endif




        public enum ItemFlag
        {
            ItemA,
            ItemB,
            ItemC,
            Max,
        }
        [System.Xml.Serialization.XmlElement("フラグ")] // エレメント名を指定
        [System.Xml.Serialization.XmlIgnore] // シリアライズ対象から除外するための属性
        public TypedBitArray<ItemFlag> Bits = new TypedBitArray<ItemFlag>(ItemFlag.Max);

 	}
	public class Profile
	{
		public Profile() {} //コンストラクタを使う場合、デフォルトコンストラクタも用意する
		public Profile(string name, int age) { this.Name = name; this.Age = age; }
		public string Name;
		public int Age;

		#if HASH_TEST
		public SerializableHashtable Arguments = new SerializableHashtable(0);
		#endif

		[System.Xml.Serialization.XmlIgnore] // シリアライズ対象から除外するための属性
		public Dictionary<string, object> Flags = new Dictionary<string, object>();

		#if DICT_TEST
		protected SerializableDictionary<string, object> _flags = new SerializableDictionary<string, object>();
		#endif
	}
	private HogeData _hogeData = new HogeData();

	// callback
	void onStart()
	{
		Debug.Log("onStart");
	}
	void onComplete()
	{
		Debug.Log("onComplete");
	}


}



