﻿using UnityEngine;
using System;

using ATK.Localization;

#pragma warning disable 414



public class Demo_Localization : MonoBehaviour
{


	private bool _isEnabled = false;
	
	
	void Awake() 
	{
		var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
		
		// LanguageManagerインスタンスの作成
		{
			string prefabName = "ATK_LanguageManager";
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
			
			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			GameObject.DontDestroyOnLoad(obj); // 永続化
		}
		
		_isEnabled = true;

	}
	
	
	
	
	
	#if UNITY_EDITOR
	float _scale = 1.5f;
	#else
	float _scale = 4.0f;
	#endif

	void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
        GUILayout.Label("= Localization Demo =");
        GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();

		
		//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();
		
		GUILayout.Space(16);


		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Load Japanese")) {
			ATK_LanguageManager.Instance.LoadLanguage(SystemLanguage.Japanese);
		}
		if (GUILayout.Button("Load English")) {
			ATK_LanguageManager.Instance.LoadLanguage(SystemLanguage.English);
		}
		if (GUILayout.Button("Load Chinese")) {
			ATK_LanguageManager.Instance.LoadLanguage(SystemLanguage.Chinese);
		}
		GUILayout.EndHorizontal();

		
		GUILayout.Label("設定言語 : " + ATK_LanguageManager.Instance.CurrentLanguege);
	    GUILayout.Label("メッセージの「いいえ」 : " + ATK_LanguageManager.Instance.MsgBox.ボタン_いいえ);
	    GUILayout.Label("コンフィグの「タイトル」 : " + ATK_LanguageManager.Instance.Config.コンフィグ_タイトル);


	
		GUILayout.EndVertical();
		//		GUILayout.EndArea();
	}
	
	

}


#pragma warning restore 414



/*



*/





