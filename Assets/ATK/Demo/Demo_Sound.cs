﻿
/*

サウンドの再生サンプル

*/

using UnityEngine;
using System;
using System.Collections;


using ATK.Audio;

//#pragma warning disable 414

public class Demo_Sound : MonoBehaviour
{
	private bool _isEnabled = false;


	void Awake() 
	{
		var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
		
		// SoundManagerインスタンスの作成　※Awakde()で作成は[NG]
		{
			string prefabName = "ATK_SoundManager";
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");

			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			GameObject.DontDestroyOnLoad(obj); // 永続化
		}

		_isEnabled = true;
	}
	
	
	
	#if UNITY_EDITOR
	float _scale = 1.5f;
	#else
	float _scale = 4.0f;
	#endif

	void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
        GUILayout.Label("= Sound Demo =");
        GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();

		//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();



		GUILayout.Space(16);


		// システムSE
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Stop SE")) {
			ATK_SoundManager.Instance.StopSystemSE();
		}
		if (GUILayout.Button("SE1")) {
			ATK_SoundManager.Instance.PlaySystemSE(SoundID.SYS_SE_OK);
		}
		if (GUILayout.Button("SE2")) {
			ATK_SoundManager.Instance.PlaySystemSE(SoundID.SYS_SE_CANCEL);
		}
		if (GUILayout.Button("SE3")) {
			// インスタンスを取得して再生する場合
			var se = ATK_Audio_SystemSEPlayerManager.Instance;
			var req = se.Player[0].Create(); // チャンネル0でリクエスト
			req.Clip = ATK.Audio.SoundBankDB.Find(ATK_SoundManager.SOUND_BANK_ID_SYSTEM_SE).GetAudioClip(SoundID.SYS_SE_OPEN);					
			// req.Output = 好きなAudioMixerGroup;
			se.Player[0].Add(req);	

		}
		GUILayout.EndHorizontal();
		GUILayout.Label("SE音量 = " + ATK_SoundManager.Instance.GetSystemSEVolume());
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("+")) {
			ATK_SoundManager.Instance.SetSystemSEVolume( Mathf.Clamp01( ATK_SoundManager.Instance.GetSystemSEVolume() + 0.1f) );
		}
		if (GUILayout.Button("-")) {
			ATK_SoundManager.Instance.SetSystemSEVolume( Mathf.Clamp01( ATK_SoundManager.Instance.GetSystemSEVolume() - 0.1f) );
		}
		GUILayout.EndHorizontal();



		GUILayout.Space(16);


		// BGM
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Stop BGM")) {
			ATK_SoundManager.Instance.StopBGM(0.5f);
		}
		if (GUILayout.Button("BGM1")) {
			ATK_SoundManager.Instance.PlayBGM(SoundID.BGM_TITLE);
		}
		if (GUILayout.Button("BGM2")) {
			ATK_SoundManager.Instance.PlayBGM(SoundID.BGM_GAME);
		}
		GUILayout.EndHorizontal();

		GUILayout.Label("BGM音量 = " + ATK_SoundManager.Instance.GetBGMVolume());
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("+")) {
			ATK_SoundManager.Instance.SetBGMVolume( Mathf.Clamp01( ATK_SoundManager.Instance.GetBGMVolume() + 0.1f) );
		}
		if (GUILayout.Button("-")) {
			ATK_SoundManager.Instance.SetBGMVolume( Mathf.Clamp01( ATK_SoundManager.Instance.GetBGMVolume() - 0.1f) );
		}
		GUILayout.EndHorizontal();
		


	
		GUILayout.EndVertical();
		//		GUILayout.EndArea();
	}
	
	

}


#pragma warning restore 414



/*

==================================================
再生リクエストについて
==================================================
同じチャンネルを使用して再生リクエストした場合、先のリクエストが鳴り終わるのを待ってから再生される
先のリクエストが roop の場合、停止しない限り次のリクエストが再生されないので注意

var se = ATK_Audio_SEPlayerManager.Instance;

var req1 = se.Player[0].Create();
req1.Clip = ATK.Audio.SoundBankDB.Find("SOUND_BANK_ID").GetAudioClip("SE_NAME1");					
se.Player[0].Add(req1);	

var req2 = se.Player[0].Create();
req2.Clip = ATK.Audio.SoundBankDB.Find("SOUND_BANK_ID").GetAudioClip("SE_NAME2");					
se.Player[0].Add(req2);	




==================================================
音声ファイルについて
==================================================
[Assets]
├ [ATK]
│ └ [Resources]　【※Unityフォルダ１】また、以下のリソースはアプリに内包されます。
│ 　 └ [Audio]　汎用的な音を入れる。追加はAssets/Resources/Audio/以下に入れる。同じフォルダ内のファイル名は重複しないようにする。
│ 　 　 ├ [BGM]　フォルダ内のファイル名は重複しないようにする。
│ 　 　 ├ [SystemSE]　フォルダ内のファイル名は重複しないようにする。
│ 　 　 ├ [SE]　フォルダ内のファイル名は重複しないようにする。
│ 　 　 └ [Voice]　フォルダ内のファイル名は重複しないようにする。
｜
└ [Resources]　【※Unityフォルダ１】また、以下のリソースはアプリに内包されます。
　 └ [Audio]
　 　 ├ [BGM]
　 　 ├ [SystemSE]
　 　 ├ [SE]
　 　 └ [Voice]
　 　 └ [Game]

Resources 以下の同じフォルダ構成は、同じデータバンクに読み込まれます。
上のフォルダ構成の場合、
ATK.Audio.SoundBankDB.Add( "SE", ATK.Audio.SoundBank.Create( "Audio/SE" ) )の読み込みで
Assets/ATK/Resources/Audio/SE/ と 
Assets/Resources/Audio/SE/ が
同じデータバンクになります。
多くのファイルがある場合は、メモリ不足を考慮して工夫する必要あり？





==================================================
再生識別子(SoundID)について
==================================================
リソースにある音声ファイル名

*/





