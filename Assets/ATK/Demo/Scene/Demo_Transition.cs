﻿
/*

トランジションのサンプル

*/
#define USING_TRANSITION_PRO	// ATK_TransitionProを使用する　※有料アセット必須


using UnityEngine;

using ATK.Scene;

//#pragma warning disable 414


public class Demo_Transition : MonoBehaviour
{
	
	[SerializeField] Canvas _canvas;
	private ATK_TransitionInterface _transition;
	private bool _isEnabled = false;
	public Texture texture;
	public Sprite sprite;

	void Start()
	{
		{		
			var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
			
			// Transitionインスタンスの作成
			#if USING_TRANSITION_PRO
			string prefabName = "ATK_TransitionPro";
			#else
			string prefabName = "ATK_Transition";
			#endif
			ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
			
			GameObject prefab = prefabs[prefabName];			
			GameObject obj = Instantiate(prefab) as GameObject;
			obj.name = prefabName;
			_transition = obj.GetComponent<ATK_TransitionInterface>();
			_transition.SetParentCanvas(_canvas);
		}

		_isEnabled = true;
	}

    private void onStartFadeInCallback()
	{
		Debug.Log("onStartFadeInCallback");
	}
    private void onFinishedFadeInCallback()
    {
        Debug.Log("onFinishedFadeInCallback");
    }
    private void onStartFadeOutCallback()
	{
		Debug.Log("onStartFadeOutCallback");
	}
	private void onFinishedFadeOutCallback()
	{
		Debug.Log("onFinishedFadeOutCallback");
	}


#if UNITY_EDITOR
    float _scale = 1.5f;
	#else
	float _scale = 4.0f;
	#endif
	
	void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();
		
		
		//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();
		
		
		GUILayout.Space(16);
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("black")) {_transition.SetFadeColor(Color.black);}
		if (GUILayout.Button("white")) {_transition.SetFadeColor(Color.white);}
		if (GUILayout.Button("red"))   {_transition.SetFadeColor(Color.red);}
		if (GUILayout.Button("green")) {_transition.SetFadeColor(Color.green);}
		if (GUILayout.Button("blue"))  {_transition.SetFadeColor(Color.blue);}
		if (GUILayout.Button("gray"))  {_transition.SetFadeColor(Color.gray);}
		if (GUILayout.Button("yellow")){_transition.SetFadeColor(Color.yellow);}
		GUILayout.EndHorizontal();
		
		GUILayout.Space(8);
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("set texture")) {
			_transition.SetFadeImage(texture);
		}
		if (GUILayout.Button("set sprite")) {
			_transition.SetFadeImage(sprite.texture);
		}
		GUILayout.EndHorizontal();


		GUILayout.Space(16);
		if (GUILayout.Button("FadeIn")) {
            _transition.OnStartFadeOutEvent += onStartFadeInCallback;
            _transition.OnFinishedFadeInEvent += onFinishedFadeInCallback;
            _transition.OnStartFadeOutEvent += onStartFadeOutCallback;
            _transition.OnFinishedFadeOutEvent += onFinishedFadeOutCallback;
            _transition.StartFadeOut();
		}
		if (GUILayout.Button("FadeOut")) {
            _transition.StartFadeIn(onStartFadeInCallback, onFinishedFadeInCallback, onStartFadeOutCallback, onFinishedFadeOutCallback);
        }


        GUILayout.EndVertical();
		//		GUILayout.EndArea();
		
		//		GUI.matrix = mat;
		
		GUILayout.Space(100);
		
	}
	
}



