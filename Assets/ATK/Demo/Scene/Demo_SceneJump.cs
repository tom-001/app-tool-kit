﻿
/*

シーンジャンプのサンプル

*/


using UnityEngine;

using ATK.Scene;

//#pragma warning disable 414


public class Demo_SceneJump : MonoBehaviour
{


	private bool _isEnabled = false;
	public Texture texture;
	public Sprite sprite;

	
	void Awake() 
	{
		string prefabName = "ATK_SceneManager";
		/// このサンプルはシーン１・２で同じスクリプト(このスクリプト)を使用している
		/// そのため、シーン遷移で毎回Instantiateを行うことになる
		/// シングルトンなので重複作成は避けられるが、
		/// Instantiateを行うとテクスチャ等が初期化されるので
		/// 無駄なインスタンス化を避ける対策をする
		if (GameObject.Find(prefabName) == null ) 
		{ 
			var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 
			
			// SceneManagerインスタンスの作成
			{
				ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
					
				GameObject prefab = prefabs[prefabName];			
				GameObject obj = Instantiate(prefab) as GameObject;
				obj.name = prefabName;
				GameObject.DontDestroyOnLoad(obj); // 永続化
			}
		}

		_isEnabled = false;
	}

	void Start()
	{
        // ちょっとウェイト
		Invoke("_func", 1f);
	}

	private void _func()
	{
		// フェードイン
		ATK_SceneManager.Instance.FadeIn(null, onFinishedFadeInCallback);
	}

	private void onStartFadeInCallback()
	{
		Debug.Log("onStartFadeInCallback");
	}
    private void onFinishedFadeInCallback()
    {
        Debug.Log("onFinishedFadeInCallback");
        _isEnabled = true;
    }
    private void onStartFadeOutCallback()
    {
        Debug.Log("onStartFadeOutCallback");
        _isEnabled = false;
    }
	private void onFinishedFadeOutCallback()
	{
		Debug.Log("onFinishedFadeOutCallback");
	}




#if UNITY_EDITOR
    float _scale = 1.0f;
	#else
	float _scale = 2.5f;
	#endif

	void OnGUI() {
		if ( !_isEnabled ) return;

		GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();

//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();

		GUILayout.Space(8);
		GUILayout.Label("Scene : "+ Application.loadedLevelName);

		if ("Demo_SceneJump1" == Application.loadedLevelName) {
			if (GUILayout.Button("Jump Scene 2")) {
                ATK_SceneManager.Instance.OnStartFadeInEvent += onStartFadeInCallback;
                ATK_SceneManager.Instance.OnFinishedFadeInEvent += onFinishedFadeInCallback;
                ATK_SceneManager.Instance.OnStartFadeOutEvent += onStartFadeOutCallback;
                ATK_SceneManager.Instance.OnStartFadeOutEvent += onFinishedFadeOutCallback;
                ATK_SceneManager.Instance.JumpScene("Demo_SceneJump2");
                _isEnabled = false;
			}
		}
		else {
			if (GUILayout.Button("Jump Scene 1")) {
                ATK_SceneManager.Instance.JumpScene("Demo_SceneJump1", onStartFadeInCallback, onFinishedFadeInCallback, onStartFadeOutCallback, onFinishedFadeOutCallback);
                _isEnabled = false;
			}
		}
        if ("Demo_SceneJump1" == Application.loadedLevelName)
        {
            if (GUILayout.Button("Call Scene 2"))
            {
                ATK_SceneManager.Instance.OnStartFadeInEvent += onStartFadeInCallback;
                ATK_SceneManager.Instance.OnFinishedFadeInEvent += onFinishedFadeInCallback;
                ATK_SceneManager.Instance.OnStartFadeOutEvent += onStartFadeOutCallback;
                ATK_SceneManager.Instance.OnStartFadeOutEvent += onFinishedFadeOutCallback;
                ATK_SceneManager.Instance.CallScene("Demo_SceneJump2");
                _isEnabled = false;
            }
        }
        else
        {
            if (GUILayout.Button("Call Scene 1"))
            {
                ATK_SceneManager.Instance.CallScene("Demo_SceneJump1", onStartFadeInCallback, onFinishedFadeInCallback, onStartFadeOutCallback, onFinishedFadeOutCallback);
                _isEnabled = false;
            }
        }
        if (GUILayout.Button("Return Scene"))//シーンがスタックにないと例外になるので注意
        {
            ATK_SceneManager.Instance.ReturnScene( onStartFadeInCallback, onFinishedFadeInCallback, onStartFadeOutCallback, onFinishedFadeOutCallback);
            _isEnabled = false;
        }

        if ("Demo_SceneJump1" == Application.loadedLevelName) {
			GUILayout.Space(8);
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("black")) {ATK_SceneManager.Instance.SetFadeColor(Color.black);}
			if (GUILayout.Button("white")) {ATK_SceneManager.Instance.SetFadeColor(Color.white);}
			if (GUILayout.Button("red"))   {ATK_SceneManager.Instance.SetFadeColor(Color.red);}
			if (GUILayout.Button("green")) {ATK_SceneManager.Instance.SetFadeColor(Color.green);}
			if (GUILayout.Button("blue"))  {ATK_SceneManager.Instance.SetFadeColor(Color.blue);}
			if (GUILayout.Button("gray"))  {ATK_SceneManager.Instance.SetFadeColor(Color.gray);}
			if (GUILayout.Button("yellow")){ATK_SceneManager.Instance.SetFadeColor(Color.yellow);}
			GUILayout.EndHorizontal();

			GUILayout.Space(8);
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("set texture")) {
				ATK_SceneManager.Instance.SetFadeImage(texture);
			}
			if (GUILayout.Button("set sprite")) {
				ATK_SceneManager.Instance.SetFadeImage(sprite);
			}
			GUILayout.EndHorizontal();
		}

		GUILayout.EndVertical();
//		GUILayout.EndArea();
	}



}



