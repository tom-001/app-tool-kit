﻿#if !UNITY_5_0 && (UNITY_5 || UNITY_6 || UNITY_7 || UNITY_8 || UNITY_9)

/*
 * Unity5.1?以降 UnityEngine.Assertions.Assert が追加されたのでそっちを使う
 * 
 * 使う場合は Player Settings > Other Settings > Configuration > Scripting Define Symbols で UNITY_ASSERTIONS を定義する
 * Unityエディタ上もしくは Development Build の場合に UNITY_ASSERTIONS は自動で定義される
 * リリースビルドで有効にする場合は、ビルド時のオプションで BuildOptions.ForceEnableAssertions を指定する
 * 
 */



using UnityEngine;
using System.Diagnostics;
using System;

namespace ATK
{
	/// <summary>
	/// Assert
	/// </summary>
	public static class Assert
	{
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void AreEqual<T>(
			T a,
			T b,
			string msg
			)
		{
			UnityEngine.Assertions.Assert.AreEqual(a, b, msg);
		}
		
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void AreNotEqual<T>(
			T a,
			T b,
			string msg
			)
		{
			UnityEngine.Assertions.Assert.AreNotEqual(a, b, msg);
		}	
		
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void IsNull(
			object obj,
			string msg
			)
		{
			UnityEngine.Assertions.Assert.IsNull(obj, msg);
		}	
		
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void IsNotNull(
			object obj,
			string msg
			)
		{
			UnityEngine.Assertions.Assert.IsNotNull(obj, msg);
		}
		
		
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void IsTrue(
			bool condition,
			string msg
			)
		{
			UnityEngine.Assertions.Assert.IsTrue(condition, msg);
		}
		
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void IsFalse(
			bool condition,
			string msg
			)
		{
			UnityEngine.Assertions.Assert.IsFalse(condition, msg);
		}
		
		
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void Fail(string msg)
		{
			
			UnityEngine.Debug.LogError(msg);
			throw new UnityEngine.UnityException();	
		}
		
		
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void IsEmpty(
			string str,
			string msg
			)
		{
			if( !string.IsNullOrEmpty(str) ){
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();
			}
		}
		
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void IsNotEmpty(
			string str,
			string msg
			)
		{
			if( string.IsNullOrEmpty(str) ){
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();
			}
		}
		
		/// <summary>
		/// 値がほぼ等しい
		/// </summary>
		/// <param name="tolerance">許容誤差</param>
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void AreApproximatelyEqual(
			float a,
			float b,
			float tolerance,
			string msg
			)
		{
			UnityEngine.Assertions.Assert.AreApproximatelyEqual(a, b, tolerance, msg);
		}
		
		/// <summary>
		/// 値がほぼ等しい デフォルトの許容誤差は0.00001f
		/// </summary>
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void AreApproximatelyEqual(
			float a,
			float b,
			string msg
			)
		{
			AreApproximatelyEqual(a, b, msg);
		}
		
		/// <summary>
		/// 値がほぼ等しくない
		/// </summary>
		/// <param name="tolerance">許容誤差</param>Not
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void AreNotApproximatelyEqual(
			float a,
			float b,
			float tolerance,
			string msg
			)
		{
			UnityEngine.Assertions.Assert.AreNotApproximatelyEqual(a, b, tolerance, msg);
		}
		
		/// <summary>
		/// 値がほぼ等しくない デフォルトの許容誤差は0.00001f
		/// </summary>
		[System.Diagnostics.Conditional( "UNITY_ASSERTIONS" )]
		public static void AreNotApproximatelyEqual(
			float a,
			float b,
			string msg
			)
		{
			AreNotApproximatelyEqual(a, b, msg);
		}
	}		
	
	
} // ATK



#elif  UNITY_3 || UNITY_4 || UNITY_5_0


///UnityのAssertクラスがなかった時バージョン

/*
 * Assertクラス
 * System.Diagnostics.Conditional属性
 * 使う場合は Player Settings > Other Settings > Configuration > Scripting Define Symbols で DEBUG を定義しておく。
 */



using UnityEngine;
using System.Diagnostics;
using System;

namespace ATK
{
	/// <summary>
	/// Assert
	/// </summary>
	public static class Assert
	{
		[System.Diagnostics.Conditional( "DEBUG" )]
		public static void AreEqual<T>(
			T a,
			T b,
			string msg
			)
		{
			if( a.Equals(b) ){
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();
			}
		}
		
		[System.Diagnostics.Conditional( "DEBUG" )]
		public static void AreNotEqual<T>(
			T a,
			T b,
			string msg
			)
		{
			if( !a.Equals(b) ){
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();
			}
		}	
		
		
		[System.Diagnostics.Conditional( "DEBUG" )]
		public static void IsNull(
			object obj,
			string msg
			)
		{
			if( null != obj ){
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();
			}
		}
		
		[System.Diagnostics.Conditional( "DEBUG" )]
		public static void IsNotNull(
			object obj,
			string msg
			)
		{
			if( null == obj ){				
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();
			}
		}
		
		
		[System.Diagnostics.Conditional( "DEBUG" )]
		public static void IsTrue(
			bool condition,
			string msg
			)
		{
			if ( !condition ) {
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();			
			}
		}
		
		[System.Diagnostics.Conditional( "DEBUG" )]
		public static void IsFalse(
			bool condition,
			string msg
			)
		{
			if( condition ){
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();			
			}
		}
		
		
		[System.Diagnostics.Conditional( "DEBUG" )]
		public static void Fail(string msg)
		{		
			UnityEngine.Debug.LogError(msg);
			throw new UnityEngine.UnityException();	
		}
		
		
		[System.Diagnostics.Conditional( "DEBUG" )]
		public static void IsEmpty(
			string str,
			string msg
			)
		{
			if( !string.IsNullOrEmpty(str) ){
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();
			}
		}
		
		[System.Diagnostics.Conditional( "DEBUG" )]
		public static void IsNotEmpty(
			string str,
			string msg
			)
		{
			if( string.IsNullOrEmpty(str) ){
				UnityEngine.Debug.LogError(msg);
				throw new UnityEngine.UnityException();
			}
		}
	}		
	
	
} // ATK



#endif