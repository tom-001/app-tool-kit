﻿using UnityEngine;
using Xml = System.Xml;
using System.IO;
using System.Collections.Generic;

//TODO 画像読み込みにも対応する必要ありそう
//TODO postfixよりもディレクトリ単位で管理すべきか？


namespace ATK.Localization
{
	/// <summary>
	/// 指定された言語のファイル(XML/画像)を詠込むクラス
	/// </summary>	             
	public static class Language
	{

		#region 公開クラス変数
		/// <summary>
		/// 言語ファイルの接尾語の環境
		/// </summary>
		public static LanguagePostfixEnvironment LanguagePostfix = new LanguagePostfixEnvironment();
		#endregion


		#region 公開クラス関数
		
		/// <summary>
		/// 指定された言語ファイル(XML)を読み込み、デシリアライズします。
		/// 見つからなかった場合は、基本言語を読込みます。
		/// </summary>
		/// <typeparam name="T">デシリアライズするクラス</typeparam>
		/// <param name="path">ファイル名を含むパス</param>
		/// <returns>デシリアライズされたクラス</returns>
		public static T LoadXml<T>(string path, SystemLanguage language) where T:class
		{
			T xml = _loadXml<T>(path, LanguagePostfix.GetPosfix(language));
			if( null == xml ){
				xml = _loadXml<T>(path, LanguagePostfix.BasePostfix);
			}
			ATK.Assert.IsNotNull(xml, string.Format("言語ファイル {0}の読み込みに失敗", path));
			return xml;			
		}

		/// <summary>
		/// ファイル名を含むパスに、指定された言語の接尾語をつけます
		/// </summary>
		public static string Name(string path, SystemLanguage language)
		{
			string postfix = LanguagePostfix.GetPosfix(language);
			if ( string.IsNullOrEmpty(postfix) )
				postfix = LanguagePostfix.BasePostfix;
			
			return path + "_" + postfix;
		}

		#endregion
		
		
		
		#region 非公開クラス関数
		
		private static T _loadXml<T>(
			string path,
			string postfix
			) where T: class
		{
			string fullpath = path + "_" + postfix;
			TextAsset text = Resources.Load(fullpath) as TextAsset;									
			if( null == text ){
				Debug.Log(string.Format("no't found {0}", fullpath));
				return null;
			}
			
			Debug.Log(string.Format("Load Localization Xml = {0}", fullpath));
			
			T o = null;			
			using(StreamReader fs = new StreamReader( new MemoryStream(text.bytes)) ){
				Xml.Serialization.XmlSerializer reader = new Xml.Serialization.XmlSerializer(typeof(T));
				o = reader.Deserialize(fs) as T;				
			}
			
			return o;												
		}
		
		#endregion

	}




	
	
	/// <summary>
	/// 言語の接尾辞の設定
	/// </summary>
	public partial class LanguagePostfixEnvironment
	{
		#region 非公開メンバ変数

		/// <summary>
		/// 基本言語
		/// </summary>
		private SystemLanguage _baseLanguage;

		private Dictionary<SystemLanguage, string> _postfixs = new Dictionary<SystemLanguage, string>();

		#endregion
		

		#region プロパティ
		
		public SystemLanguage BaseLanguage { get { return _baseLanguage; } }
		public string BasePostfix { get { return _postfixs[_baseLanguage]; } }

 		#endregion
		
		
		
		#region 公開メンバ関数
		
		// コンストラクタ
		public LanguagePostfixEnvironment()
		{
			_baseLanguage = SystemLanguage.Japanese;
			_postfixs.Add(SystemLanguage.Japanese, "Japanese");
			_postfixs.Add(SystemLanguage.English, "English");

			_setup();
		}

		/// <summary>
		/// 指定した言語の接尾辞を取得する
		/// </summary>
		public string GetPosfix(SystemLanguage language)
		{
			if (_postfixs.ContainsKey(language))
				return _postfixs[language];

			return "";
		}

		#endregion
		
		#region 非公開メンバ関数
		
		/// <summary>
		/// 値を変更
		/// </summary>
		partial void _setup();
		
		#endregion
		
	}


}


/*




==================================================
LanguagePostfixEnvironment _baseLanguage について
==================================================
_baseLanguage には必ずある言語を設定する
言語ファイルが見つからなかった場合、デフォルトを使用する。
_baseLanguage = UnityEngine.Application.systemLanguage を設定しないこと


＜例　postfix が見つからない＞
・用意した言語ファイル
Ln_Config_English
Ln_Config_Japanese
Ln_Msgbox_English

・基本言語の設定
_baseLanguage = SystemLanguage.English

・追加した Postfix
_postfixs.Add(SystemLanguage.English, "English");
_postfixs.Add(SystemLanguage.Japanese, "Japanese");

・言語ファイル取得
ロード ＝ "Ln_Msgbox_" + Postfix.GetPosfix( SystemLanguage.Chinese )
→ postfix(Chinese) は見つからず、ファイル(Ln_Msgbox_) も見つからない
　 この場合、基本エラーになり Ln_Msgbox_English をロードする


＜例　言語ファイルが見つからない＞
・用意した言語ファイル
Ln_Config_English
Ln_Config_Japanese
Ln_Msgbox_English

・基本言語の設定
_baseLanguage = SystemLanguage.English

・追加した Postfix
_postfixs.Add(SystemLanguage.English, "English");
_postfixs.Add(SystemLanguage.Japanese, "Japanese");

・言語ファイル取得
ロード ＝ "Ln_Msgbox_" + Postfix.GetPosfix( SystemLanguage.Japanese )
→ postfix(Japanese) は見つかっても、ファイル(Ln_Msgbox_Japanese) は見つからない
　 この場合、エラーになり Ln_Msgbox_English をロードする


==================================================
言語ファイル（接尾辞）の追加
==================================================
http://docs.unity3d.com/jp/current/ScriptReference/SystemLanguage.html
postfix の名前は SystemLanguage に合わせる



*/