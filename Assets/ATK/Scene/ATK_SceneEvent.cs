﻿
using UnityEngine;

namespace ATK.Scene
{
	/// <summary>
	/// フェードイン開始時に呼ばれるコールバック
	/// 引数なしのデリゲート
	/// </summary>
	public delegate void OnStartFadeInDelegate();
	
	/// <summary>
	/// フェードアウト開始時に呼ばれるコールバック
	/// 引数なしのデリゲート
	/// </summary>
	public delegate void OnStartFadeOutDelegate();
	
	/// <summary>
	/// フェードイン終了時に呼ばれるコールバック
	/// 引数なしのデリゲート
	/// </summary>
	public delegate void OnFinishedFadeInDelegate();
	
	/// <summary>
	/// フェードアウト終了時に呼ばれるコールバック
	/// 引数なしのデリゲート
	/// </summary>
	public delegate void OnFinishedFadeOutDelegate();

	/// <summary>
	/// シーン廃棄時に呼ばれるコールバック
	/// </summary>
	public delegate void OnDisposeedDelegate();
}