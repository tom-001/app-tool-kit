﻿
using UnityEngine;

namespace ATK.Scene
{
	public interface ATK_TransitionInterface
	{

        #region インターフェース

        /// <summary>
        /// フェードイン(だんだん明るくなる)開始時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        event OnStartFadeInDelegate OnStartFadeInEvent;

        /// <summary>
        /// フェードイン(だんだん明るくなる)終了時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        event OnFinishedFadeInDelegate OnFinishedFadeInEvent;

        /// <summary>
        /// フェードアウト(だんだん暗くなる)開始時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        event OnStartFadeOutDelegate OnStartFadeOutEvent;

        /// <summary>
        /// フェードアウト(だんだん暗くなる)終了時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        event OnFinishedFadeOutDelegate OnFinishedFadeOutEvent;

 
		/// <summary>
		/// 親（Canvas）を設定します
		/// </summary>
		/// <param name="canvas">Canvas.</param>
		void SetParentCanvas ( Canvas canvas );

		/// <summary>
		/// フェードカラー設定
		/// </summary>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="b"></param>
		void SetFadeColor ( float r, float g, float b );
		void SetFadeColor ( Color color );

		/// <summary>
		/// フェード画像の設定
		/// </summary>
		void SetFadeImage ( Texture texture );
		void SetFadeImage ( Sprite sprite );

		/// <summary>
		/// フェードイン開始
		/// </summary>
		void StartFadeIn(
                float time,
                OnStartFadeInDelegate onStartFadeInDelegate,
                OnFinishedFadeInDelegate onFinishedFadeInDelegate,
                OnStartFadeOutDelegate onStartFadeOutDelegate,
                OnFinishedFadeOutDelegate onFinishedFadeOutDelegate);
        void StartFadeIn(
                OnStartFadeInDelegate onStartFadeInDelegate,
                OnFinishedFadeInDelegate onFinishedFadeInDelegate,
                OnStartFadeOutDelegate onStartFadeOutDelegate,
                OnFinishedFadeOutDelegate onFinishedFadeOutDelegate);
        void StartFadeIn(float time);
        void StartFadeIn();

        /// <summary>
        /// フェードアウト開始
        /// </summary>
		void StartFadeOut(
            float time,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate);
        void StartFadeOut(
                OnStartFadeInDelegate onStartFadeInDelegate,
                OnFinishedFadeInDelegate onFinishedFadeInDelegate,
                OnStartFadeOutDelegate onStartFadeOutDelegate,
                OnFinishedFadeOutDelegate onFinishedFadeOutDelegate);
        void StartFadeOut(float time);
		void StartFadeOut();

		/// <summary>
		/// フェード中かどうか
		/// </summary>
		bool NowFading ();

		/// <summary>
		/// 表示するかしないか
		/// </summary>
		void SetActive( bool value );

		#endregion

	}

}