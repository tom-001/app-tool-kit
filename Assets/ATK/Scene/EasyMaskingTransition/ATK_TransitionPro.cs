﻿/*

トランジション　有料アセット版
　Unityエディタ上でゲームの雰囲気に合わせてカスタマイズする
　Gradation Texture や Transition Curve を別々に設定すると面白い
　SetColorもいじる
　必ずSetParentCanvasで親Canvasを設定する設計

・使用アセット　※有料
Easy Masking Transition

・プレファブ
ATK/Resources/Prefab/ATK_TransitionPro

・プレファブ構成
ATK_TransitionPro
├ Easy Masking Transition 1
└ Easy Masking Transition 2

・入力について
　RawImageを使用しているので、CanvasのSort Order下の入力は無視されます
　入力を有効にしたい場合は、Canvas Groupで

*/


//#define USING_INVERSE_SETCOLOR

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;


namespace ATK.Scene
{

	/// <summary>
	/// シーンマネージャ
	/// </summary>
	public class ATK_TransitionPro : CacheComponentMonoBehaviour, ATK_TransitionInterface
	{
        #region インターフェース

        /// <summary>
        /// フェードイン(だんだん明るくなる)開始時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnStartFadeInDelegate OnStartFadeInEvent;

        /// <summary>
        /// フェードイン(だんだん明るくなる)終了時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnFinishedFadeInDelegate OnFinishedFadeInEvent;

        /// <summary>
        /// フェードアウト(だんだん暗くなる)開始時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnStartFadeOutDelegate OnStartFadeOutEvent;

        /// <summary>
        /// フェードアウト(だんだん暗くなる)終了時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnFinishedFadeOutDelegate OnFinishedFadeOutEvent;

        /// <summary>
        /// 親（Canvas）を設定します
        /// </summary>
        /// <param name="canvas">Canvas.</param>
        public void SetParentCanvas( Canvas canvas )
		{
			cachedTransform.parent = canvas.transform;
			_setup();
			_initialized = true;
		}



		/// <summary>
		/// フェードカラー設定
		/// </summary>
		public void SetFadeColor(
			float r,
			float g,
			float b
			)
		{

#if USING_INVERSE_SETCOLOR
			// 反転カラー
			bool inverse = false;
			foreach( EMTransitionCustom t in _transitions ) {
				if (inverse) {
					t.GetComponent<RawImage>().color = new Color(
							Mathf.Clamp01(1f-r),
							Mathf.Clamp01(1f-g),
							Mathf.Clamp01(1f-b)
						);
				}else {
					t.GetComponent<RawImage>().color = new Color(r, g, b);
				}
				inverse = !inverse;
			}
#else
			// 半減カラー
			float scale = 1.0f;
			foreach( EMTransitionCustom t in _transitions ) {
				t.GetComponent<RawImage>().color = new Color(r*scale, g*scale, b*scale);
				scale *= 0.5f;
			}
#endif
		}

		/// <summary>
		/// フェードカラー設定。α値は無視されます
		/// </summary>
		public void SetFadeColor( Color color )
		{
			SetFadeColor(color.r, color.g, color.b);
		}

		/// <summary>
		/// フェード画像の設定
		/// </summary>
		public void SetFadeImage( Texture texture )
		{
			foreach( EMTransitionCustom t in _transitions ) {
				t.GetComponent<RawImage>().texture = texture;
			}
		}
		public void SetFadeImage( Sprite sprite )
		{
			foreach( EMTransitionCustom t in _transitions ) {
				t.GetComponent<RawImage>().texture = sprite.texture;
			}
		}

		
		/// <summary>
		/// フェードイン開始
		/// </summary>
		public void StartFadeIn(
            float time,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
		{
			Assert.IsNotNull(cachedTransform.parent, "親（Canvas）が設定されていません。");

			foreach( EMTransitionCustom t in _transitions ) {
				t.duration = time;
				t.flip = true;
				t.Play();
				t.onTransitionComplete.AddListener(_onComplete);
			}
			_completeCounter = 0;
			_fadeState = FadeState.InStart;

            if (!object.ReferenceEquals(onStartFadeInDelegate, null)) this.OnStartFadeInEvent += onStartFadeInDelegate;
            if (!object.ReferenceEquals(onFinishedFadeInDelegate, null)) this.OnFinishedFadeInEvent += onFinishedFadeInDelegate;
            if (!object.ReferenceEquals(onStartFadeOutDelegate, null)) this.OnStartFadeOutEvent += onStartFadeOutDelegate;
            if (!object.ReferenceEquals(onFinishedFadeOutDelegate, null)) this.OnFinishedFadeOutEvent += onFinishedFadeOutDelegate;
        }

        public void StartFadeIn(
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            StartFadeIn(DefaultFadeTime, onStartFadeInDelegate, onFinishedFadeInDelegate, onStartFadeOutDelegate, onFinishedFadeOutDelegate);
        }

        public void StartFadeIn(float time)
        {
            StartFadeIn(time, null, null, null, null);
        }

        public void StartFadeIn()
        {
            StartFadeIn(DefaultFadeTime, null, null, null, null);
        }



        /// <summary>
        /// フェードアウト開始
        /// </summary>
        public void StartFadeOut(
            float time,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
			Assert.IsNotNull(cachedTransform.parent, "親（Canvas）が設定されていません。");

			foreach( EMTransitionCustom t in _transitions ) {
				t.duration = time;
				t.flip = false;
				t.Play();
				t.onTransitionComplete.AddListener(_onComplete);
			}
			_completeCounter = 0;
			_fadeState = FadeState.OutStart;

            if (!object.ReferenceEquals(onStartFadeInDelegate, null)) this.OnStartFadeInEvent += onStartFadeInDelegate;
            if (!object.ReferenceEquals(onFinishedFadeInDelegate, null)) this.OnFinishedFadeInEvent += onFinishedFadeInDelegate;
            if (!object.ReferenceEquals(onStartFadeOutDelegate, null)) this.OnStartFadeOutEvent += onStartFadeOutDelegate;
            if (!object.ReferenceEquals(onFinishedFadeOutDelegate, null)) this.OnFinishedFadeOutEvent += onFinishedFadeOutDelegate;
        }

        public void StartFadeOut(
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            StartFadeOut(DefaultFadeTime, onStartFadeInDelegate, onFinishedFadeInDelegate, onStartFadeOutDelegate, onFinishedFadeOutDelegate);
        }

        public void StartFadeOut(float time)
        {
            StartFadeOut(time, null, null, null, null);
        }

        public void StartFadeOut()
        {
            StartFadeOut(DefaultFadeTime, null, null, null, null);
        }



        /// <summary>
        /// フェード中かどうか
        /// </summary>
        public bool NowFading()
		{
			return FadeState.Idle != _fadeState;
		}


		/// <summary>
		/// 表示するかしないか
		/// </summary>
		public void SetActive( bool value )
		{
			foreach( EMTransitionCustom t in _transitions ) {
				t.gameObject.SetActive(value);
			}
		}
		
		#endregion





	
		#region 定数
		
		/// <summary>
		/// フェード状態
		/// </summary>
		private enum FadeState
		{
			Idle,
			
			InStart,
			In,
			
			OutStart,
			Out,
		}
		
		#endregion
		
		
		
		#region 非公開メンバ変数

//		private RawImage _fadeImage = null;
		
		private FadeState _fadeState = FadeState.Idle;
//		private float _fadeEndTime = 0.0f;
//		private float _fadeCurrentTime = 0.0f;
//		private Color _fadeColor = Color.clear;
		
		private bool _initialized = false;
		private int _completeCounter;

		
		#endregion
		
		
		
		#region 公開クラス変数

		/// <summary>
		/// トランジション
		/// </summary>
		public List<EMTransitionCustom> _transitions;
		
		/// <summary>
		/// デフォルトフェード時間
		/// </summary>
		public static float DefaultFadeTime = 0.25f;
		
		#endregion


		
		#region 公開メンバ関数
		
		void Awake() 
		{
			_setup();
			//_fadeImage = GetComponent<RawImage>();
			//_fadeImage.enabled = false;
		}
		
		void LateUpdate()
		{
			if( !_initialized ){
				return;
			}
			_updateFade();
		}

		#endregion
		
		
		
		
		#region 非公開メンバ関数
		
		/// <summary>
		/// RectTransformを縦横ストレッチに設定する
		/// </summary>
		private void _setup()
		{
			cachedRectTransform.anchorMin = Vector2.zero;
			cachedRectTransform.anchorMax = Vector2.one;
			cachedRectTransform.pivot.Set(0.5f,0.5f);
			cachedRectTransform.localPosition = Vector3.zero;
			cachedRectTransform.localScale = Vector3.one;
			cachedRectTransform.sizeDelta = Vector2.zero;
		}
		
		/// <summary>
		/// フェード更新
		/// </summary>
		private void _updateFade()
		{
			switch (_fadeState) {
				
			case FadeState.InStart:
				_fadeState = FadeState.In;
                if (!object.ReferenceEquals(this.OnStartFadeInEvent, null)) {
					this.OnStartFadeInEvent();
					this.OnStartFadeInEvent = null;
				}
				break;
				
			case FadeState.In:
				if ( _transitions.Count == _completeCounter ) {
					_fadeState = FadeState.Idle;
					_clearOnCompleteListener();
                    if (!object.ReferenceEquals(this.OnFinishedFadeInEvent, null)) {
						this.OnFinishedFadeInEvent();
						this.OnFinishedFadeInEvent = null;
					}
				}
				break;
				
			case FadeState.OutStart:
				_fadeState = FadeState.Out;
                if (!object.ReferenceEquals(this.OnStartFadeOutEvent, null)) {
					this.OnStartFadeOutEvent();
					this.OnStartFadeOutEvent = null;
				}
				break;
				
			case FadeState.Out:
				if ( _transitions.Count == _completeCounter ) {
					_fadeState = FadeState.Idle;
					_clearOnCompleteListener();
                    if (!object.ReferenceEquals(this.OnFinishedFadeOutEvent, null)) {
						this.OnFinishedFadeOutEvent();
						this.OnFinishedFadeOutEvent = null;
					}

				}
				break;
			}
		}




		private void _onComplete()
		{
			_completeCounter++;
		}

		private void _clearOnCompleteListener()
		{
			foreach( EMTransitionCustom t in _transitions ) {
				t.onTransitionComplete.RemoveAllListeners();
			}
		}


		
		#endregion
		
	}

}