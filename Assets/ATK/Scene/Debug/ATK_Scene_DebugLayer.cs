﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace ATK
{

public class ATK_Scene_DebugLayer : MonoBehaviour {

#region 非公開メンバ変数

	[SerializeField]	
	private Text _fps = null;
	
	[SerializeField]
	private GameObject _ui = null;
				
	private Dictionary<Camera,LayerMask> _cameraList = null;
	private List<GameObject> _menuStack = new List<GameObject>();
			
	private bool _isPressed = false;
	private float _pressTimer = 0.0f;	
	private ATK.Scene.Debug.Factory _menuFactory = new ATK.Scene.Debug.Factory();

#endregion

#region プロパティ

	/// <summary>
	/// デバッグレイヤーが表示されているか
	/// </summary>
	public bool IsVisible
	{
		get
		{
			if( !UnityEngine.Debug.isDebugBuild ){
				return false;
			}
			
			if( _fps.enabled || _ui.activeSelf ){
				return true;
			}
																
			return false;
		}
	}
	
	public bool IsVisibleFPS
	{
		get { return _fps.enabled; }		
	}
	public void SetVisibleFPS(bool isVisible)
	{
		_fps.enabled = isVisible;
	}
	
	
	/// <summary>
	/// メニューファクトリ
	/// </summary>
	public ATK.Scene.Debug.Factory MenuFactory
	{
		get
		{
			return _menuFactory;
		}
	}
	
#endregion

#region 公開メンバ関数

	
	/// <summary>
	/// メニュー追加
	/// </summary>
	/// <param name="menu"></param>
	public void CallMenu(GameObject menu)
	{
		Assert.IsNotNull(menu,"null==menu");	
		
		if( 0 != _menuStack.Count ){
			_menuStack[_menuStack.Count-1].SetActive(false);
		}
		
		menu.transform.parent = this.transform;
		menu.transform.localPosition = new Vector3(0.0f,0.0f,-100.0f);
		menu.transform.localScale = Vector3.one;
		menu.transform.localRotation = Quaternion.identity;
		_menuStack.Add(menu);				
	}
	

	// Use this for initialization
	void Start () {		
		_ui.gameObject.SetActive(false);
		_fps.enabled = false;
	}
			
	// Update is called once per frame
	void Update () {
	
			if( UnityEngine.Debug.isDebugBuild ){
		
			if( _isPressed ){
				_pressTimer += Time.deltaTime;
			}
			
				
			if( !_ui.activeSelf ){		

				if( 2.0f <= _pressTimer ){
					_showDebugMenu();						
					_isPressed = false;
				}
								
			}else{			
				_isPressed = false;
				_pressTimer = 0.0f;			
			}		
		}
	}

#endregion

#region 非公開メンバ関数

	private void _showDebugMenu()
	{
		_ui.SetActive(true);
		Debug.Log("ShowDebugMenu");
		
		Object[] objects = GameObject.FindObjectsOfType(typeof(Camera));
		_cameraList = new Dictionary<Camera,LayerMask>();
		
		foreach(Object obj in objects){		
			
			Camera camera = obj as Camera;			
			// カメラを非表示に
			_cameraList.Add(camera,camera.cullingMask);
			camera.cullingMask = 0;
		}
		/*asdf
		// シーンマネージャーのカメラは有効
		GameObject sceneMgr = GameObject.Find("SceneManager") as GameObject;
		Camera cam = ATK.Utility.FindChild(sceneMgr.transform,"Camera").GetComponent<Camera>();
		cam.cullingMask = 1<<0;
		
		if( 0 == _menuStack.Count ){
			CallMenu( this.MenuFactory.CreateRootMenu() );	
		}else{		
			for(int i = 0; i < _menuStack.Count; ++i){
				_menuStack[i].SetActive(i == (_menuStack.Count-1));
			}
		}	*/							
	}

	private void _onRelease(GameObject sender)
	{
		_isPressed = false;
	}
	

	private void _onPress(GameObject sender)
	{
		_pressTimer = 0.0f;
		_isPressed = true;		
		//Debug.Log("Press");		
	}
		
	private void _onClose(GameObject sender)
	{		
		_ui.gameObject.SetActive(false);		
		
		foreach(var p in _cameraList){
			p.Key.cullingMask = p.Value;
		}
		_cameraList = null;
		
		for(int i = 0; i < _menuStack.Count; ++i){
			_menuStack[i].SetActive(false);			
		}
	}
	
	private void _onReturn(GameObject sender)
	{
		// 念のため
		if( 0 == _menuStack.Count ){
			_onClose(sender);
			return;
		}		
		
		int last = _menuStack.Count - 1;
		GameObject.Destroy(_menuStack[last]);
		_menuStack.RemoveAt(last);
		
		if( 0 == _menuStack.Count ){
			// 終了
			_onClose(sender);
		}else{
			_menuStack[last-1].SetActive(true);		
		}				
	}
		

#endregion
	
	
}
}