﻿/*

FPS 表示
uGgUIのTextがあるオブジェクトに追加する

 */
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace ATK
{

public class ATK_Scene_Debug_FPS : MonoBehaviour {

#region 非公開メンバ変数

	private Text _text = null;
	private int _frame = 0;
	private float _time = 0.0f;

#endregion

	// Use this for initialization
	void Start () 
	{
		_text = GetComponent<Text>();
		_time = Time.realtimeSinceStartup;
		_frame = 0;		
	}
			
	void Update () 
	{
		_frame++;
		float current = Time.realtimeSinceStartup;
		float time = current - _time;
		
		if( 1.0f <= time ){						
			float fps = _frame / time;			
			_text.text = string.Format("FPS {0:F2}",fps);
			_time = current;
			_frame = 0;
		}		
	}
	
}
}