﻿using UnityEngine;
using System.Collections.Generic;
using ATK;

namespace ATK.Scene.Debug
{
	/// <summary>
	/// メニューファクトリー	
	/// </summary>
	public class Factory
	{
	#region 非公開メンバ変数
		
		private Dictionary<string, GameObject> _dictionary = new Dictionary<string, GameObject>();
	
	#endregion
	
	#region 公開メンバ変数
	
		/// <summary>
		/// ルートメニュー名
		/// 必要なら変更すること
		/// </summary>
		public string RootMenuName = "Root";
		
	#endregion
	
	#region 公開メンバ関数
	
		/// <summary>
		/// メニュープレハブの追加
		/// </summary>
		/// <param name="className"></param>
		/// <param name="prefab"></param>
		public void AddMenu(
			string className,
			GameObject prefab
			)
		{
			Assert.IsNotEmpty(className,"className==empty");
			Assert.IsNotNull(prefab,"prefab==null");
			Assert.IsFalse(_dictionary.ContainsKey(className),"ContainsKey(className)");			
			_dictionary[className] = prefab;
		}
	
		public GameObject CreateRootMenu()
		{
			return CreateMenu(RootMenuName);
		}
		
		public GameObject CreateMenu(string className)
		{
			return GameObject.Instantiate(_dictionary[className]) as GameObject;
		}
	#endregion
		
	}
}