﻿/*

トランジション
　必ずSetParentCanvasで親Canvasを設定する設計

・プレファブ
ATK/Resources/Prefab/ATK_Transition

・プレファブ構成
ATK_TransitionPro
├ Easy Masking Transition 1
└ Easy Masking Transition 2

・入力について
　RawImageを使用しているので、CanvasのSort Order下の入力は無視されます
　入力を有効にしたい場合は、Canvas Groupで

*/

using UnityEngine;
using UnityEngine.UI;
using ATK;



namespace ATK.Scene
{
	
	
	
	/// <summary>
	/// シーンマネージャ
	/// </summary>
	public class ATK_Transition : CacheComponentMonoBehaviour, ATK_TransitionInterface
	{
        #region インターフェース

        /// <summary>
        /// フェードイン(だんだん明るくなる)開始時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnStartFadeInDelegate OnStartFadeInEvent;

        /// <summary>
        /// フェードイン(だんだん明るくなる)終了時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnFinishedFadeInDelegate OnFinishedFadeInEvent;

        /// <summary>
        /// フェードアウト(だんだん暗くなる)開始時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnStartFadeOutDelegate OnStartFadeOutEvent;

        /// <summary>
        /// フェードアウト(だんだん暗くなる)終了時に呼ばれるコールバック
        /// 引数なしのデリゲート
        /// </summary>
        public event OnFinishedFadeOutDelegate OnFinishedFadeOutEvent;

        /// <summary>
        /// 親（Canvas）を設定します
        /// </summary>
        /// <param name="canvas">Canvas.</param>
        public void SetParentCanvas( Canvas canvas )
		{
			cachedTransform.parent = canvas.transform;
			_setup();
			_initialized = true;
		}
		
		
		
		/// <summary>
		/// フェードカラー設定
		/// </summary>
		public void SetFadeColor(
			float r,
			float g,
			float b
			)
		{
			_fadeColor.r = r;
			_fadeColor.g = g;
			_fadeColor.b = b;
			_fadeImage.color = _fadeColor;
		}
		
		/// <summary>
		/// フェードカラー設定。α値は無視されます
		/// </summary>
		public void SetFadeColor( Color color )
		{
			SetFadeColor(color.r, color.g, color.b);
		}
		
		/// <summary>
		/// フェード画像の設定
		/// </summary>
		public void SetFadeImage( Texture texture )
		{
			_fadeImage.texture = texture;
		}
		public void SetFadeImage( Sprite sprite )
		{
			_fadeImage.texture = sprite.texture;
		}		
		
		
		/// <summary>
		/// フェードイン開始
		/// </summary>
		public void StartFadeIn(
            float time,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
			Assert.IsNotNull(cachedTransform.parent, "親（Canvas）が設定されていません。");
			
			_fadeEndTime = time;
			_fadeCurrentTime = 0.0f;
			if (0.01f >= _fadeEndTime) {
				_fadeEndTime = 0.01f;
			}
			_fadeState = FadeState.InStart;

            if (!object.ReferenceEquals(onStartFadeInDelegate, null)) this.OnStartFadeInEvent += onStartFadeInDelegate;
            if (!object.ReferenceEquals(onFinishedFadeInDelegate, null)) this.OnFinishedFadeInEvent += onFinishedFadeInDelegate;
            if (!object.ReferenceEquals(onStartFadeOutDelegate, null)) this.OnStartFadeOutEvent += onStartFadeOutDelegate;
            if (!object.ReferenceEquals(onFinishedFadeOutDelegate, null)) this.OnFinishedFadeOutEvent += onFinishedFadeOutDelegate;
        }

        public void StartFadeIn(
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            StartFadeIn(DefaultFadeTime, onStartFadeInDelegate, onFinishedFadeInDelegate, onStartFadeOutDelegate, onFinishedFadeOutDelegate);
        }

        public void StartFadeIn(float time)
        {
            StartFadeIn(time, null, null, null, null);
        }

        public void StartFadeIn()
        {
            StartFadeIn(DefaultFadeTime, null, null, null, null);
        }



        /// <summary>
        /// フェードアウト開始
        /// </summary>
        public void StartFadeOut(
            float time,
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            Assert.IsNotNull(cachedTransform.parent, "親（Canvas）が設定されていません。");

            _fadeEndTime = time;
            _fadeCurrentTime = 0.0f;
            if (0.01f >= _fadeEndTime)
            {
                _fadeEndTime = 0.01f;
            }
            _fadeState = FadeState.OutStart;

            if (!object.ReferenceEquals(onStartFadeInDelegate, null)) this.OnStartFadeInEvent += onStartFadeInDelegate;
            if (!object.ReferenceEquals(onFinishedFadeInDelegate, null)) this.OnFinishedFadeInEvent += onFinishedFadeInDelegate;
            if (!object.ReferenceEquals(onStartFadeOutDelegate, null)) this.OnStartFadeOutEvent += onStartFadeOutDelegate;
            if (!object.ReferenceEquals(onFinishedFadeOutDelegate, null)) this.OnFinishedFadeOutEvent += onFinishedFadeOutDelegate;
        }

        public void StartFadeOut(
            OnStartFadeInDelegate onStartFadeInDelegate,
            OnFinishedFadeInDelegate onFinishedFadeInDelegate,
            OnStartFadeOutDelegate onStartFadeOutDelegate,
            OnFinishedFadeOutDelegate onFinishedFadeOutDelegate
            )
        {
            StartFadeOut(DefaultFadeTime, onStartFadeInDelegate, onFinishedFadeInDelegate, onStartFadeOutDelegate, onFinishedFadeOutDelegate);
        }

        public void StartFadeOut(float time)
        {
            StartFadeOut(time, null, null, null, null);
        }

        public void StartFadeOut()
        {
            StartFadeOut(DefaultFadeTime, null, null, null, null);
        }


        /// <summary>
        /// フェード中かどうか
        /// </summary>
        public bool NowFading()
		{
			return FadeState.Idle != _fadeState;
		}
		
		
		/// <summary>
		/// 表示するかしないか
		/// </summary>
		public void SetActive( bool value )
		{
			this.gameObject.SetActive(value);
		}
		
		#endregion
		
		
		
		
		
		
		#region 定数
		
		/// <summary>
		/// フェード状態
		/// </summary>
		private enum FadeState
		{
			Idle,
			
			InStart,
			In,
			
			OutStart,
			Out,
		}
		
		#endregion
		
		
		
		#region 非公開メンバ変数
		
		private RawImage _fadeImage = null;
		
		private FadeState _fadeState = FadeState.Idle;
		private float _fadeEndTime = 0.0f;
		private float _fadeCurrentTime = 0.0f;
		private Color _fadeColor = Color.clear;
		
		private bool _initialized = false;
		
		#endregion
		
		
		
		#region 公開クラス変数
		
		/// <summary>
		/// デフォルトフェード時間
		/// </summary>
		public static float DefaultFadeTime = 0.25f;
		
		#endregion
		
		
		
		#region 公開メンバ関数
		
		void Awake() 
		{
			_setup();
			_fadeImage = GetComponent<RawImage>();
			//_fadeImage.enabled = false;
		}
		
		void LateUpdate()
		{
			if( !_initialized ){
				return;
			}
			_updateFade();
		}
		
		#endregion
		
		
		
		
		#region 非公開メンバ関数
		
		/// <summary>
		/// RectTransformを縦横ストレッチに設定する
		/// </summary>
		private void _setup()
		{
			cachedRectTransform.anchorMin = Vector2.zero;
			cachedRectTransform.anchorMax = Vector2.one;
			cachedRectTransform.pivot.Set(0.5f,0.5f);
			cachedRectTransform.localPosition = Vector3.zero;
			cachedRectTransform.localScale = Vector3.one;
			cachedRectTransform.sizeDelta = Vector2.zero;
		}
		
		/// <summary>
		/// フェード更新
		/// </summary>
		private void _updateFade()
		{
			switch (_fadeState) {
				
			case FadeState.InStart:
				_updateFadeAlpha(_fadeEndTime - _fadeCurrentTime);
				_fadeState = FadeState.In;
                if (!object.ReferenceEquals(this.OnStartFadeInEvent, null)) {
					this.OnStartFadeInEvent();
					this.OnStartFadeInEvent = null;
				}
				break;
				
			case FadeState.In:
				_fadeCurrentTime += Time.deltaTime;
				if (_fadeCurrentTime >= _fadeEndTime) {
					_fadeCurrentTime = _fadeEndTime;
				}
				
				_updateFadeAlpha(_fadeEndTime - _fadeCurrentTime);
				if (_fadeCurrentTime >= _fadeEndTime) {
					_fadeState = FadeState.Idle;
                    if (!object.ReferenceEquals(this.OnFinishedFadeInEvent, null)) {
						this.OnFinishedFadeInEvent();
						this.OnFinishedFadeInEvent = null;
					}
				}
				break;
				
			case FadeState.OutStart:
				_updateFadeAlpha(_fadeCurrentTime);
				_fadeState = FadeState.Out;
                if (!object.ReferenceEquals(this.OnStartFadeOutEvent, null)) {
					this.OnStartFadeOutEvent();
					this.OnStartFadeOutEvent = null;
				}
				break;
				
			case FadeState.Out:
				_fadeCurrentTime += Time.deltaTime;
				if (_fadeCurrentTime >= _fadeEndTime) {
					_fadeCurrentTime = _fadeEndTime;
				}
				_updateFadeAlpha(_fadeCurrentTime);
				if (_fadeCurrentTime >= _fadeEndTime) {
					_fadeState = FadeState.Idle;
                    if (!object.ReferenceEquals(this.OnFinishedFadeOutEvent, null)) {
						this.OnFinishedFadeOutEvent();
						this.OnFinishedFadeOutEvent = null;
					}
				}
				break;
			}
		}
		
		
		/// <summary>
		/// アルファ値更新
		/// </summary>
		private void _updateFadeAlpha(float time)
		{
			float a = time / _fadeEndTime;
			_fadeColor.a = a;
			SetFadeColor(_fadeColor.r, _fadeColor.g, _fadeColor.b);
		}
		
		#endregion
		
	}
	
}