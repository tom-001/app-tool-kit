﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using ATK;
using ATK.Scene;

#if false
namespace ATK.Scene
{
	/// <summary>
	/// トースト
	/// </summary>
	public class Toast
	{
	#region デフォルト値
	
		public static Color DefaultColor = Color.white;	
		public static Vector3 DefaultScale = Vector3.one;
		public static Vector3 DefaultPositon = Vector3.zero;
		public static float DefaultTime = 0.5f;
		public static int DefaultMaxLineCount = 4;
		public static bool DefaultMultiLine = true;
		public static bool DefaultEncoding	= true;
		public static UILabel.Effect DefaultEffect = UILabel.Effect.None;
		private Text.eff
		
		// とりあえず左上のみ
		//public static UILabel.Pivot DefaultPivot = UIWidget.Pivot.Center;
		
		public static Color DefaultEffectColor = Color.clear;
		public static Vector2 DefaultEffectDistance = Vector2.one;
		public static UIFont.SymbolStyle DefaultSymbolStyle = UIFont.SymbolStyle.None;
		
		public static bool DefaultIsFixedWindowSize = false;
		public static Vector4 DefaultWindowSize = new Vector4(-16.0f,-16.0f,16.0f,16.0f);

							
	#endregion
	
	#region 公開メンバ変数
	
		/// <summary>
		/// 表示するメッセージ
		/// </summary>
		public string Message = "Toast";
		
		/// <summary>
		/// メッセージ色
		/// </summary>
		public Color Color = DefaultColor;
		
		/// <summary>
		/// 表示する時間
		/// </summary>
		public float Time = DefaultTime;
		
		/// <summary>
		/// スケール
		/// </summary>
		public Vector3 Scale = DefaultScale;
		
		/// <summary>
		/// 表示位置
		/// </summary>
		public Vector3 Position = DefaultPositon;
		
		/// <summary>
		/// 固定ウィンドウサイズ
		/// </summary>
		public bool IsFixedWindowSize = DefaultIsFixedWindowSize;
		
		/// <summary>
		/// ウィンドウの余白 or ウィンドウサイズ
		/// 「固定」
		/// -x,-y,w,h
		/// 「相対」
		/// ラベルの表示領域から-w,-h,+w,+h
		/// した値がウィンドウの位置と大きさになる
		/// </summary>
		public Vector4 WindowSize = DefaultWindowSize;
		
		
		// UILabelの値
		
		public int MaxLineCount					= DefaultMaxLineCount;
		public bool MultiLine					= DefaultMultiLine;
		public bool Encoding					= DefaultEncoding;
		public UILabel.Effect Effect			= DefaultEffect;
		
		// とりあえず左上のみ
		//public UILabel.Pivot Pivot				= DefaultPivot;
		
		public Color EffectColor				= DefaultEffectColor;
		public Vector2 EffectDistance			= DefaultEffectDistance;
		public UIFont.SymbolStyle SymbolStyle	= DefaultSymbolStyle;
		
				
		
		/// <summary>
		/// ラベル特別
		/// </summary>
		public GameObject LabelPrefab = null;
	#endregion
	
	}

}


public class ATK_Scene_ToastLayer : MonoBehaviour {


#region 非公開メンバ変数

	private UIAtlas _atlas = null;
	private UIFont _font = null;
	private List<Toast> _queue = new List<Toast>();
	private float _time = 0.0f;
	
	enum State
	{
		Show,
		Disp,
		Hide,
		Idle,
	}
	private State _state = State.Idle;
	
	
#endregion

#region 定数

	const float EaseTime = 0.5f;

#endregion

#region 公開メンバ関数

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="atlas"></param>
	/// <param name="font"></param>
	public void Initialize(
		UIAtlas atlas,
		UIFont font
		)
	{
		Assert.IsNotNull(atlas,"null==atlas");
		Assert.IsNotNull(font,"null==font");
		_atlas = atlas;
		_font = font;
		
		// 表示位置
		Toast.DefaultPositon.x = RuntimeEnvironment.Display.Width*-0.5f;
		Toast.DefaultPositon.y = RuntimeEnvironment.Display.Height*0.5f;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		switch( _state ){
		case State.Idle:
			if( 0 == _queue.Count ){
				// メッセージが追加されるまで無効に
				this.gameObject.SetActive(false);
			}else{
				_time = 0.0f;
				_state = State.Show;
				_showToast();
			}
			break;
			
		case State.Show:
			_time += Time.deltaTime;
			if( EaseTime <= _time ){
				_time = 0.0f;
				_state = State.Disp;
			}		
			break;

		case State.Disp:
			_time += Time.deltaTime;
			if( _queue[0].Time <= _time ){
				_time = 0.0f;
				_state = State.Hide;
				_hideToast();
			}		
			break;
			
		case State.Hide:
			_time += Time.deltaTime;
			if( EaseTime <= _time ){
				_time = 0.0f;
				_state = State.Idle;
				_queue.RemoveAt(0);				
			}					
			break;						
		}		
			
	}
	
	/// <summary>
	/// メッセージを追加
	/// </summary>
	/// <param name="msg"></param>
	public void Add(Toast toast)
	{
		Assert.IsNotNull(toast,"null==toast");
		this.gameObject.SetActive(true);		
		_queue.Add(toast);
	}
	

#endregion


#region 非公開メンバ関数

	/// <summary>
	/// トースト作成
	/// </summary>
	private void _showToast()
	{
		{
			Transform t = Utility.FindChild(this.transform,"Toast");
			if( null != t ){
				GameObject.Destroy(t.gameObject);
			}
		}
		
		Toast cur = _queue[0];
		
		GameObject parent = new GameObject();
		parent.name = "Toast";
		parent.transform.parent = this.transform;
		parent.transform.localScale = Vector3.one;
		parent.transform.localPosition = cur.Position;
		
		
		UILabel label = null;
		
		// ラベル
		{
			GameObject obj = null;			
			
			if( null != cur.LabelPrefab ){
				obj = Instantiate(cur.LabelPrefab) as GameObject;
				label = obj.GetComponent<UILabel>();
			}else{
				obj = new GameObject();
				label = obj.AddComponent<UILabel>();				
				label.font = _font;
				
				label.maxLineCount = cur.MaxLineCount;
				label.multiLine = cur.MultiLine;
				label.supportEncoding = cur.Encoding;
				label.effectStyle = cur.Effect;
				label.pivot = UIWidget.Pivot.TopLeft;
				label.effectColor = cur.EffectColor;
				label.effectDistance = cur.EffectDistance;
				label.symbolStyle = cur.SymbolStyle;
																								
				label.depth = 1;				
			}
			Assert.IsNotNull(obj,"null==obj");
			Assert.IsNotNull(label,"null==label");
									
			obj.name = "Labl";
			obj.transform.parent = parent.transform;
			obj.transform.localScale = cur.Scale;
			obj.transform.localPosition = new Vector3(
				-cur.WindowSize.x,
				+cur.WindowSize.y,
				0.0f
				);				
				
									
			label.text = cur.Message;
			label.color = cur.Color;						
			label.MakePixelPerfect();												
		}
		
		// ウィンドウ
		{
			GameObject obj = new GameObject();
			obj.name = "Window";
			obj.transform.parent = parent.transform;
			
			Vector2 relativeSize = label.relativeSize;
			Vector3 textScale = label.gameObject.transform.localScale;
			textScale.x *= relativeSize.x;
			textScale.y *= relativeSize.y;
			Vector2 wndSize;
			
			if( cur.IsFixedWindowSize ){
				wndSize.x = cur.WindowSize.z;
				wndSize.y = cur.WindowSize.w;
				obj.transform.localScale = wndSize;							
			}else{			
				wndSize = textScale;			
				wndSize.x -= cur.WindowSize.x;
				wndSize.x += cur.WindowSize.z;
				wndSize.y -= cur.WindowSize.y;
				wndSize.y += cur.WindowSize.w;
			}

			obj.transform.localScale = wndSize;
			obj.transform.localPosition = Vector3.zero;
											
			UISlicedSprite wnd = obj.AddComponent<UISlicedSprite>();
			wnd.atlas = _atlas;
			wnd.spriteName = "ToastWindow";
			wnd.depth = 0;		
			wnd.pivot = UIWidget.Pivot.TopLeft;

		}
		
	}
	
	/// <summary>
	/// トーストを非表示に
	/// </summary>
	private void _hideToast()
	{
		Transform t = Utility.FindChild(this.transform,"Toast");
		if( null == t ){
			return;
		}		
	}

#endregion
	
	
}
#endif