﻿using UnityEngine;
using System.Collections;



namespace ATK.Plugin
{
	

	public class Directory
	{

		/// <summary>
		/// 外部記憶パス
		/// </summary>
		private string _externalStorageDirectory = "";
		public string ExternalStorage {
			get {
				if( string.IsNullOrEmpty(_externalStorageDirectory) ) {
					_externalStorageDirectory = ATK.Plugin.Storage.GetExternalStorage();
				}
				return _externalStorageDirectory;
			}
		}
		
		/// <summary>
		/// 内部記憶パス
		/// </summary>
		private string _internalStorageDirectory = "";
		public string InternalStorage {
			get {
				if( string.IsNullOrEmpty(_internalStorageDirectory) ) {
					_internalStorageDirectory = ATK.Plugin.Storage.GetInternalStorage();
				}
				return _internalStorageDirectory;
			}
		}		

		public static string GetExternalSaveData()
		{
			string path = "";
			return path;
		}
	}
	
}

