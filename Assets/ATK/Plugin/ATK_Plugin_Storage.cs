﻿using UnityEngine;
using System.Collections;

#pragma warning disable 162, 414

namespace ATK.Plugin
{
	

	public static class Storage
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		/// <summary>
		/// Environment
		/// </summary>
		class Environment : System.IDisposable
		{
			public AndroidJavaClass _class = null;
			
			public Environment()
			{
				_class = new AndroidJavaClass("android.os.Environment");
			}
			
			~Environment()
			{
				Dispose();
			}
			
			public void Dispose()
			{
				if( null != _class ){
					_class.Dispose();
					_class = null;
				}
			}
		}
		private static Environment _environment = new Environment();
		
		/// <summary>
		/// Context
		/// </summary>
		class Context : System.IDisposable
		{
			public AndroidJavaObject _class = null;
			
			public Context()
			{
				using (var actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
					using(var activity = actClass.GetStatic<AndroidJavaObject>("currentActivity")){
						_class = activity.Call<AndroidJavaObject>("getApplicationContext");
					}
				}						
			}
			
			~Context()
			{
				Dispose();
			}
			
			
			public void Dispose()
			{
				if( null != _class ){
					_class.Dispose();
					_class = null;
				}
			}
		}
		private static Context _context = new Context();
		
		#endif
		
		#region 非公開メンバ変数
		
		private static string _externalStorageDirectory = null;
		private static string _internalStorageDirectory = null;

		#endregion
		
		#region 公開クラス関数
		
		/// <summary>
		/// 外部ストレージパス
		/// </summary>
		/// <returns></returns>
		public static string GetExternalStorage()
		{
			string path = null;
			
			if( RuntimeEnvironment.Plugin.IsEnabled ){

				#if UNITY_ANDROID && !UNITY_EDITOR
				
				if( string.IsNullOrEmpty(_externalStorageDirectory) ){
					
					using(var file = _environment._class.CallStatic<AndroidJavaObject>("getExternalStorageDirectory")){
						var valueString = file.Call<string>("getPath");
						Debug.Log("ExternalStorage = " + valueString);
						_externalStorageDirectory = valueString;
					}
				}

				path = _externalStorageDirectory;

				#endif
			}
			
			// デフォルト?
			if( string.IsNullOrEmpty(path) ){				
				path = UnityEngine.Application.persistentDataPath;
			}

			return path;
		}
		
		/// <summary>
		/// 内蔵ストレージパス
		/// </summary>
		/// <param name="dirname"></param>
		/// <returns></returns>
		public static string GetInternalStorage(string dirname="AppStorage")
		{
			string path = null;
			
			if( RuntimeEnvironment.Plugin.IsEnabled ){
				
				#if UNITY_ANDROID && !UNITY_EDITOR
				
				if( string.IsNullOrEmpty(_internalStorageDirectory) ){
					int modePrivate = _context._class.GetStatic<int>("MODE_PRIVATE");
					using(var file = _context._class.Call<AndroidJavaObject>("getDir","[path]",modePrivate)){								
						_internalStorageDirectory = file.Call<string>("getAbsolutePath");
					}										
				}
				
				path = _internalStorageDirectory.Replace("[path]",dirname);

				#endif

			}
			
			// デフォルト?
			if( string.IsNullOrEmpty(path) ){
				path = UnityEngine.Application.persistentDataPath;
			}
			
			return path;
		}
		
		/// <summary>
		/// テンポラリキャッシュパス
		/// </summary>
		public static string GetTemporaryStorage()
		{
			string path = null;
			
			path = UnityEngine.Application.temporaryCachePath;

			return path;
		}
		
		#endregion
		
	}
	
}// ATK


#pragma warning restore 162, 414


