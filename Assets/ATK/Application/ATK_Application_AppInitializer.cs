﻿using UnityEngine;
//using System.Collections;
//using ATK;
//using ATK.Utilities;


namespace ATK.Application
{

	/// <summary>
	/// アプリケーションの初期化
	/// </summary>
	public partial class ATK_Application_AppInitializer : CacheComponentMonoBehaviour
	{
		#region 公開メンバ変数
		

		#endregion

		#region 非公開クラス変数

		/// <summary>
		/// 初期化済みか
		/// </summary>
		static private bool _oneTimeInitialized = false;

		/// <summary>
		/// サウンド機能を有効にする
		/// </summary>	
		[SerializeField] static private bool _enabledSoundManager = true;
		
		/// <summary>
		/// シーン管理機能を有効にする
		/// </summary>	
		[SerializeField] static private bool _enabledSceneManager = true;
		
		/// <summary>
		/// シリアライズ機能を有効にする
		/// </summary>	
		[SerializeField] static private bool _enabledSaveManager = true;
		
		/// <summary>
		/// イベントシステム機能を有効にする
		/// </summary>	
		[SerializeField] static private bool _enabledEventSystemManager = true;

		#endregion


		#region 非公開メンバ変数

		/// <summary>
		/// Audio clipはサンプリングレートを変更して数フレーム待ってから読み込む
		/// </summary>	
		private int _loadResidentAudioClipsTimer = -1;


		#endregion



		#region 公開メンバ関数	

		void Awake()
		{
			
		}

		void Start () 
		{
			if( _oneTimeInitialized ) {
				return;
			}
			_oneTimeInitialized = true;
			
			_initialize();

			Debug.Log ("@@@@@ LoadUserData");
			LoadUserData();
		}

		void Update () 
		{
			if( 0 <= _loadResidentAudioClipsTimer ){
				--_loadResidentAudioClipsTimer;
				if( -1 == _loadResidentAudioClipsTimer){
					OnLoadResidentAudioClips();
				}
			}	
		}
				
		/// <summary>
		/// セーブデータ保存
		/// </summary>
		public void OnApplicationQuit()
		{				
			Debug.Log ("@@@@@ OnApplicationQuit");
			SaveUserData();		
		}

		public void OnApplicationPause()
		{
			Debug.Log ("@@@@@ OnApplicationPause");
			SaveUserData();
		}

		#endregion



		#region 非公開メンバ関数

		/// <summary>
		/// 初期化
		/// </summary>
		private void _initialize()
		{
			UnityEngine.Application.targetFrameRate = RuntimeEnvironment.Display.FrameRate;
					
			Debug.Log("App Initialize");				
			Debug.Log("フレームレートを" + UnityEngine.Application.targetFrameRate + "に変更");
			
			var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 

			// SoundManagerインスタンスの作成
			if (_enabledSoundManager ) {
				string prefabName = "ATK_SoundManager";
				ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
				
				GameObject prefab = prefabs[prefabName];			
				GameObject obj = Instantiate(prefab) as GameObject;
				obj.name = prefabName;
				GameObject.DontDestroyOnLoad(obj); // 永続化
			}
				
			// SceneManagerインスタンスの作成
			if (_enabledSceneManager ) {
				string prefabName = "ATK_SceneManager";
				ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
				
				GameObject prefab = prefabs[prefabName];			
				GameObject obj = Instantiate(prefab) as GameObject;
				obj.name = prefabName;
				GameObject.DontDestroyOnLoad(obj); // 永続化
			}
			
			// SaveManagerインスタンスの作成
			if (_enabledSaveManager ) {
				string prefabName = "ATK_SaveManager";
				ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
				
				GameObject prefab = prefabs[prefabName];			
				GameObject obj = Instantiate(prefab) as GameObject;
				obj.name = prefabName;
				GameObject.DontDestroyOnLoad(obj); // 永続化
			}

			// EventSystemManagerインスタンスの作成
			if (_enabledEventSystemManager ) {
				string prefabName = "ATK_EventSystemManager";
				ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");
				
				GameObject prefab = prefabs[prefabName];			
				GameObject obj = Instantiate(prefab) as GameObject;
				obj.name = prefabName;
				GameObject.DontDestroyOnLoad(obj); // 永続化
			}
			

					
			/* まだ

			Vector3 overlayPos = Vector3.zero;
			overlayPos.z = -100000.0f;
			
			// トースト
			{			
				GameObject prefab = prefabs["Toast"];
				GameObject parent = Instantiate(prefab) as GameObject;
				parent.name = "Toast";			
				parent.transform.position = overlayPos;
				parent.transform.localScale = Vector3.one;			
				GameObject.DontDestroyOnLoad(parent);			
			}
			
			// デバッグモード		
			Debug.Log("Debug = " + UnityEngine.Debug.isDebugBuild);
			if( UnityEngine.Debug.isDebugBuild && (null == DebugMode.Instance) ){
				GameObject prefab = prefabs["DebugMode"];
				GameObject parent = Instantiate(prefab) as GameObject;
				parent.name = "DebugMode";			
				parent.transform.position = overlayPos;
				parent.transform.localScale = Vector3.one;			
				GameObject.DontDestroyOnLoad(parent);					
			}			
			*/
			
			#if UNITY_IPHONE
			AudioSettings.outputSampleRate = 44100;
			_loadResidentAudioClipsTimer = 4;
			#endif
			
			#if UNITY_EDITOR || !UNITY_IPHONE
			_loadResidentAudioClipsTimer = -1;
			OnLoadResidentAudioClips();
			#endif		
			
			OnInitialize();		
		}

		#endregion


		#region アプリ毎に実装

		/// <summary>
		/// 常駐サウンド読み込み
		/// </summary>
		partial void OnLoadResidentAudioClips();

		/// <summary>
		/// 初期化
		/// </summary>
		partial void OnInitialize();

		partial void SaveUserData();
		partial void LoadUserData();



		#endregion	

			
	}

}