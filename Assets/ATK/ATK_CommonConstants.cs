﻿

using UnityEngine;
using System.Collections;


namespace ATK
{

    /// <summary>
    /// 定数
    /// </summary>
    public static class CommonConstants
    {
        /*
        /// <summary>
        /// 広告
        /// </summary>
        public partial class AdsConstants
        {
            /// <summary>
            /// バナーの形
            /// </summary>
            public enum BannerType
            {
                /// Android : 画面の幅×32、50 または 90  ATK_AdmobManager.BannerViewType.SmartBannerを使用
                /// iOS     : 従来のスマートバナー        ATK_iAdManager.BannerViewType.SmartBannerを使用
                SmartBanner,

                /// Android : 300×250 ATK_AdmobManager.BannerViewType.MediumRectangleを使用
                /// iOS     : 300×250 ATK_iAdManager.BannerViewType.MediumRectを使用
                MediumRect,
            }

            /// <summary>
            /// バナーのレイアウト
            /// </summary>
            public enum BannerLayout
            {
                /// Android : ATK_AdmobManager.BannerViewLayout.～を使用
                /// iOS     : ATK_iAdManager.BannerViewLayout.～を使用
                Top,
                Bottom,
                TopLeft,
                TopRight,
                BottomLeft,
                BottomRight,
            }
        }
        */

        //TODO いまのところ未使用
        /// <summary>
        /// 汎用キー
        /// </summary>
        public static partial class KeyConstants
        {
            public const string KEY_TYPE = "type";
            public const string KEY_ID = "id";
            public const string KEY_POSITION = "position";
            public const string KEY_ROTATION = "rotation";
            public const string KEY_SCALE = "scale";
            public const string KEY_TEXTURE = "texture";
            public const string KEY_PARENT = "parent";
            public const string KEY_EFFECT = "effect";
            public const string KEY_COLOR = "color";
            public const string KEY_DATE = "date";
            public const string KEY_POINTS = "points";
            public const string KEY_MESSAGE = "message";
            public const string KEY_TEXT = "text";
            public const string KEY_VALUE = "value";
        }

    }
}

