﻿/// <summary>
/// シングルトンクラス
/// CacheComponentMonoBehaviourクラスを使用する。ThirdParty/Editor/CacheComponentMonoBehaviour.csを参照
/// </summary>

using UnityEngine;
using System.Collections;

namespace ATK 
{

//old	public abstract class ATK_SingletonMonoBehaviour<T> : MonoBehaviour where T : ATK_SingletonMonoBehaviour<T>
	public abstract class ATK_SingletonMonoBehaviour<T> : CacheComponentMonoBehaviour where T : ATK_SingletonMonoBehaviour<T>
	{
		protected static T instance;
		public static T Instance {
			get {
				if (instance == null) {
					instance = (T)FindObjectOfType (typeof(T));
					
					if (instance == null) {
						Debug.LogWarning (typeof(T) + "is nothing");
					}
				}
				
				return instance;
			}
		}
		
		virtual protected void Awake()
		{
			_checkInstance();
		}
		
		protected bool _checkInstance()
		{
			Debug.Log("CheckInstance : " + this.name + "<" + typeof(T).Name + ">");


			if( instance == null) {
				instance = (T)this;
				return true;
			}
			else if( Instance == this ) {
				return true;
			}

			Debug.LogWarning(typeof(T).Name + "component is singleton!");
			#if true // コンポーネントのみを削除を推奨
			// コンポーネントを削除
			Debug.LogWarning("destorying component! object name = " + this.name);
			Destroy(this);
			#else
			// オブジェクトを削除
			Debug.LogWarning("destorying object! object name = " + this.name);
			Destroy(this.gameObject);
			#endif

			return false;
		}
	}

}




/*

==================================================
Awakeについて
==================================================
派生側で Awake を使う場合は、_checkInstance()を呼ぼう
override protected void Awake() 
{
	base.Awake(); 
	～ 処理 ～
}




*/