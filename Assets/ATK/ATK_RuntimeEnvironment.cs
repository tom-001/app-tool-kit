﻿using UnityEngine;
using System.Collections;

#pragma warning disable 1030

namespace ATK
{
	
	/// <summary>
	/// 実行環境設定
	/// </summary>
	public static class RuntimeEnvironment
    {
#if DEBUG

        /// MonoDevelop なら Player Settings > Other Settings > Configuration > Scripting Define Symbols で DEBUG 定義されているなら
        // visual studioでDEBUGシンボルが定義されているなら
        // そちらを使用する
        public const string Enable = "DEBUG";
		public const string Disable = "NDEBUG";

		#else

		#if !ATK_ENABLE
		#warning    'undefined ATK_ENABLE'
		#endif

		#if ATK_DISABLE
		#error  'redefined ATK_DISABLE'
		#endif		

		public const string Enable = "ATK_ENABLE";
		public const string Disable = "ATK_DISABLE";

		#endif
		
		static public DisplayEnvironment Display = new DisplayEnvironment();		
		static public InputEnvironment Input = new InputEnvironment();		
		static public AudioEnvironment Audio = new AudioEnvironment();
		
		

		/// <summary>
		/// プラグイン環境設定
		/// </summary>
		public class Plugin
		{
			#if UNITY_ANDROID && !UNITY_EDITOR
			public const bool IsEnabledAndroid = true;
			public const string EnableAndroid = RuntimeEnvironment.Enable;
			#else
			public const bool IsEnabledAndroid = false;
			public const string EnableAndroid = RuntimeEnvironment.Disable;
			#endif

			#if UNITY_IPHONE && !UNITY_EDITOR
			public const bool IsEnablediOS = true;
			public const string EnableiOS = RuntimeEnvironment.Enable;
			#else
			public const bool IsEnablediOS = false;
			public const string EnableiOS = RuntimeEnvironment.Disable;
			#endif

			#if UNITY_EDITOR
			public const bool IsEnabled = false;
			public const bool IsEnabledEditor = true;
			public const string EnableEditor = RuntimeEnvironment.Enable;
			#else
			public const bool  IsEnabled = true;
			public const bool IsEnabledEditor = false;
			public const string EnableEditor = RuntimeEnvironment.Disable;
			#endif
		}
		

		
		/// <summary>
		/// デバッグ環境設定
		/// </summary>
		public class Debug
		{
			#if ATK_DEBUG

			#warning 'enable Debug'
			public const string Enable = RuntimeEnvironment.Enable;
			public const bool IsEnabled = true;

			#else

			#warning 'disable Debug'
			public const string Enable = RuntimeEnvironment.Disable;
			public const bool IsEnabled = false;

			#endif	
		}
		
		/// <summary>
		/// アサート設定
		/// </summary>
		public class Assert
		{
			#if ATK_DEBUG

			#warning 'enable Assert'
			public const string Enable = RuntimeEnvironment.Enable;
			public const bool IsEnabled = true;

			#else

			#warning 'disable Assert'
			public const string Enable = RuntimeEnvironment.Disable;
			public const bool IsEnabled = false;

			#endif
		}
		
	}
	

	
	/// <summary>
	/// 入力環境
	/// </summary>
	public partial class InputEnvironment
	{
		#region 非公開メンバ変数
		#endregion
		
		
		#region プロパティ
		
		/// <summary>
		/// PCでマウスタッチをエミュレート
		/// </summary>
		public bool IsTouchEmulate
		{
			get
			{
				bool ret = false;
				#if UNITY_IPHONE || UNITY_ANDROID
					#if UNITY_EDITOR
						ret = true;
					#endif
				#else
					ret = true;
				#endif
				
				return ret;
			}
		}
		
		#endregion
		
		
		#region 公開メンバ関数
		
		// コンストラクタ
		public InputEnvironment()
		{
			_setup();
		}
		
		#endregion
		
		#region 非公開メンバ関数
		
		/// <summary>
		/// 値を変更
		/// </summary>
		partial void _setup();
		
		#endregion
		
		
	} 
	


	/// <summary>
	/// Audioの設定
	/// </summary>
	public partial class AudioEnvironment
	{
		// 同時に再生出来る数			
		#region 非公開メンバ変数
		private int _numBGMChannels = 1;
		private int _numSEChannels = 1;
		private int _numSystemSEChannels = 1;
		private int _numVoiceChannels = 1;
		#endregion
		
		#region プロパティ
		
		public int NumBGMChannels { get { return _numBGMChannels; } }
		public int NumSEChannels { get { return _numSEChannels; } }
		public int NumSystemSEChannels { get { return _numSystemSEChannels; } }
		public int NumVoiceChannels { get { return _numVoiceChannels; } }
		
		#endregion
		
		#region 公開メンバ関数
		
		// コンストラクタ
		public AudioEnvironment()
		{
			_setup();
		}
		
		#endregion
		
		#region 非公開メンバ関数
		
		/// <summary>
		/// 値を変更
		/// </summary>
		partial void _setup();
		
		#endregion
		
		
	}
	
	
	
	/// <summary>
	/// 画面の設定
	/// </summary>
	public partial class DisplayEnvironment
	{
		#region 非公開メンバ変数
		
		/// <summary>
		/// ゲームの解像度
		/// </summary>
		private int _width = 960;
		
		/// <summary>
		/// ゲームの解像度
		/// </summary>		
		private int _height = 640;
		
		/// <summary>
		/// フレームレート
		/// </summary>
		private int _frameRate = 60;
		
		/// <summary>
		/// セーフフレーム
		/// </summary>
		private int _safeWidth = 0;
		
		/// <summary>
		/// セーフフレーム
		/// </summary>		
		private int _safeHeight = 0;

		#endregion


		
		#region プロパティ
		
		public int Width { get { return _width; } }
		public int Height { get { return _height; } }
		public int SafeWidth { get { return _safeWidth; } }
		public int SafeHeight { get { return _safeHeight; } }		
		public int FrameRate { get { return _frameRate; } }

		#endregion
		


		#region 公開メンバ関数
		
		// コンストラクタ
		public DisplayEnvironment()
		{
			_setup();
			
			if( 0 >= _safeWidth ){
				_safeWidth = _width;
			}
			if( 0 >= _safeHeight ){
				_safeHeight = _height;
			}
		}
		
		#endregion
		
		#region 非公開メンバ関数
		
		/// <summary>
		/// 値を変更
		/// </summary>
		partial void _setup();
		
		#endregion
		
	}		


}

#pragma warning restore 1030
