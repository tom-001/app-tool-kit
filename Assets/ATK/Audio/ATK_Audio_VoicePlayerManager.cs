﻿using UnityEngine;
using System.Collections;


namespace ATK.Audio 
{
	public class ATK_Audio_VoicePlayerManager : ATK_Audio_Manager<ATK_Audio_VoicePlayerManager>
	{
	
		#region 公開メンバ関数	
		
		override protected void Awake()
		{
			base.Awake();
			if ( base._checkInstance() ) {
				if( 0 < RuntimeEnvironment.Audio.NumVoiceChannels ) {
						_initialize("Voice", RuntimeEnvironment.Audio.NumVoiceChannels);
				}
			}
		}
			
		#endregion
			
	}
}