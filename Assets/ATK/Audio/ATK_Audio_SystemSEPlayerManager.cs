﻿using UnityEngine;
using System.Collections;


namespace ATK.Audio 
{
	public class ATK_Audio_SystemSEPlayerManager : ATK_Audio_Manager<ATK_Audio_SystemSEPlayerManager>
	{
			
		#region 公開メンバ関数	
				
		override protected void Awake()
		{
			base.Awake();
			if ( base._checkInstance() ) {
				if( 0 < RuntimeEnvironment.Audio.NumSystemSEChannels ) {
					_initialize("SystemSE", RuntimeEnvironment.Audio.NumSystemSEChannels);
				}
			}
		}
			
		#endregion


	}
}