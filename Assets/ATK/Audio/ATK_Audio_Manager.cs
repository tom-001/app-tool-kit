﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ATK;

namespace ATK.Audio 
{

/// <summary>
/// Audioマネージャ
/// </summary>
public class ATK_Audio_Manager<T> : ATK_SingletonMonoBehaviourFast<T> where T : ATK_Audio_Manager<T>
{

#region 公開メンバ変数
	
	/// <summary>
	/// Audioプレイヤー
	/// </summary>
	public List<ATK.Audio.Player> Player = new List<ATK.Audio.Player>();
	
#endregion


#region 公開メンバ関数
	
	void Update()
	{	
		for(int i = 0; i < this.Player.Count; ++i){
			this.Player[i].Update(Time.deltaTime);
		}
	}
	
	/// <summary>
	/// マスター音量一括変更
	/// </summary>
	/// <param name="vol"></param>
	public void SetMasterVolume(float vol)
	{
		for(int i = 0; i < this.Player.Count; ++i){
			this.Player[i].MasterVolume = vol;
		}	
	}

	/// <summary>
	/// マスター音量取得
	/// </summary>
	/// <param name="vol"></param>
	public float GetMasterVolume()
	{
		return this.Player[0].MasterVolume;
	}

	/// <summary>
	/// 全て停止
	/// </summary>
	public void StopAll(float time)
	{
		foreach(ATK.Audio.Player p in Player){
			p.Stop(time);
			p.Clear();
		}
	}
		
	public void StopAll()
	{
		StopAll(0.2f);
	}
	
	/// <summary>
	/// 範囲指定で停止
	/// </summary>
	/// <param name="begin"></param>
	/// <param name="n"></param>
	public void StopRange(
		int begin,
		int n
		)
	{
		for(int i = 0; i < n; ++i){
			this.Player[i+begin].Stop();
			this.Player[i+begin].Clear();
		}
	}
	
			
	/// <summary>
	/// 開いてるプレイヤーを探す
	/// </summary>
	/// <param name="begin"></param>
	/// <param name="n"></param>
	/// <returns></returns>
	public ATK.Audio.Player GetFree(
		int begin,
		int n
		)
	{		
		ATK.Audio.Player first = null;
	
		for(int i = 0; i < n; ++i){
			ATK.Audio.Player player = this.Player[begin+i];
			if( !player.NowPlaying() ){
				return player;
			}
			
			
			if( null == first ){
				first = player;
			}else if( first.StartTime > player.StartTime ){
				first = player;
			}			
		}
		
		return first;
	}
	public ATK.Audio.Player GetFree()
	{			
		return GetFree(0,this.Player.Count);
	}
	
#endregion	

		
#region 非公開メンバ関数	
		
	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="numOf"></param>
	protected void _initialize(
		string name,
		int numOf
		)
	{
		Transform parent = this.transform;
		for(int i = 0; i < numOf; ++i){
			GameObject obj = new GameObject();
			obj.transform.parent = parent;			
			obj.name = string.Format("{0}({1})",name,i);
			obj.AddComponent<AudioSource>();			
			this.Player.Add( new ATK.Audio.Player(obj.GetComponent<AudioSource>()));		
		}	
	}

#endregion
	
}

}