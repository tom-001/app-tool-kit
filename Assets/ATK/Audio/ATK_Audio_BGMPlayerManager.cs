﻿using UnityEngine;
using System.Collections;

namespace ATK.Audio 
{
	public class ATK_Audio_BGMPlayerManager : ATK_Audio_Manager<ATK_Audio_BGMPlayerManager>
	{


		#region 公開メンバ関数	

		override protected void Awake()
		{
			base.Awake();
			if ( base._checkInstance() ) {
				if( 0 < RuntimeEnvironment.Audio.NumBGMChannels ) {
					_initialize("BGM", RuntimeEnvironment.Audio.NumBGMChannels);
				}
			}
		}

		#endregion

	}
}