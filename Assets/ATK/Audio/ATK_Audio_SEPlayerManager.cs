﻿using UnityEngine;
using System.Collections;


namespace ATK.Audio 
{
	public class ATK_Audio_SEPlayerManager : ATK_Audio_Manager<ATK_Audio_SEPlayerManager>
	{

		#region 公開メンバ関数	
			
		override protected void Awake()
		{
			base.Awake();
			if ( base._checkInstance() ) {
				if( 0 < RuntimeEnvironment.Audio.NumSEChannels ){
					_initialize("SE", RuntimeEnvironment.Audio.NumSEChannels);
				}
			}
		}

		#endregion

	}
}