﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;

namespace ATK.Audio
{
	/// <summary>
	/// 再生リクエスト
	/// </summary>
	public class Request
	{
	#region 公開メンバ変数
			
		/// <summary>
		/// 再生開始までの遅延
		/// </summary>
		public float Delay = 0.0f;
			
		/// <summary>
		/// フェードイン時間
		/// </summary>
		public float FadeIn = 0.0f;
			
		/// <summary>
		/// 音量
		/// </summary>
		public float Volume = 1.0f;
			
		/// <summary>
		/// Audioクリップ
		/// </summary>
		public AudioClip Clip = null;		
			
		/// <summary>
		/// ループ再生
		/// </summary>
		public bool Loop = false;

		/// <summary>
		/// 出力
		/// </summary>
		public AudioMixerGroup Output = null;

	
	#endregion
	}
		
	/// <summary>
	/// 音量コントローラ
	/// </summary>
	class VolumeController
	{
	#region 非公開メンバ変数
	
		private float _currentTime = 0.0f;
		private float _endTime = 0.0f;
		private float _startVolume = 0.0f;
		private float _endVolume =  0.0f;
	#endregion
	
	#region 公開メンバ関数
		
		public VolumeController()
		{
			Reset();
		}
			
		public void Reset()
		{
			_startVolume = 0.0f;
			_endVolume = 1.0f;
			_currentTime = 0.0f;
			_endTime = -1.0f;
		}
			
		public void Start(
			float start,
			float end,
			float endTime
			)
		{
			Reset();
			_endTime = endTime;
			_startVolume = start;
			_endVolume = end;			
		}
			
		public bool Update(
			out float volume,
			float deltaTime
			)
		{
			volume = _endVolume;
			
			if( 0.0f <= _endTime ){
				_currentTime += deltaTime;
				if( _endTime <= _currentTime ){
					_endTime = -1.0f;
				}else{
					float ratio = _currentTime / _endTime;
					float range = _endVolume - _startVolume;
					volume = _startVolume + range * ratio;
				}
				return true;
			}
			return false;
		}
		
		public bool IsEnd()
		{
			return 0.0f > _endTime;
		}
		
	#endregion
	
	}
				
		
	/// <summary>
	/// Audio再生管理
	/// </summary>
	public class Player
	{
	
	#region 非公開メンバ変数	
		
		private VolumeController _fadeCtrl = new VolumeController();
		private VolumeController _stopCtrl = new VolumeController();
		
		/// <summary>
		/// リクエストキュー
		/// </summary>
		private List<Request> _requests = new List<Request>();
			
		private AudioSource _source = null;
		private Request _current;
		private bool _updateVolume = false;
		private float _volume = 0.0f;
		private System.DateTime _startTime;
		
		/// <summary>
		/// 停止要求
		/// </summary>
		private bool _stop = false;
			
		
		/// <summary>
		/// ベース音量
		/// </summary>
		private float _masterVolume = 1.0f;
	
			
		private enum State
		{
			Idle,	//!< 再生準備中
			Delay,	//!< Delay
			Playing,//!< 再生中
			Stop,	//!< 停止中
			End,	//!< おしまい
		}
		State _state = State.Idle;
	
	#endregion		
		
	#region 公開メンバ関数
		
		public Player(AudioSource source)
		{
			MasterVolume = 1.0f;		
			_source = source;
		}
		
		
		/// <summary>
		/// 毎フレームの更新
		/// </summary>
		/// <param name="deltaTime">更新時間</param>
		public void Update(float deltaTime){
			
			while( _update(deltaTime) ){			
			}
			
			float volume0 = _volume;
			if( _fadeCtrl.Update(out volume0,deltaTime) ){
				_updateVolume = true;
				_volume = volume0;
			}
			
			float volume1 = 1.0f;
			if( State.Stop == _state ){
				if( _stopCtrl.Update(out volume1,deltaTime) ){
					_updateVolume = true;
				}
			}
			
			// 音量変更
			if( _updateVolume ){
				_updateVolume = false;			
				float volume = _masterVolume * volume0 * volume1;
				_source.volume = volume;			
			}
		
		}
		
		/// <summary>
		/// 再生中か
		/// </summary>
		/// <returns></returns>
		public bool NowPlaying()
		{
			if( (null == _current) && (0 == _requests.Count) ){
				return false;
			}else{
				return true;
			}
		}
		
		/// <summary>
		/// ポーズ
		/// </summary>
		public void Pause()
		{		
			_source.Pause();
		}
		
		/// <summary>
		/// ポーズ解除
		/// </summary>
		public void Restart()
		{
			_source.Play();
		}
		
		
		/// <summary>
		/// リクエスト作成
		/// </summary>
		/// <returns></returns>
		public Request Create() { return new Request(); }
		
		/// <summary>
		/// 再生リクエストの追加
		/// </summary>
		/// <param name="request"></param>
		public void Add(Request request)
		{
			if( null == request.Clip ){
				// リクエスト
				return;
			}
			_requests.Add(request);		
		}
		
		/// <summary>
		/// リクエストのクリア
		/// </summary>
		public void Clear()
		{
			_requests.Clear();
		}
		
		/// <summary>
		/// 再生中のBGMを停止
		/// </summary>
		/// <param name="fadeTime"></param>	
		public void Stop(float fadeTime)
		{
			if( (State.End == _state) || (State.Stop == _state) ){
				return;
			}
		
			_stop = true;
			_stopCtrl.Start(
				_volume,
				0.0f,
				fadeTime
				);
		}
		public void Stop()
		{
			Stop(0.01f);
		}

		/// <summary>
		/// 指定時間でゆっくり音量変更
		/// </summary>
		/// <param name="volume"></param>
		/// <param name="time"></param>
		public void ChangeVolume(
			float volume,
			float time
			)
		{
			volume = Mathf.Clamp01(volume);
			_updateVolume = true;				
			_fadeCtrl.Start(_volume,volume,time);
		}

		
		
		/// <summary>
		/// 毎フレームの更新
		/// </summary>
		/// <param name="deltaTime"></param>
		/// <returns></returns>
		private bool _update(float deltaTime)
		{
			switch( _state ){
			
			case State.Idle:
				{
					// 再生リクエスト待ち
					if( 0 != _requests.Count ){
						_reset();
						_current = _requests[0];
						_requests.RemoveAt(0);
						_state = State.Delay;
						return true;
					}
					
				}
				break;
				
			case State.Delay:
				{
					if( _stop ){
						_state = State.End;
						return true;
					}
					
					// Delay
					_current.Delay -= deltaTime;
					if( 0.0f >= _current.Delay ){
						
						// 再生開始
						_state = State.Playing;					
						_startTime = System.DateTime.Now;
						
						_volume = 0.0f;
						_source.loop = _current.Loop;
						_source.clip = _current.Clip;
						_source.volume = 0.0f;
						_source.outputAudioMixerGroup = _current.Output;
						_source.Play();					

						_fadeCtrl.Start(0.0f,_current.Volume,_current.FadeIn);
						_updateVolume = true;										
					}						
				}
				break;
				
			case State.Playing:
				{
					// 再生中
					if( _stop ){
						// stop処理開始
						_state = State.Stop;
						return true;
					}else{
						if( !_source.isPlaying ){
							// 再生終了
							_state = State.End;
							return true;
						}
					}
				}break;
				
			case State.Stop:
				// 停止中
				{
					if( _stopCtrl.IsEnd() ){
						_source.Stop();
						_state = State.End;
						return true;
					}
				}
				break;
				
			case State.End:
				{
					// 終わり
					_current = null;
					_source.clip = null;
					_source.outputAudioMixerGroup = null;
					_stop = false;
					_state = State.Idle;				
				}return true;
			}
			return false;
		}
		
		/// <summary>
		/// 現在再生されているクリップか調べる
		/// </summary>
		/// <param name="clip"></param>
		/// <returns></returns>
		public bool NowPlayingClip(AudioClip clip)
		{
			if( (State.End == _state) || (State.Stop == _state) ){
				return false;
			}else{
				
				if( _source.clip == clip ){
					return true;
				}
				if( (null != _current) && (_current.Clip == clip) ){
					return true;
				}
				return false;
			}
		}
		
		/// <summary>
		/// 状態リセット
		/// </summary>
		private void _reset()
		{
			_updateVolume = true;
			_stop = false;
			_volume = 1.0f;
			_fadeCtrl.Reset();
			_stopCtrl.Reset();
		}
	
	#endregion
	
	#region プロパティ
				
		/// <summary>
		/// マスター音量の変更
		/// </summary>
		public float MasterVolume
		{
			get{ return _masterVolume; }
			set
			{
				_masterVolume = Mathf.Clamp01(value);
				_updateVolume = true;			
			}
		}

		/// <summary>
		/// 音量の変更
		/// </summary>
		public float Volume
		{
			get{ return _volume; }
			set
			{
				_volume = Mathf.Clamp01(value);
				_updateVolume = true;			
			}
		}						
		
		/// <summary>
		/// 再生開始時間
		/// </summary>
		public long StartTime
		{
			get { return _startTime.ToBinary(); }
		}
		
	#endregion
	
	}
			
}

