﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ATK.Audio
{

	/// <summary>
	/// Audioクリップのまとめ
	/// </summary>
	public class SoundBank
	{	
	#region 非公開メンバ変数	
		private Dictionary<string, AudioClip> _clips = new Dictionary<string, AudioClip>();
	#endregion
	
	#region 公開クラス関数
	
		/// <summary>
		/// resourceから一括読み込み
		/// </summary>
		/// <param name="path"></param>
		public static SoundBank Create(string path)
		{
			SoundBank bank = new SoundBank();
			bank.LoadAll(path);
			return bank;
		}
				
	#endregion
			
		
	#region 公開メンバ関数
	
	

		/// <summary>
		/// コンストラクタ
		/// </summary>
		
		public SoundBank()
		{}
		
		
		/// <summary>
		/// クリップ削除
		/// </summary>
		public void Clear()
		{
			_clips.Clear();
		}
		
		/// <summary>
		/// clip追加
		/// </summary>
		/// <param name="name">name</param>
		/// <param name="clip">clip</param>
		public void Add(
			string name,
			AudioClip clip
			)
		{
			_clips.Add(name,clip);
		}
		
		/// <summary>
		/// clip追加
		/// </summary>
		/// <param name="clip"></param>	
		public void Add(AudioClip clip)
		{
			Add(clip.name,clip);
		}
		
		/// <summary>
		/// clip追加
		/// </summary>
		/// <param name="p"></param>
		public void Add(Dictionary<string,AudioClip> p)
		{
			foreach (KeyValuePair<string, AudioClip> pair in p) {
				Add(pair.Key,pair.Value);
			}		
		}
		
		/// <summary>
		/// resourceから一括読み込み
		/// </summary>
		/// <param name="path"></param>
		public void LoadAll(string path)
		{
			Add( Utilities.Resources.LoadAll<AudioClip>(path) );		
		}
		

		/// <summary>
		/// audioクリップの取得
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public AudioClip GetAudioClip(string name)
		{		
			if( !_clips.ContainsKey(name) ){
				return null;
			}
			return _clips[name]; 		
		}	
		
	#endregion
					
	}
	
	/// <summary>
	/// サウンドバンクデータベース
	/// </summary>
	public static class SoundBankDB
	{
	#region 非公開メンバ変数
		private static Dictionary<string, SoundBank> __banks = new Dictionary<string, SoundBank>();	
	#endregion
	
	#region 公開クラス関数
	
		/// <summary>
		/// サウンドバンク追加
		/// </summary>
		/// <param name="name"></param>
		/// <param name="bank"></param>
		public static void Add(
			string name,
			SoundBank bank
			)
		{
			__banks.Add(name,bank);
		}
		
		/// <summary>
		/// サウンドバンク取得
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static SoundBank Find(string name)
		{
			return __banks[name];
		}
		
		/// <summary>
		/// サウンドバンク削除
		/// </summary>
		/// <param name="name"></param>
		public static void Remove(string name)
		{
			__banks.Remove(name);
		}
		
		/// <summary>
		/// audioclipの取得
		/// </summary>
		/// <param name="bankName"></param>
		/// <param name="clipName"></param>
		/// <returns></returns>
		public static AudioClip Find(
			string bankName,
			string clipName
			)
		{
			SoundBank bank = null;
			if( __banks.TryGetValue(bankName,out bank) ){
				return bank.GetAudioClip(clipName);
			}else{
				Assert.Fail(string.Format("No'tFound {0}",bankName));
			}			
			return null;
		}
	
	#endregion
	
			
		
	}	



}// Audio

