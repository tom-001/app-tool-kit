﻿#if !UNITY_EDITOR
#define DEBUG_OVERWLAP
#endif

using UnityEngine;

/// <summary>
/// Debug ラップクラス
/// </summary>
/// エディタでは UnityEngine の Debug クラスが使われ、
/// それ以外のプラットフォームでは ラッパーが使用される。
/// ログを出力したい場合は、Debelopment Build にチェックをいれる
#if DEBUG_OVERWLAP

public static class Debug
{
	private static bool isEnable()
	{
		return UnityEngine.Debug.isDebugBuild; 
	}

	public static bool isDebugBuild
	{
		get { return isEnable(); }
	}

	public static void Break()
	{
		if( isEnable() ) {
			UnityEngine.Debug.Break();
		}
	}
	
	public static void Log( object message )
	{
		if( isEnable() ) {
			UnityEngine.Debug.Log( message );
		}
	}

	public static void Log( object message, Object context )
	{
		if( isEnable() ) {
			UnityEngine.Debug.Log( message, context );
		}
	}
	
	public static void LogWarning( object message)
	{
		if( isEnable() ) {
		}
	}
	
	public static void LogWarning( object message, Object context )
	{
		if( isEnable() ) {
			UnityEngine.Debug.LogWarning( message, context );
		}
	}

	public static void LogError( object message)
	{
		if( isEnable() ) {
			UnityEngine.Debug.LogError( message );
		}
	}
	
	public static void LogError( object message, Object context )
	{
		if( isEnable() ) {
			UnityEngine.Debug.LogError( message, context );
		}
	}
	
	public static void LogException( System.Exception exception )
	{
		if( isEnable() ) {
			UnityEngine.Debug.LogException( exception );
		}
	}
	
	public static void LogException( System.Exception exception, Object context )
	{
		if( isEnable() ) {
			UnityEngine.Debug.LogException( exception, context );
		}
	}

	
}

#endif