﻿/// <summary>
/// シングルトンクラス
/// タグ名をあらかじめ設定しておくことで、高速化になる。
/// Awake が呼ばれた後は大差ない。
/// CacheComponentMonoBehaviourクラスを使用する。ThirdParty/Editor/CacheComponentMonoBehaviour.csを参照
/// </summary>


using UnityEngine;
using System;
using System.Collections;


namespace ATK 
{

//old	public abstract class ATK_SingletonMonoBehaviourFast<T> : MonoBehaviour where T : ATK_SingletonMonoBehaviourFast<T>
	public abstract class ATK_SingletonMonoBehaviourFast<T> : CacheComponentMonoBehaviour where T : ATK_SingletonMonoBehaviourFast<T>
	{
		protected static readonly string[] _findTags  =
		{
			"GameController",
			"Manager",
		};

		
		protected static T _instance;
		public static T Instance {
			get {
				if (_instance == null) {
					
					Type type = typeof(T);
					
					foreach( var tag in _findTags )
					{
						GameObject[] objs = GameObject.FindGameObjectsWithTag(tag);

						for(int j=0; j<objs.Length; j++)
						{
							_instance = (T)objs[j].GetComponent(type);
							if( _instance != null)
								return _instance;
						}
					}
					
					Debug.LogWarning( string.Format("{0} is not found", type.Name) );
				}
				
				return _instance;
			}
		}
		
		virtual protected void Awake()
		{
			_checkInstance();
		}
		
		protected bool _checkInstance()
		{
			Debug.Log("CheckInstance : " + this.name + "<" + typeof(T).Name + ">");

			if( _instance == null) {
				_instance = (T)this;
				return true;
			}
			else if( Instance == this ) {
				return true;
			}
			
			Debug.LogWarning(typeof(T).Name + "component is singleton!");
			#if false // コンポーネントのみを削除を推奨
			// コンポーネントを削除
			Debug.LogWarning("destorying component! object name = " + this.name);
			Destroy(this);
			#else
			// オブジェクトを削除
			Debug.LogWarning("destorying object! object name = " + this.name);
			Destroy(this.gameObject);
			#endif

			return false;
		}

	}

}



/*

==================================================
Awakeについて
==================================================
派生側で Awake を使う場合は、_checkInstance()を呼ぼう
override protected void Awake() 
{
	base.Awake(); 
	～ 処理 ～
}


*/