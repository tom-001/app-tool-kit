﻿using System;
using System.Collections;
using ATK.Extension;


namespace ATK.Utilities
{
	public class TypedBitArray<T> where T : struct, IConvertible
    {
        public bool this[T id]
        {
            get { return _array.Get(id.ToInt32(null)); }
            set { _array.Set(id.ToInt32(null), value); }
        }
        public bool this[int index]
        {
            get { return _array.Get(index); }
            set { _array.Set(index, value); }
        }
        private BitArray _array;
		
		public TypedBitArray( T maxSize ) 
		{
			_array = new BitArray( maxSize.ToInt32( null ) );
		}

        public int Length
        {
            get { return _array.Length; }
        }

        public string BitStrings()
        {
            string ret = "";
            foreach(bool b in _array)
            {
                ret += b ? "1" : "0";
            }
            return ret;
        }

        /// <summary>
        /// 全てのビットをONにする
        /// </summary>
        public void AllOn()
        {
            _array.AllOn();
        }

        /// <summary>
        /// 全てのビットをOFFにする
        /// </summary>
        public void AllOff()
        {
            _array.AllOff();
        }

        /// <summary>
        /// いずれかのビットがONかどうか
        /// </summary>
        public bool Any()
        {
            return _array.Any();
        }

        /// <summary>
        /// 全てのビットがONかどうか
        /// </summary>
        public bool All()
        {
            return _array.All();
        }

        /// <summary>
        /// 全てのビットがOFFかどうか
        /// </summary>
        public bool None()
        {
            return !Any();
        }

        /// <summary>
        /// 指定したビットを反転する
        /// </summary>
        /// <param name="index">Index.</param>
        public void Flip(int index)
        {
            _array.Flip(index);
        }

        /// <summary>
        /// バイト配列に変換 8bit -> 1byte
        /// </summary>
        /// <returns></returns>
        public byte[] ToByteArray()
        {
            return _array.ToByteArray();
        }

        /// <summary>
        /// bool配列に変換 1bit -> 1bool
        /// </summary>
        /// <returns></returns>
        public bool[] ToBoolArray()
        {
            return _array.ToBoolArray();
        }

    }


}

/*



==================================================
使用例
==================================================
//http://rikiong.hatenablog.com/entry/2013/09/13/203341

enum MoveType{
    Slow,
    Normal,
    Fast,
    Max,
};

TypedBitArray<MoveType> bits = new TypedBitArray<MoveType>( MoveType.Max );


if( bits[MoveType.Slow] ){
    bits[MoveType.Slow] = false;
    bits[2] = true;
}


 */
