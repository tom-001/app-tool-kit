﻿/*
 * セーブデータのロード・セーブ
 */

using UnityEngine;
using System.Collections;
using System.Security.Cryptography;

namespace ATK.Utilities
{
	
	/// <summary>
	/// セーブデータ破損例外
	/// </summary>
	public class SaveDataBrokenException : System.Exception
	{
		public SaveDataBrokenException(string msg) : 
			base(msg)
		{}
	}
	
	/// <summary>
	/// セーブデータバージョンエラー例外
	/// </summary>
	public class SaveDataVersionMissMatchException : System.Exception
	{
		public SaveDataVersionMissMatchException() : 
			base("SaveData Version MissMatch")
		{}
	}
	
	
	
	/// <summary>
	/// セーブデータの読み書き
	/// </summary>
	public static class SaveData
	{

		#region 定数
		public static readonly string ROOT_PATH = UnityEngine.Application.persistentDataPath;
		
		/// Android用のアプリケーション領域のパス　アンロックなどのコピーされたくない情報を保存するのに使う
		public static readonly string ROOT_PATH_INTERNAL = ATK.Plugin.Storage.GetInternalStorage();
		
		
		/// <summary>
		/// 読み込み結果
		/// </summary>
		public enum EError
		{
			/// <summary>
			/// 成功
			/// </summary>
			Succeed,
			
			/// <summary>
			/// ファイルなし
			/// </summary>
			FileNotFound,
			
			/// <summary>
			/// ファイル破損
			/// </summary>
			Broken,	
			
			/// <summary>
			/// バージョン違い
			/// </summary>
			VersionMissMatch,
			
			/// <summary>
			/// その他エラー
			/// </summary>
			Unknown,
		};
		
		/// <summary>
		/// 暗号化キー
		/// </summary>
		private static readonly byte[] MAGIC =
		{
			0xcc,0xa3,0x91,0xa9,
			0xad,0x77,0xfb,0xa3,				
			0x75,0xef,0xc7,0xf4,
			0xad,0xe5,0xd8,0x6e,
			0x6c,0xf1,0xd6,0x3a,		
			0xe7,0x6c,0x9c,0x35,
			0x48,0x09,0x05,0x5c,
			0x86,0xdc,0xda,0xae,		
			0xb9,0x1c,0x65,0x6e,
			0x2a,0x23,0x48,0x8f,
			0x4b,0xa7,0x1d,0x73,
			0xc9,0x45,0xf4,0x27,		
			0x36,0x9e,0xab,0xc3,
			0x84,0xab,0x84,0x8b,
			0xc5,0xeb,0x88,0x3a,		
			0xa8,0x98,0x77,0xc9,				
			0x7a,0x91,0xae,0x77,
			0xb6,0x0f,0x20,0x56,
			0x83,0x20,0x1b,0xbf,
			0x1b,0x68,0x3c,0x44,
			0x4b,0xf2,0xa0,0xdf,		
			0xee,0x18,0x55,0xf4,						
			0xbb,0xad,0x06,0x60,
			0x61,0xe4,0xdc,0x44,
		};

		/// <summary>
		/// セーブデータサイズ暗号化
		/// </summary>
		private static readonly uint SIZE_CRYPTO = 0xbbad0660;
		
		/// <summary>
		/// セーブデータバージョン
		/// </summary>
		public static uint VERSION = 0x01010101;


        #endregion

        #region 公開クラス関数


        /// <summary>
        /// セーブデータ保存
        /// </summary>
        /// <param name="cryptographicKey">暗号化キー　Null or Empty の場合、デフォルト（MAGIC）が使われます</param>
        public static void SaveFullPath<T>(
			out EError error,
			string fullPath,
			T obj,
            string cryptographicKey
            )
		{
			error = EError.Unknown;
			
			try
			{
				Debug.Log("SaveData Save = " + fullPath);
				
				// xml化			
				byte[] data = null;				
				using(var ms = new System.IO.MemoryStream(128*1024)){
					using(var strm = new System.IO.StreamWriter(ms,System.Text.Encoding.UTF8)){
						
						System.Xml.Serialization.XmlSerializer xml = new System.Xml.Serialization.XmlSerializer(typeof(T));	
						xml.Serialize(strm,obj);
						strm.Close();											
						// ms.Close();
						data = ms.ToArray();					
					}					
				}
				
				int size = data.Length;
				Debug.Log(string.Format("SaveData Size = {0}B", size));
				
				// ハッシュ値
				MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider();
				byte[] hash = crypto.ComputeHash(data);			
				
				// 簡易暗号化
				if ( string.IsNullOrEmpty(cryptographicKey) ) {
					for(int i = 0; i < data.Length; ++i){
						data[i] ^= MAGIC[i % MAGIC.Length];
					}
				}
				else {
					byte[] magic = System.Text.Encoding.UTF8.GetBytes(cryptographicKey);
					for(int i = 0; i < data.Length; ++i){
						data[i] ^= magic[i % magic.Length];
					}
				}
				
				// 保存												
				using(System.IO.BinaryWriter write = new System.IO.BinaryWriter(System.IO.File.OpenWrite(fullPath)) ){
					
					// セーブデータバージョン
					write.Write(VERSION);
					
					// データサイズ
					write.Write(size);						
					
					// 暗号化したサイズ
					uint cryptoSize = (uint)size;
					cryptoSize ^= SIZE_CRYPTO;
					write.Write(cryptoSize);				
					
					// MD5サイズ
					write.Write(hash.Length);
					
					// MD5
					write.Write(hash);
					
					// データ
					write.Write(data);			
				}
				
				Debug.Log("SaveData Save Complete!!");
				error = EError.Succeed;
				
			}catch(System.IO.IOException e){
				
				Debug.Log("SaveData Save IOException");
				Debug.LogError(e);			
				
			}catch(System.Exception e){
				
				Debug.Log("SaveData Save Exception");
				Debug.LogError(e);
			}
		}
		public static void Save<T>(
			out EError error,
			string fname,
			T obj
			)
		{
			string fullPath = ROOT_PATH + "/" + fname;
			SaveFullPath<T>( out error, fullPath, obj, null );
		}
		public static void SaveToInternal<T>(
			out EError error,
			string fname,
			T obj
			)
		{
			string fullPath = ROOT_PATH_INTERNAL + "/" + fname;
			SaveFullPath<T>( out error, fullPath, obj, null );
		}


        /// <summary>
        /// セーブデータ読み込み
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cryptographicKey">暗号化キー　Null or Empty の場合、デフォルト（MAGIC）が使われます</param>
        public static T LoadFullPath<T>(
			out EError error,
			string fullPath,
            string cryptographicKey
            ) where T : class
		{
			T ret = null;
			error = EError.Unknown;
			
			try
			{
				Debug.Log("SaveData Load = " + fullPath);
				
				int size = 0;
				int sumSize = 0;
				byte[] sum = null;			
				byte[] data = null;
				
				if( !System.IO.File.Exists(fullPath) ){
					throw new System.IO.FileNotFoundException();
				}
				
				using(System.IO.BinaryReader reader = new System.IO.BinaryReader(System.IO.File.OpenRead(fullPath)) ){
					
					uint version = reader.ReadUInt32();
					Debug.Log("SaveData Version = " + version);
					if( VERSION != version ){
						throw new SaveDataVersionMissMatchException();
					}
					
					// ファイルサイズ
					size = reader.ReadInt32();
					Debug.Log("SaveData Load Size = " + size);
					Debug.Log("SaveData Load FileSize = " + reader.BaseStream.Length);
					
					// 破損			
					if( reader.BaseStream.Length < size ){
						throw new SaveDataBrokenException("ファイルサイズがおかしい");			
					}
					
					// 暗号化したサイズ
					uint crpytoSize = reader.ReadUInt32();
					crpytoSize ^= SIZE_CRYPTO;
					if( crpytoSize != (uint)size ){
						throw new SaveDataBrokenException("ファイルサイズが一致しない");			
					}
					
					// チェックサムサイズ
					sumSize = reader.ReadInt32();
					Debug.Log("SaveData Load SumSize = " + sumSize);
					if( 16 != sumSize ){
						throw new SaveDataBrokenException("チェックサムサイズが一致しない");
					}
					
					// チェックサム
					sum = reader.ReadBytes(sumSize);
					
					// データ
					data = reader.ReadBytes(size);													
				}
				
				// 復号化
				if ( string.IsNullOrEmpty(cryptographicKey) ) {
					for (int i = 0; i < data.Length; ++i) {
						data[i] ^= MAGIC[i % MAGIC.Length];
					}
				}
				else {
					byte[] magic = System.Text.Encoding.UTF8.GetBytes(cryptographicKey);
					for(int i = 0; i < data.Length; ++i){
						data[i] ^= magic[i % magic.Length];
					}
				}
				
				// 破損チェック
				MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider();
				byte[] hash = crypto.ComputeHash(data);
				
				if( hash.Length != sumSize ){
					throw new SaveDataBrokenException("ハッシュのサイズが一致しない");							
				}
				
				for(int i = 0; i < sumSize; ++i){
					if( hash[i] != sum[i] ){
						throw new SaveDataBrokenException("ハッシュが一致しない");			
					}
				}
				
				using (var ms = new System.IO.MemoryStream(data)) {
					using(var strm = new System.IO.StreamReader(ms,System.Text.Encoding.UTF8)){					
						System.Xml.Serialization.XmlSerializer xml = new System.Xml.Serialization.XmlSerializer(typeof(T));
						ret = xml.Deserialize(strm) as T;
					}
				}
				
				if( null == ret ){
					throw new SaveDataBrokenException("xmlの復元に失敗");
				}
				
				Debug.Log("SaveData Load Complete!");
				error = EError.Succeed;
				
			} catch (SaveDataVersionMissMatchException e) {
				
				Debug.Log("SaveData VersionMissMatch");
				Debug.LogWarning(e.Message); // 停止させないため LogWarning。これが返えったら、対応処理を必要とする
				error = EError.VersionMissMatch;
				
			}catch( SaveDataBrokenException e ){
				
				Debug.Log("SaveData Broken");
				Debug.LogWarning(e.Message); // 停止させないため LogWarning。これが返えったら、対応処理を必要とする
				error = EError.Broken;
				
			} catch (System.IO.FileNotFoundException e ){
				
				Debug.Log("System.IO.FileNotFoundException");
				Debug.LogWarning(e.Message); // 停止させないため LogWarning。これが返えったら、対応処理を必要とする
				error = EError.FileNotFound;
				
			}catch (System.IO.IOException e) {
				
				Debug.Log("SaveData Load IOException");
				Debug.LogError(e); // 謎のエラーなので停止
				error = EError.Unknown;			
				
			}catch(System.Exception e){
				
				Debug.Log("SaveData Load Exception");
				Debug.LogError(e); // 謎のエラーなので停止
				error = EError.Unknown;
			}
			
			return ret;
			
		}
		public static T Load<T>(
			out EError error,
			string fname
			) where T : class
		{
			string fullPath = ROOT_PATH + "/" + fname;
			return LoadFullPath<T>( out error, fullPath, null );
		}		
		public static T LoadFromInternal<T>(
			out EError error,
			string fname
			) where T : class
		{
			string fullPath = ROOT_PATH_INTERNAL + "/" + fname;
			return LoadFullPath<T>( out error, fullPath, null );
		}

        #endregion















        #region XML

        /// <summary>
        /// セーブデータをXMLで保存
        /// </summary>
        public static void SaveXmlFullPath<T>(
			out EError error,
			string fullPath,
			T obj
            )
        {
			error = EError.Unknown;
			
			try
			{
				Debug.Log("SaveData SaveXml = " + fullPath);
				
				using(System.IO.StreamWriter fs = new System.IO.StreamWriter(fullPath)){			
					System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(T));	
					writer.Serialize(fs, obj);
					fs.Close();
					Debug.Log("DEBUG : "+fullPath);
				}

				Debug.Log("SaveData SaveXml Complete!!");
				error = EError.Succeed;
					
			}catch(System.IO.IOException e){
				
				Debug.Log("SaveData SaveXml IOException");
				Debug.LogError(e);			
				
			}catch(System.Exception e){
				
				Debug.Log("SaveData SaveXml Exception");
				Debug.LogError(e);
			}
		}


        /// <summary>
        /// セーブデータをXMLで読み込み
        /// </summary>
        public static T LoadXmlFullPath<T>(
			out EError error,
			string fullPath
			) where T : class
		{
			T ret = null;
			error = EError.Unknown;
			
			try
			{
				Debug.Log("SaveData LoadXml = " + fullPath);


				if( !System.IO.File.Exists(fullPath) ){
					throw new System.IO.FileNotFoundException();
				}
				
				using(var strm = new System.IO.StreamReader(fullPath)){
					System.Xml.Serialization.XmlSerializer xml = new System.Xml.Serialization.XmlSerializer(typeof(T));
					ret = xml.Deserialize(strm) as T;
				}

				if( null == ret ){
					throw new SaveDataBrokenException("xmlの復元に失敗");
				}
				
				Debug.Log("SaveData LoadXml Complete!");
				error = EError.Succeed;
				
			} catch (SaveDataVersionMissMatchException e) {
				
				Debug.Log("SaveData VersionMissMatch");
				Debug.Log(e.Message);
				error = EError.VersionMissMatch;
				
			}catch( SaveDataBrokenException e ){
				
				Debug.Log("SaveData Broken");
				Debug.Log(e.Message);
				error = EError.Broken;
				
			} catch (System.IO.FileNotFoundException e ){
				
				Debug.Log("System.IO.FileNotFoundException");
				Debug.Log(e.Message);
				error = EError.FileNotFound;
				
			}catch (System.IO.IOException e) {
				
				Debug.Log("SaveData LoadXml IOException");
				Debug.LogError(e);
				error = EError.Unknown;			
				
			}catch(System.Exception e){
				
				Debug.Log("SaveData LoadXml Exception");
				Debug.LogError(e);
				error = EError.Unknown;
			}

			return ret;
		}
		
	}

	#endregion
	
}