﻿using UnityEngine.Events;
using System;
using System.Collections;



namespace ATK.Utilities
{

	[System.Serializable]
	public class NotificationObject<T>
	{
		public delegate void NotificationCallback (T t);

		public UnityAction<T> Event;
		
		private T _data;
		
		public NotificationObject (T t)
		{
			Value = t;
		}
		
		public NotificationObject ()
		{
		}
		
		~NotificationObject ()
		{ 
			Dispose ();
		}
		
		public T Value {
			get {
				return _data;
			}
			set {
				_data = value;
				notice ();
			}
		}

		public void Dispose ()
		{
			Event = null;
		}
		
		private void notice ()
		{
			if (Event != null)
				Event (_data);
		}
	}
}

/*



==================================================
使用例
==================================================
参考：http://tsubakit1.hateblo.jp/entry/20140308/1394213883

・GameControllerクラス(singleton)
public NotificationObject<int> Score = new NotificationObject<int>(0);


・ラベルを持つクラス
void Start() 
{
	// 変更通知の登録
	GameController.Instance.Score.Event += ChangeScore;
}

void ChangeScore(int score)
{
	// label1の変更処理
}

 */