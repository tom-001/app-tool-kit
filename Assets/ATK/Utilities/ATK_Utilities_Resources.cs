﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ATK.Utilities
{

	/// <summary>
	/// リソース読み込みお助け
	/// </summary>
	public static class Resources
	{
	#region 公開メンバ関数
	
		/// <summary>
		/// リソース読み込み
		/// </summary>
		/// <typeparam name="TValue">型</typeparam>
		/// <param name="fullPath">ファイルパス</param>
		/// <returns>読み込んだデータの配列</returns>
		public static Dictionary<string, TValue> LoadAll<TValue>(string fullPath) where TValue : UnityEngine.Object
		{
			Dictionary<string, TValue> ret = new Dictionary<string, TValue>();

			UnityEngine.Object[] objects = UnityEngine.Resources.LoadAll(fullPath, typeof(TValue));				
			foreach(UnityEngine.Object p in objects){
				TValue value = p as TValue;
				string key = p.name;
				ret.Add(key,value);
			}
			
			if( 0 == ret.Count ){
				return null;
			}					
			return ret;
		}

		/// <summary>
		/// リソース読み込み
		/// </summary>
		/// <typeparam name="TValue">型</typeparam>
		/// <param name="fullPath">ファイルパス</param>		
		public static TValue Load<TValue>(string fullPath) where TValue : UnityEngine.Object
		{			
			UnityEngine.Object[] objects = UnityEngine.Resources.LoadAll(fullPath, typeof(TValue));	
			if( (null == objects) || (0 == objects.Length) ){
				return null;
			}
			
			if( 2 <= objects.Length ){			
				for(int i = 0; i < objects.Length; ++i){
					// warning
					Debug.LogWarning("ファイルが複数存在します" + objects[i].name);
				}
			}
			
			return objects[0] as TValue;
		}

		
		
	#endregion
	
	}
	

}
