﻿//using UnityEngine;
using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

/// 参照：http://dobon.net/vb/dotnet/file/xmlserializerhashtable.html


namespace ATK.Utilities
{

	/// <summary>
	/// XMLシリアル化ができるHashtableクラス
	/// </summary>
	public class SerializableHashtable : Hashtable, IXmlSerializable
	{
		public SerializableHashtable() {}
		public SerializableHashtable(int capacity) : base(capacity) {}

	    public System.Xml.Schema.XmlSchema GetSchema()
	    {
	        return null;
	    }

	    public void ReadXml(XmlReader reader)
	    {
	        bool wasEmpty = reader.IsEmptyElement;
	        reader.Read();
	        if (wasEmpty)
	            return;

	        XmlSerializer keySerializer;
	        XmlSerializer valueSerializer;

	        while (reader.NodeType != XmlNodeType.EndElement)
	        {
	            reader.ReadStartElement("KeyValuePair");

	            //キーを復元する
	            //キーの型を取得する
	            Type keyType = Type.GetType(reader.GetAttribute("Type"));
	            reader.ReadStartElement("Key");
	            //キーの値を取得する
	            keySerializer = new XmlSerializer(keyType);
	            object key = keySerializer.Deserialize(reader);
	            reader.ReadEndElement();

	            //値を復元する
	            //値の型を取得する
	#if true
				string typename = reader.GetAttribute("Type");
	            Type valueType = Type.GetType(typename);
				if( valueType == null ) valueType = Type.GetType(typename+", UnityEngine");
	#else
	      		      Type valueType = Type.GetType(reader.GetAttribute("Type"));
	#endif
	            reader.ReadStartElement("Value");
	            //値の値を取得する
	            valueSerializer = new XmlSerializer(valueType);
	            object val = valueSerializer.Deserialize(reader);
	            reader.ReadEndElement();

	            reader.ReadEndElement();

	            //コレクションに追加する
	            this.Add(key, val);

	            //次へ
	            reader.MoveToContent();
	        }

	        reader.ReadEndElement();
	    }

	    public void WriteXml(XmlWriter writer)
	    {
	        XmlSerializer keySerializer;
	        XmlSerializer valueSerializer;

	        foreach (object key in this.Keys)
	        {
	            writer.WriteStartElement("KeyValuePair");

	            //キーを書き込む
	            writer.WriteStartElement("Key");
	            //キーの型を書き込む
	            Type keyType = key.GetType();
	            
	            writer.WriteAttributeString("Type", keyType.FullName);
	            //キーをシリアル化して書き込む
	            keySerializer = new XmlSerializer(keyType);
	            keySerializer.Serialize(writer, key);
	            writer.WriteEndElement();

	            //値を書き込む
	            writer.WriteStartElement("Value");
	            //値の型を書き込む
	            object val = this[key];
	            Type valueType = val.GetType();

				ATK.Assert.AreNotEqual(valueType.FullName, "UnityEngine.Transform", "UnityEngine.Transformは記録できません" );
//				if ( valueType.FullName == "UnityEngine.Transform" ) { Debug.LogError( ((Transform)val).name ); }
				
	            writer.WriteAttributeString("Type", valueType.FullName);
	            //値をシリアル化して書き込む
	            valueSerializer = new XmlSerializer(valueType);
	            valueSerializer.Serialize(writer, val);
	            writer.WriteEndElement();

	            writer.WriteEndElement();
	        }
	    }

		/// <summary>
		/// SerializableHashtable => Hashtable
		/// </summary>
		/// <returns>Hashtable</returns>
		public Hashtable ToHashtable()
		{
			Hashtable newTable = new Hashtable(this.Count);

			foreach (object key in this.Keys)
			{
				newTable.Add( key, this[key] );
			}
			return newTable;
		}

		/// <summary>
		/// Hashtable => SerializableHashtable
		/// </summary>
		/// <returns>SerializableHashtable</returns>
		/// <param name="table">Hashtable</param>
		public static SerializableHashtable HashtableToSerializableHashtable( Hashtable table )
		{
			SerializableHashtable newTable = new SerializableHashtable(table.Count);
			
			foreach (object key in table.Keys)
			{
				newTable.Add( key, table[key] );
			}
			return newTable;
		}
		
		/// <summary>
		/// SerializableHashtable => Hashtable
		/// </summary>
		/// <returns>Hashtable</returns>
		/// <param name="table">SerializableHashtable</param>
		public static Hashtable SerializableHashtableToHashtable( SerializableHashtable table )
		{
			Hashtable newTable = new Hashtable(table.Count);
			
			foreach (object key in table.Keys)
			{
				newTable.Add( key, table[key] );
			}
			return newTable;
		}	


		/// <summary>
		/// コレクションを作成
		/// </summary>
		/// <returns>コレクション</returns>
		/// <param name="args">"key", value</param>
		/// SerializableHashtable.CreateCollection( "key1", val1, "key2", val2, ... )
		public static SerializableHashtable CreateCollection( params object[] args )
		{
			if (args.Length == 0 || args.Length %2 != 0) {
				ATK.Assert.Fail("コレクションの要素数が不正です Length = " + args.Length );
				return null;
			}

			SerializableHashtable collection = new SerializableHashtable(args.Length/2);
			
			int i = 0;
			while ( i < args.Length - 1 )  
			{
				collection.Add(args[i], args[i+1]);
				i += 2;
			}
			return collection;
		}

	}

}


/*

==================================================
SerializableHashtable を作成
==================================================
		
SerializableHashtable stable = SerializableHashtable.CreateCollection(
	KEY_POSITION, Vector3.one,
	KEY_ROTATION, Quaternion.identity
	);



==================================================
Hashtable <==>  SerializableHashtable 変換例
==================================================

SerializableHashtable stable = SerializableHashtable.HashtableToSerializableHashtable( table );



==================================================
SerializableHashtable <==>  Hashtable 変換例
==================================================

Hashtable table = stable.ToHashtable();

Hashtable table = SerializableHashtable.SerializableHashtableToHashtable( stable );





*/