﻿//using UnityEngine;
using System;
//using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

/// 参照：http://dobon.net/vb/dotnet/file/xmlserializerhashtable.html


namespace ATK.Utilities
{

	/// <summary>
	/// XMLシリアル化ができるDictionaryクラス
	/// </summary>
	/// <typeparam name="TKey">キーの型</typeparam>
	/// <typeparam name="TValue">値の型</typeparam>
	public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
	{
		public SerializableDictionary() : base() {}
		public SerializableDictionary(int capacity) : base(capacity) {}

	    //nullを返す
	    public System.Xml.Schema.XmlSchema GetSchema()
	    {
	        return null;
	    }


		public void ReadXml(XmlReader reader)
		{
			bool wasEmpty = reader.IsEmptyElement;
			reader.Read();
			if (wasEmpty)
				return;
			
			XmlSerializer keySerializer;
			XmlSerializer valueSerializer;
			
			while (reader.NodeType != XmlNodeType.EndElement)
			{
				reader.ReadStartElement("KeyValuePair");
				
				//キーを復元する
				//キーの型を取得する
				Type keyType = Type.GetType(reader.GetAttribute("Type"));
				reader.ReadStartElement("Key");
				//キーの値を取得する
				keySerializer = new XmlSerializer(keyType);
				object key = keySerializer.Deserialize(reader);
				reader.ReadEndElement();
				
				//値を復元する
				//値の型を取得する
				#if true
				string typename = reader.GetAttribute("Type");
				Type valueType = Type.GetType(typename);
				if( valueType == null ) valueType = Type.GetType(typename+", UnityEngine");
				#else
				Type valueType = Type.GetType(reader.GetAttribute("Type"));
				#endif
				reader.ReadStartElement("Value");
				//値の値を取得する
				valueSerializer = new XmlSerializer(valueType);
				object val = valueSerializer.Deserialize(reader);
				reader.ReadEndElement();
				
				reader.ReadEndElement();
				
				//コレクションに追加する
				this.Add((TKey)key, (TValue)val);
				
				//次へ
				reader.MoveToContent();
			}
			
			reader.ReadEndElement();
		}
		
		public void WriteXml(XmlWriter writer)
		{
			XmlSerializer keySerializer;
			XmlSerializer valueSerializer;
			
			foreach (TKey key in this.Keys)
			{
				writer.WriteStartElement("KeyValuePair");
				
				//キーを書き込む
				writer.WriteStartElement("Key");
				//キーの型を書き込む
				Type keyType = key.GetType();
				
				writer.WriteAttributeString("Type", keyType.FullName);
				//キーをシリアル化して書き込む
				keySerializer = new XmlSerializer(keyType);
				keySerializer.Serialize(writer, key);
				writer.WriteEndElement();
				
				//値を書き込む
				writer.WriteStartElement("Value");
				//値の型を書き込む
				TValue val = this[key];
				Type valueType = val.GetType();
				
				ATK.Assert.AreNotEqual(valueType.FullName, "UnityEngine.Transform", "UnityEngine.Transformは記録できません" );
				//				if ( valueType.FullName == "UnityEngine.Transform" ) { Debug.LogError( ((Transform)val).name ); }
				
				writer.WriteAttributeString("Type", valueType.FullName);
				//値をシリアル化して書き込む
				valueSerializer = new XmlSerializer(valueType);
				valueSerializer.Serialize(writer, val);
				writer.WriteEndElement();
				
				writer.WriteEndElement();
			}
		}
/*
エラーになる
	    /// XMLを読み込んで復元する
	    public void ReadXml(XmlReader reader)
	    {
	        bool wasEmpty = reader.IsEmptyElement;
	        reader.Read();
	        if (wasEmpty)
	            return;

	        //XmlSerializerを用意する
	        XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
	        XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

	        while (reader.NodeType != XmlNodeType.EndElement)
	        {
	            reader.ReadStartElement("KeyValuePair");

	            //キーを逆シリアル化する
	            reader.ReadStartElement("Key");
	            TKey key = (TKey)keySerializer.Deserialize(reader);
	            reader.ReadEndElement();

	            //値を逆シリアル化する
	            reader.ReadStartElement("Value");
	            TValue val = (TValue)valueSerializer.Deserialize(reader);
	            reader.ReadEndElement();

	            reader.ReadEndElement();

	            //コレクションに追加する
	            this.Add(key, val);

	            //次へ
	            reader.MoveToContent();
	        }

	        reader.ReadEndElement();
	    }

	    /// 現在の内容をXMLに書き込む
	    public void WriteXml(XmlWriter writer)
	    {
	        //XmlSerializerを用意する
	        XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
	        XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

	        foreach (TKey key in this.Keys)
	        {
	            writer.WriteStartElement("KeyValuePair");

	            //キーを書き込む
	            writer.WriteStartElement("Key");
	            keySerializer.Serialize(writer, key);
	            writer.WriteEndElement();

	            //値を書き込む
	            writer.WriteStartElement("Value");
	            valueSerializer.Serialize(writer, this[key]);
	            writer.WriteEndElement();

	            writer.WriteEndElement();
	        }
	    }
*/

		public Dictionary<TKey, TValue> ToDictionary()
		{
			Dictionary<TKey, TValue> newDic = new Dictionary<TKey, TValue>(this.Count);

			foreach (TKey key in this.Keys)
			{
				newDic.Add( key, this[key] );
			}
			return newDic;
		}
		
		/// <summary>
		/// Dictionary => SerializableDictionary
		/// </summary>
		/// <returns>SerializableDictionary</returns>
		/// <param name="table">Dictionary</param>
		public static SerializableDictionary<TKey, TValue> DictionaryToSerializableDictionary( Dictionary<TKey, TValue> dic )
		{
			SerializableDictionary<TKey, TValue> newDic = new SerializableDictionary<TKey, TValue>(dic.Count);
			
			foreach (TKey key in dic.Keys)
			{
				newDic.Add( key, dic[key] );
			}
			return newDic;
		}
		
		/// <summary>
		/// SerializableDictionary => Dictionary
		/// </summary>
		/// <returns>Dictionary</returns>
		/// <param name="table">SerializableDictionary</param>
		public static Dictionary<TKey, TValue> SerializableDictionaryToDictionary( SerializableDictionary<TKey, TValue> dic )
		{
			Dictionary<TKey, TValue> newDic = new Dictionary<TKey, TValue>(dic.Count);
			
			foreach (TKey key in dic.Keys)
			{
				newDic.Add( key, dic[key] );
			}
			return newDic;
		}	

		
		/// <summary>
		/// コレクションを作成
		/// </summary>
		/// <returns>コレクション</returns>
		/// <param name="args">"key", value</param>
		/// SerializableDictionary.CreateCollection( "key1", val1, "key2", val2, ... )
		public static SerializableDictionary<TKey, TValue> CreateCollection( params object[] args )
		{
			if (args.Length == 0 || args.Length %2 != 0) {
				ATK.Assert.Fail("コレクションの要素数が不正です Length = " + args.Length );
				return null;
			}

			SerializableDictionary<TKey, TValue> collection = new SerializableDictionary<TKey, TValue>(args.Length/2);

			int i = 0;
			while ( i < args.Length - 1 )  
			{
				collection.Add((TKey)args[i], (TValue)args[i+1]);
				i += 2;
			}
			return collection;
		}
	}

}



/*



==================================================
SerializableDictionary を作成
==================================================

SerializableDictionary<string, object> sdic = SerializableDictionary<string, object>.CreateCollection(
	KEY_POSITION, Vector3.one,
	KEY_ROTATION, Quaternion.identity
	);
	


==================================================
Dictionary <==>  SerializableDictionary 変換例
==================================================

SerializableDictionary<string, object> sdic = SerializableDictionary<string, object>.DictionaryToSerializableDictionary( dic );





==================================================
SerializableDictionary <==>  Dictionary 変換例
==================================================

Dictionary<string, object> dic = sdic.ToDictionary();

Dictionary<string, object> dic = SerializableDictionary<string, object>.SerializableDictionaryToDictionary( sdic );




*/
