﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;



namespace ATK.UI
{
	public class ATK_UI_DialogBox : MonoBehaviour
	{

		/// <summary>
		/// 時に呼ばれるコールバック
		/// 引数なしのデリゲート
		/// </summary>
		public delegate void OnCloseCallback(Result result);
//		public event OnCloseCallback OnCloseEvent;
//		public delegate void OnCloseCallback();

		/// <summary>
		/// </summary>
		public UnityAction OnOpenEvent;
		public UnityAction<Result> OnCloseEvent;

		private Result _result = Result.None;

		public enum Buttons {
			Ok,
			OkCancel,
			AbortRetryIgnore,
			YesNo,
			YesNoCancel,
			RetryCancel,
		}

		public enum Result {
			None,
			Ok,
			Cancel,
			Abort,
			Retry,
			Ignore,
			Yes,
			No,
		}

		public void Show()
		{
			onOpen();
			Debug.Log("Show");
			onClose();
		}

		private void onOpen()
		{
            if (!object.ReferenceEquals(OnOpenEvent, null))
            {
				OnOpenEvent();
				OnOpenEvent = null;
			}
		}

		private void onClose()
		{
            if (!object.ReferenceEquals(OnCloseEvent, null))
            { 
				OnCloseEvent(_result);
				OnCloseEvent = null;
			}
			Destroy(this.gameObject);
		}

	}
}
