﻿using System;
using System.Collections;
//using System.Collections.Generic;




namespace ATK.Extension
{

	/// <summary>
	/// Hashtable 拡張
	/// </summary>
	public static class HashtableExtension
	{
		/// <summary>
		/// ハッシュテーブルを作成します。キーと値のペアを連続して追加します。
		/// </summary>
		/// <returns>コレクション</returns>
		/// <param name="args">キー, 値</param>
		/// HashtableExtension.CreateCollection( "key1", val1, "key2", val2, ... )
		public static Hashtable CreateCollection( params object[] args )
		{
			Hashtable collection = new Hashtable(args.Length/2);
			
			if (args.Length %2 != 0) 
			{
				ATK.Assert.AreEqual(args.Length %2, 0, "コレクションの要素数が偶数でない");
				return null;
			}

			int i = 0;
			while ( i < args.Length - 1 )  
			{
				collection.Add(args[i], args[i+1]);
				i += 2;
			}
			return collection;
		}

		/// <summary>
		/// Hashtable を SerializableHashtable に変換します。
		/// </summary>
		/// <returns>serializable hashtable.</returns>
		public static ATK.Utilities.SerializableHashtable ToSerializableHashtable(this Hashtable self )
		{
			ATK.Utilities.SerializableHashtable newTable = new ATK.Utilities.SerializableHashtable(self.Count);
			
			foreach (object key in self.Keys)
			{
				newTable.Add( key, self[key] );
			}
			return newTable;
		}


		/// <summary>
		/// 指定したキーに関連付けられている値を取得します。
		/// </summary>
		/// <returns>コレクションに格納されている場合はtrue。それ以外はfalse</returns>
		/// <param name="key">取得する値のキー</param>
		/// <param name="value">キーがあるなら、関連付けされた値が格納。それ以外は型の規定値</param>
		public static bool TryGetValue<TKey, TValue>(
			this Hashtable self,
			TKey key,
			out TValue value
			)
		{
			bool ret = false;
			value = default(TValue);

			if (self.ContainsKey(key)) {
				value = (TValue)self[key];
				ret = true;
			}

			return ret;
		}

		/// <summary>
		/// 指定したキーに関連付けられている値を取得します。
		/// </summary>
		/// <returns>コレクションに格納されている場合は、関連付けされた値。それ以外は型の規定値</returns>
		/// <param name="key">取得する値のキー</param>
		public static TValue GetValueOrDefault<TKey, TValue>(
			this Hashtable self,
			TKey key
			) 
		{
			if (self.ContainsKey(key)) {
				return (TValue)self[key];
			}
			
			return default(TValue);
		}
	}

}




/*



==================================================
Hashtable を作成
==================================================

Hashtable table = HashtableExtension.CreateCollection(
	KEY_POSITION, Vector3.one,
	KEY_ROTATION, Quaternion.identity
	);



==================================================
TryGetValue 使用例
==================================================

string KEY_POSITION		 = "position";
string KEY_ROTATION		 = "rotation";

// コレクション作成
Hashtable table = HashtableExtension.CreateCollection(
		KEY_POSITION, Vector3.one,
		KEY_ROTATION, Quaternion.identity
	);

Vector3 pos;

// 指定したキーの値取得①
if (table.TryGetValue<string, Vector3>(KEY_POSITION, out pos)) {
	Debug.Log("成功");
}

// 指定したキーの値取得②
pos = _hogeData.Arguments.GetValueOrDefault<string, Vector3>(KEY_POSITION);







*/