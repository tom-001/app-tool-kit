﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace ATK.Extension
{
	/// <summary>
	/// Dictionary 拡張
	/// </summary>
	public static class DictionaryExtension
	{

		/// <summary>
		/// 辞書を作成します。キーと値のペアを連続して追加します。
		/// </summary>
		/// <returns>コレクション</returns>
		/// <param name="args">キー, 値</param>
		/// DictionaryExtension.CreateCollection( "key1", val1, "key2", val2, ... )
		public static Dictionary<TKey, TValue> CreateCollection<TKey, TValue>( params object[] args )
		{
			Dictionary<TKey, TValue> collection = new Dictionary<TKey, TValue>();
			
			if (args.Length %2 != 0) 
			{
				ATK.Assert.AreEqual(args.Length %2, 0, "コレクションの要素数が偶数でない");
				return null;
			}

			int i = 0;
			while ( i < args.Length - 1 )  
			{
				collection.Add((TKey)args[i], (TValue)args[i+1]);
				i += 2;
			}
			return collection;
		}



		/// <summary>
		/// 指定したキーに関連付けられている値を取得します。
		/// </summary>
		/// <returns>コレクションに格納されている場合は、関連付けされた値。それ以外は型の規定値</returns>
		/// <param name="key">取得する値のキー</param>
		public static TValue GetValueOrDefault<TKey, TValue>(
			this IDictionary<TKey, TValue> self,
			TKey key
			) 
		{
			TValue value;
			if (self.TryGetValue(key, out value))
				return value;
			
			return default(TValue);
		}

	}

}





/*




==================================================
Dictionary を作成
==================================================

Dictionary<string, object> dic = DictionaryExtension.CreateCollection<string, object>(
	KEY_POSITION, Vector3.one,
	KEY_ROTATION, Quaternion.identity
	);




==================================================
TryGetValue 使用例
==================================================

string KEY_POSITION		 = "position";
string KEY_ROTATION		 = "rotation";

// コレクション作成　※objectで作成すると、扱うときにキャストが必要
dictionary = DictionaryExtension<string, object>.CreateCollection(
		KEY_POSITION, Vector3.one,
		KEY_ROTATION, Quaternion.identity
	);

Vector3 pos;


// 指定したキーの値取得①　既存メソッドの使用
// 要素がobject型の場合、既存のメソッドでは扱いにくい
object obj;
if (dictionary.TryGetValue(KEY_POSITION, out obj)) {
	pos = (Vector3)obj;
}

// 指定したキーの値取得②　追加メソッドの使用
// 要素がobject型の場合、こちらの方が使いやすいかも
pos = (Vector3)dictionary.GetValueOrDefault<string, Vector3>(KEY_POSITION);




*/
