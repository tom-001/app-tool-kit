﻿using UnityEngine;
using System.Collections;


namespace AEK.Extension
{

	public static class OtherExtension
	{
		/// <summary>
		/// Sets the speed.
		/// </summary>
		/// <param name="anim">Animation.</param>
		/// <param name="speed">Speed.</param>
		public static void SetSpeed(this Animation anim, float speed)
		{
			anim[anim.clip.name].speed = speed; 
		}
	}

}