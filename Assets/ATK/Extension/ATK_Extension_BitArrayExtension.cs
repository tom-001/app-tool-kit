﻿using System;
using System.Collections;

namespace ATK.Extension
{
	/// <summary>
	/// BitArray 拡張
	/// </summary>
	public static class BitArrayExtension
	{
        /// <summary>
        /// 全てのビットをONにする
        /// </summary>
        public static void AllOn(this BitArray array)
        {
            array.SetAll(true);
        }

        /// <summary>
        /// 全てのビットをOFFにする
        /// </summary>
        public static void AllOff(this BitArray array)
        {
            array.SetAll(false);
        }

        /// <summary>
        /// いずれかのビットがONかどうか
        /// </summary>
        public static bool Any(this BitArray array)
		{
			foreach( bool bit in array ) {
				if (bit) return true;
			}
			return false;
		}

		/// <summary>
		/// 全てのビットがONかどうか
		/// </summary>
		public static bool All(this BitArray array)
		{
			foreach( bool bit in array ) {
				if (!bit) return false;
			}
			return true;
		}

		/// <summary>
		/// 全てのビットがOFFかどうか
		/// </summary>
		public static bool None(this BitArray array)
		{
			return !Any (array);
		}

		/// <summary>
		/// 指定したビットを反転する
		/// </summary>
		/// <param name="index">Index.</param>
		public static void Flip(this BitArray array, int index)
		{
			array.Set(index, !array.Get(index));
		}

        /// <summary>
        /// バイト配列に変換 8bit -> 1byte
        /// </summary>
        /// <returns></returns>
        public static byte[] ToByteArray(this BitArray array)
        {
            byte[] ret = new byte[(array.Length - 1) / 8 + 1];
            array.CopyTo(ret, 0);
            return ret;
        }

        /// <summary>
        /// bool配列に変換 1bit -> 1bool
        /// </summary>
        /// <returns></returns>
        public static bool[] ToBoolArray(this BitArray array)
        {
            bool[] bools = new bool[array.Length];

            for (int i = 0; i < array.Length; i++)
            {
                bools[i] = array[i] ? true : false;
            }

            return bools;
        }
        

    }

}

