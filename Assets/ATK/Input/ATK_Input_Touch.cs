﻿using UnityEngine;
using System.Collections;
using ATK;

/// <summary>
/// 簡単なタッチ検出
/// </summary>
public class ATK_Input_Touch : MonoBehaviour 
{

#region 非公開クラス定数
	static private readonly Vector3 INVALID = new Vector3(Mathf.Infinity,Mathf.Infinity,Mathf.Infinity);
#endregion	

#region 非公開メンバ変数

	private Ray ray_;
	private Vector3 _currentWorldPosition;
	private Vector3 _currentPosition;
	private Vector3 _prevPosition;	
			
	
	private bool _isEnabled = true;
	private bool _isPressed = false;
	private bool _isDown = false;			
	private bool _prevPressed = false;
		
	[SerializeField]
	private Camera _camera = null;
			
#endregion

#region プロパティ

	

	public Vector3 CurrentWorldPosition
	{
		get { return _currentWorldPosition; }
	}
	
	/// <summary>
	/// 押された瞬間
	/// </summary>
	public bool IsDown
	{ 
		get { return _isDown; } 
	}

	/// <summary>
	/// 押されているか
	/// </summary>
	public bool IsPressed
	{
		get { return _isPressed; }
	}

	public bool IsEnabled 
	{
		set { 
			_isEnabled = value;			
			_currentPosition = INVALID;
			_currentWorldPosition = INVALID;
			_prevPosition = INVALID;
			_isPressed = false;
			_isDown = false;
			_prevPressed = false;					
		}
	}

#endregion
	
#region 公開メンバ関数	
	
	// Use this for initialization
	void Start () 
	{				
		IsEnabled = false;
		IsEnabled = true;		
	}
	
	// Update is called once per frame
	void Update () 
	{
		_prevPressed = _isPressed;
	
		if( !_isEnabled ){
			IsEnabled = false;
			return;
		}
	
		_prevPosition = _currentPosition;		
		
		if ( RuntimeEnvironment.Input.IsTouchEmulate ){
					
			if( Input.GetMouseButton(0) ){				
				_currentPosition = Input.mousePosition;
				_isPressed = true;				
			}else{
				_reset();
			}
			
			if( Input.GetMouseButtonDown(0) ){
				_isDown = true;
				_prevPosition = _currentPosition;
			}else{
				_isDown = false;
			}
					
		}else{
							
			if( 0 != Input.touchCount ){				
			
				UnityEngine.Touch data = Input.GetTouch(0);
								
				_currentPosition = data.position;				
				_isPressed = true;				
				
				// 押された瞬間
				if( UnityEngine.TouchPhase.Began ==  data.phase ){
					_prevPosition = _currentPosition;
					_isDown = true;
				}else{
					_isDown = false;
				}
															
			}else{			
				_reset();
			}			
		}		
		
		if( (null != _camera) && _isPressed ){		
			_currentWorldPosition = _camera.ScreenToWorldPoint(_currentPosition);			
			_currentWorldPosition.z = 0.0f;						
			ray_ = _camera.ScreenPointToRay(_currentPosition);
			
			if( null != this.GetComponent<Collider>() ){			
				RaycastHit hit = new RaycastHit();		
				if( !this.GetComponent<Collider>().Raycast(ray_,out hit,Mathf.Infinity) ){
					_reset();
				}							
			}			
		}						
	}
	
	/// <summary>
	/// ワールド座標に変換
	/// </summary>
	/// <param name="pos"></param>
	/// <returns></returns>
	public Vector3 WorldToScreenPoint(Vector3 pos)
	{
		return _camera.WorldToScreenPoint(pos);
	}
	
	/// <summary>
	/// ヒットチェック
	/// </summary>
	/// <param name="parent"></param>
	/// <returns></returns>
	public bool HitCheck(GameObject obj)
	{
		if( !_isPressed ){
			return false;
		}
		
		// objectとのhitcheckを使用する場合は
		// cameraが必要		
		Assert.IsNotNull(_camera,"cameraが設定されていない");
		
		RaycastHit hit = new RaycastHit();		
		if( obj.GetComponent<Collider>().Raycast(ray_,out hit,Mathf.Infinity) ){			
			if( obj == hit.transform.gameObject ){
				return true;
			}else{
				Debug.Log("なんかおかしい");
			}		
		}
	
		return false;
	}
#endregion

	
#region 非公開メンバ関数

	private void _reset()
	{
		_isPressed = false;
		_isDown = false;
		_currentPosition = INVALID;
		_currentWorldPosition = INVALID;
		_prevPosition = INVALID;
	}
		
#endregion
	
}

