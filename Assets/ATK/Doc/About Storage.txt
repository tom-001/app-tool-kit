﻿

////////////////////////////////////////////////////////////////////////////////////////////////////
UnityエディターAndroidの設定
////////////////////////////////////////////////////////////////////////////////////////////////////

●インストール設定
┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│File > Build Settings > Player Settings > Other Settings > Configuration > Install Location                   │
│──────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│アプリケーションをインストールするストレージ                                                                      │
├────────────────┬─────────────────────────────────────────────────────────────────────────────────────────────┤
│Automatic       │内部ストレージ優先。不足しているなら外部。インストール後、内部or外部に移動可                        │
├────────────────┼─────────────────────────────────────────────────────────────────────────────────────────────┤
│Prefer External │外部ストレージ優先。不足または存在しない場合、内部にインストール。インストール後、内部or外部に移動可能│
├────────────────┼─────────────────────────────────────────────────────────────────────────────────────────────┤
│Force Internal  │内部ストレージのみ。外部にはインストールできない。インストール後、内部or外部に移動不可               │
└────────────────┴─────────────────────────────────────────────────────────────────────────────────────────────┘
※基本的に Prefer External を設定する


●ストレージ設定
┌────────────────────────────────────────────────────────────────────────────────────────┐
│File > Build Settings > Player Settings > Other Settings > Configuration > Write Access │
├────────────────────────────────────────────────────────────────────────────────────────┤
│データを保存するストレージ                                                                │
├────────────────┬───────────────────────────────────────────────────────────────────────┤
│Internal Only   │内部ストレージに保存							                         │
├────────────────┼───────────────────────────────────────────────────────────────────────┤
│External(SDCard)│外部ストレージ(SDカード)に保存					                         │
└────────────────┴───────────────────────────────────────────────────────────────────────┘
※基本的に External(SDCard) を設定する












////////////////////////////////////////////////////////////////////////////////////////////////////
Android(Xperia Z Ultra) 実機検証
////////////////////////////////////////////////////////////////////////////////////////////////////


==================================================
UnityEngine.Application.dataPath
UnityEngine.Application.persistentDataPath
UnityEngine.Application.streamingAssetsPath
UnityEngine.Application.temporaryCachePath
==================================================

パターン①
┌────────────────────────────────────────────────────┐
│SDカードの有無 : Install Location + Write Access                                                        │
│────────────────────────────────────────────────────│
│SDカード有り   : Automatic        + Internal Only                                                       │
│SDカード有り   : Force Internal   + Internal Only                                                       │
│SDカード無し   : Automatic        + Internal Only                                                       │
│SDカード無し   : Prefer External  + Internal Only                                                       │
├──────────────────────┬─────────────────────────────┤
│UnityEngine.Application.dataPath            │/data/app/[Bundle Identifier]-2.apk                       │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.persistentDataPath  │/data/data/[Bundle Identifier]/files                      │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.streamingAssetsPath │jar:file:///data/app/[Bundle Identifier]-2.apk!/assets    │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.temporaryCachePath  │/data/data/[Bundle Identifier]/cache                      │
└──────────────────────┴─────────────────────────────┘
パターン②
┌────────────────────────────────────────────────────┐
│SDカードの有無 : Install Location + Write Access                                                        │
│────────────────────────────────────────────────────│
│SDカード有り   : Prefer External  + Internal Only                                                       │
│SDカード無し   : Force Internal   + Internal Only                                                       │
├──────────────────────┬─────────────────────────────┤
│UnityEngine.Application.dataPath            │/data/app/[Bundle Identifier]-1.apk                       │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.persistentDataPath  │/data/data/[Bundle Identifier]/files                      │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.streamingAssetsPath │jar:file:///data/app/[Bundle Identifier]-1.apk!/assets    │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.temporaryCachePath  │/data/data/[Bundle Identifier]/cache                      │
└──────────────────────┴─────────────────────────────┘
パターン③
┌────────────────────────────────────────────────────┐
│SDカードの有無 : Install Location + Write Access                                                        │
│────────────────────────────────────────────────────│
│SDカード有り   : Automatic        + External(SD Card)                                                   │
│SDカード有り   : Prefer External  + External(SD Card)                                                   │●
│SDカード無し   : Prefer External  + External(SD Card)                                                   │●
├──────────────────────┬─────────────────────────────┤
│UnityEngine.Application.dataPath            │/data/app/[Bundle Identifier]-1.apk                       │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.persistentDataPath  │/storage/emulated/0/Android/data/[Bundle Identifier]/files│
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.streamingAssetsPath │jar:file:///data/app/[Bundle Identifier]-1.apk!/assets    │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.temporaryCachePath  │/storage/emulated/0/Android/data/[Bundle Identifier]/cache│
└──────────────────────┴─────────────────────────────┘
パターン④
┌────────────────────────────────────────────────────┐
│SDカードの有無 : Install Location + Write Access                                                        │
│────────────────────────────────────────────────────│
│SDカード有り   : Force Internal   + External(SD Card)                                                   │
│SDカード無し   : Automatic        + External(SD Card)                                                   │
│SDカード無し   : Force Internal   + External(SD Card)                                                   │
├──────────────────────┬─────────────────────────────┤
│UnityEngine.Application.dataPath            │/data/app/[Bundle Identifier]-2.apk                       │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.persistentDataPath  │/storage/emulated/0/Android/data/[Bundle Identifier]/files│
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.streamingAssetsPath │jar:file:///data/app/[Bundle Identifier]-2.apk!/assets    │
├──────────────────────┼─────────────────────────────┤
│UnityEngine.Application.temporaryCachePath  │/storage/emulated/0/Android/data/[Bundle Identifier]/cache│
└──────────────────────┴─────────────────────────────┘
※Development Buildオプションにチェックがあると、
[Write Access : External(SD Card)]設定でパターン③になる
[Write Access : Internal Only]設定でパターン④になる

Android端末によっては下記内容が保証されないかも？
内部ストレージ(内部だけどSDカード1扱い)は [Bundle Identifier]-1
外部のSDカードは [Bundle Identifier]-2
オススメ(●)設定であれば、SDカードの有無に関わらず同じパターン③となる
Install Location : Prefer External
Write Access     : External(SD Card)

Android(Xperia Z Ultra)では内部ストレージはSDカード1扱いとなっている。
その為、[Write Access : External(SD Card)]設定でも、内部ストレージへ保存される。
AndroidManifest.xml に WRITE_EXTERNAL_STORAGE が指定されていれば結果が変わるかも？（未検証）



==================================================
ATK_Plugin_Storage.cs
ATK.Storage.GetExternalStorage()
ATK.Storage.GetInternalStorage()
ATK.Storage.GetTemporaryStorage()→中身は UnityEngine.Application.temporaryCachePath
==================================================

パターン①
┌───────────────────────────────────────────────┐
│SDカードの有無 : Install Location + Write Access                                              │
│───────────────────────────────────────────────│
│SDカード有り   : Automatic        + Internal Only                                             │
│SDカード有り   : Force Internal   + Internal Only                                             │
│SDカード有り   : Prefer External  + Internal Only                                             │
│SDカード無し   : Automatic        + Internal Only                                             │
│SDカード無し   : Force Internal   + Internal Only                                             │
│SDカード無し   : Prefer External  + Internal Only                                             │
├────────────────┬──────────────────────────────┤
│ATK.Storage.GetExternalStorage()│/storage/emulated/0                                         │
├────────────────┼──────────────────────────────┤
│ATK.Storage.GetInternalStorage()│/data/data/[Bundle Identifier]/app_[Dir Name]               │
├────────────────┴──────────────────────────────┤
│ATK.Storage.GetTemporaryCache() │/data/data/[Bundle Identifier]/cache                        │
└────────────────┴──────────────────────────────┘
パターン②　※Development Buildオプションにチェックがあると、設定に関係なくパターン②になる
┌───────────────────────────────────────────────┐
│SDカードの有無 : Install Location + Write Access                                              │
│───────────────────────────────────────────────│
│SDカード有り   : Automatic        + External(SD Card)                                         │
│SDカード有り   : Force Internal   + External(SD Card)                                         │
│SDカード有り   : Prefer External  + External(SD Card)                                         │●
│SDカード無し   : Automatic        + External(SD Card)                                         │
│SDカード無し   : Force Internal   + External(SD Card)                                         │
│SDカード無し   : Prefer External  + External(SD Card)                                         │●
├────────────────┬──────────────────────────────┤
│ATK.Storage.GetExternalStorage()│/storage/emulated/0                                         │
├────────────────┼──────────────────────────────┤
│ATK.Storage.GetInternalStorage()│/data/data/[Bundle Identifier]/app_[DirName]                │
├────────────────┴──────────────────────────────┤
│ATK.Storage.GetTemporaryCache() │/storage/emulated/0/Android/data/[Bundle Identifier]/cache  │
└────────────────┴──────────────────────────────┘
上記の通り ATK.Storage を使用すれば、SDカードの有無で変化するのは GetTemporaryCache() のみ


SDカードのデータは編集可能なので、重要なデータは ATK.Storage.GetInternalStorage() を使用して記録する












////////////////////////////////////////////////////////////////////////////////////////////////////
iPhone 6 実機検証
////////////////////////////////////////////////////////////////////////////////////////////////////
























