﻿using UnityEngine;
using System.Collections.Generic;

public class SO_Sound : ScriptableObject
{
    public List<AudioClip> SE;
}