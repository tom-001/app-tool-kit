﻿

using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using ATK;
using ATK.Scene;
using ATK.UI;

using ATK.Utilities;


public class Test : MonoBehaviour
{
    public SO_Sound SE;


    [SerializeField] Canvas _canvas;

	[SerializeField] ATK_UI_DialogBox _dialog;

    public enum ItemFlag
    {
        Item0,
        Item1,
        Item2,
        Item3,
        Item4,
        Item5,
        Max,
    }
    void Start()
	{

        TypedBitArray<ItemFlag> bits = new TypedBitArray<ItemFlag>(ItemFlag.Max);
       bits[0] = true;
        bits[3] = true;
        byte[] bytes = bits.ToByteArray();
        Debug.Log(bytes.ToString());
        string str="";
        
        for(int i=0; i<bits.Length; i++)
        {

            str += bits[i] ? "1" : "0";
        }
        Debug.Log("BITS=" + str);
        Debug.Log("BITS="+bits.BitStrings());
        

        /*
                var prefabs = ATK.Utilities.Resources.LoadAll<GameObject>("Prefab"); 

                // EventSystemManagerインスタンスの作成
                {
                    string prefabName = "ATK_EventSystemManager";
                    ATK.Assert.IsTrue( prefabs.ContainsKey(prefabName), prefabName + "プレハブがない");

                    GameObject prefab = prefabs[prefabName];			
                    GameObject obj = Instantiate(prefab) as GameObject;
                    obj.name = prefabName;
                    GameObject.DontDestroyOnLoad(obj); // 永続化
                }
                //ATK_EventSystemManager.Instance.TouchInputModule.
                _dialog.OnCloseEvent += OnClose;
                _dialog.Show();
                */
    }

void OnClose(ATK_UI_DialogBox.Result result)
	{
		Debug.Log(result.ToString());
	}


	#if UNITY_EDITOR
	float _scale = 1.5f;
	#else
	float _scale = 4.0f;
	#endif
	
	void OnGUI() {
        //GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
        /*
                if (GUI.Button(new Rect(Screen.width-128, Screen.height-32*3, 128, 32), SceneName.@Demo_Save)) {
                    Application.LoadLevel(SceneName.@Demo_Save);
                }
                if (GUI.Button(new Rect(Screen.width-128, Screen.height-32*2, 128, 32), SceneName.@Demo_Sound)) {
                    Application.LoadLevel(SceneName.@Demo_Sound);
                }
                if (GUI.Button(new Rect(Screen.width-128, Screen.height-32*1, 128, 32), SceneName.@Demo_Localization)) {
                    Application.LoadLevel(SceneName.@Demo_Localization);
                }
        */

        GUIUtility.ScaleAroundPivot(new Vector2(_scale, _scale), new Vector2(0,0));
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("GUI Scale");
		if (GUILayout.Button("+")) { _scale = Mathf.Clamp(_scale+0.2f, 1.0f, 5.0f); }
		if (GUILayout.Button("-")) { _scale = Mathf.Clamp(_scale-0.2f, 1.0f, 5.0f); }
		GUILayout.EndHorizontal();


//		GUILayout.BeginArea(new Rect(0,0,W,H));
		GUILayout.BeginVertical();


		GUILayout.Space(16);

		if (GUILayout.Button("int")) {
		}
		if (GUILayout.Button("out")) {
		}


		GUILayout.EndVertical();
//		GUILayout.EndArea();
		
//		GUI.matrix = mat;

		GUILayout.Space(100);

	}

}



