﻿/*

==================================================
CacheComponentMonoBehaviour.cs
を出力する Editor スクリプト
==================================================

●概要
コンポーネントをキャッシュしたMonoBhaviour派生クラスファイルを生成
Unity5ではgameObjectやtransformを除く、変数として扱えていたrigidbodyやaudioなどの便利変数が非推奨になります。
恐らくパフォーマンス的な問題なのでしょう。
ですので自前でコンポーネントのキャッシュを行うCacheComponentMonoBehaviourを作成します。

●出力方法
メニューの Assets -> Create CacheComponentMonoBehaviourから作成します。
既にCacheComponentMonoBehaviour.csが作成されている場合は上書きしますので注意してください。

●使い方
MonoBehaviourを継承する代わりにCacheComponentMonoBehaviourを継承するようにします。



==================================================
参考
==================================================
UNIBOOK-1.0.0.pdf
第20章：Monovehaviarで拡張しておきたいもの
記載コードを流用

●変更点
・文字コードをBOM有りUTF8で保存するよう変更
・出力パスを変更 "Assets/ScriptsBuild/" ※既にどこかにファイルが存在する場合は、上書き

 */


using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;


public class CreateCacheComponentMonoBehaviour
{
	[MenuItem("Assets/Create CacheComponentMonoBehaviour")]
	static void CreateScript ()
	{
		//CacheComponentMonoBehaviour.cs のスクリプトをテキストで生成する

		StringBuilder sb = new StringBuilder ();
		sb.AppendLine ("using UnityEngine;");
		sb.AppendLine ("using UnityEngine.Networking.Match;");
		sb.AppendLine ("");
		sb.AppendLine ("public class CacheComponentMonoBehaviour : MonoBehaviour");
		sb.AppendLine ("{");

		foreach (var type in Assembly.Load("UnityEngine.dll").GetTypes()) {
			// コンポーネント&非推奨ではない &MonoBehaviour ではない
			if (type.IsSubclassOf (typeof(Component))
			    && type.IsPublic
			    && IsObsolete (type) == false
				&& type != typeof(MonoBehaviour)) {
				var name = type.Name;
				sb.AppendFormat ("\tprivate {0} m_{1};\n\n", name, name);
				sb.AppendFormat ("\tpublic {0} cached{1} {{\n", name, name);
				sb.AppendLine ("\t\tget {");
				sb.AppendFormat ("\t\t\tif (m_{0} == null)\n", name);
				sb.AppendFormat ("\t\t\t\tm_{0} = GetComponent<{1}> ();\n", name, name);
				sb.AppendFormat ("\t\t\treturn m_{0};\n", name);
				sb.AppendLine ("\t\t}");
				sb.AppendLine ("\t}");
				sb.AppendLine ();
			}
		}
		sb.Append ("}");

		string fileName = "CacheComponentMonoBehaviour";
		string dirName = "Assets/ScriptsBuild/";
		string path;

		// 全ての Runtime スクリプトを取得して CacheComponentMonoBehaviour.cs が存在すれば取得する
		var importer = MonoImporter.GetAllRuntimeMonoScripts ()
			.Where (i => i.GetClass () != null
			        && i.GetClass ().Name == fileName)
			.FirstOrDefault ();

		// 存在すれば上書きする
		if(importer) {
			path = AssetDatabase.GetAssetPath (importer);
		}
		else {
			if (!Directory.Exists(dirName)) Directory.CreateDirectory(dirName);
			path = dirName + fileName + ".cs";
		}

		System.Text.Encoding enc = new System.Text.UTF8Encoding(true);//BOM有りUTF8
		File.WriteAllText (path, sb.ToString (), enc);
		AssetDatabase.ImportAsset (path);
	}

	// 非推奨かどうか
	static bool IsObsolete (System.Type type)
	{
		return type.GetCustomAttributes (typeof(System.ObsoleteAttribute), true).Length != 0;
	}
}