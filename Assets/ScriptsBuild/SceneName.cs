﻿public class SceneName
{

	/// <summary>
	/// return "MainScene"
 	/// </summary>
	public const string @MainScene = "MainScene";

	/// <summary>
	/// <para>0. "MainScene"</para>
	/// </summary>
	public static readonly string[] names = new string[]{"MainScene"};
}
